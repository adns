--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Exceptions;

with Anet.Constants;

with D_Bus.Arguments.Basic;
with D_Bus.Arguments.Containers;
with D_Bus.Messagebox;

with Adns_Util.Logger;

with DNS.Domain;
with DNS.Message;
with DNS.Resource_Record;

package body Resolver
is
   package Logger renames Adns_Util.Logger;

   --  Parse the servers in the given D-Bus message.
   procedure Parse_Servers
     (Msg     :        D_Bus.Messages.Message_Type;
      Index   :    out Natural;
      Servers : in out DNS.Server.List.Server_List);

   --  Parse the hostname and family in the given D-Bus message.
   procedure Parse_Hostname
     (Msg    :     D_Bus.Messages.Message_Type;
      Domain : out DNS.Domain.Domain_Type;
      Family : out Natural);

   -------------------------------------------------------------------------

   procedure Parse_Servers
     (Msg     :        D_Bus.Messages.Message_Type;
      Index   :    out Natural;
      Servers : in out DNS.Server.List.Server_List)
   is
      use D_Bus;
      use D_Bus.Arguments.Basic;
      use D_Bus.Arguments.Containers;

      --  Parse the arguments "ia(iay)", that is, an integer denoting the
      --  interface index (unused) followed by an array of addresses that each
      --  consists of an integer for the family followed by a byte array.

      Args      : constant Arguments.Argument_List_Type
        := Messages.Get_Arguments (Msg => Msg);
      Ifindex   : constant Signed_32 := Int32_Type (Args.First_Element).To_Ada;
      Addresses : constant Array_Type := Array_Type
        (Args.Get_Element (Pos  => 2));
   begin
      Logger.Log
        (Level   => Logger.Debug,
         Message => "Set_Servers: ifindex" & Ifindex'Img & ", count"
         & Addresses.Get_Count'Img);

      Index := Natural (Ifindex);

      for I in 1 .. Addresses.Get_Count loop
         declare
            Addr_Rec : constant Struct_Type := Struct_Type
              (Addresses.Get_Element (Pos => I));

            Family   : constant Signed_32 := Int32_Type
              (Addr_Rec.Get_Element (Pos  => 1)).To_Ada;

            Octets   : constant Array_Type := Array_Type
              (Addr_Rec.Get_Element (Pos => 2));
         begin
            case Family is
               when Anet.Constants.Sys.AF_INET =>
                  declare
                     IPv4 : Anet.IPv4_Addr_Type;
                  begin
                     if Octets.Get_Count /= IPv4'Length then
                        raise Constraint_Error with "Expected" &
                          IPv4'Length'Img & " bytes, received" &
                          Octets.Get_Count'Img;
                     end if;

                     for O in IPv4'Range loop
                        IPv4 (O) := Anet.Byte
                          (Byte_Type (Octets.Get_Element (Pos => O)).To_Ada);
                     end loop;

                     Servers.Append
                       (New_Item => DNS.Server.Create
                          (Address => IPv4,
                           Port    => DNS.Server_Port_Default));
                     Logger.Log
                       (Level   => Logger.Info,
                        Message => "Set_Servers: received server " &
                          Anet.To_String (Address => IPv4));
                  end;

               when Anet.Constants.Sys.AF_INET6 =>
                  declare
                     IPv6 : Anet.IPv6_Addr_Type;
                  begin
                     if Octets.Get_Count /= IPv6'Length then
                        raise Constraint_Error with "Expected" &
                          IPv6'Length'Img & " bytes, received" &
                          Octets.Get_Count'Img;
                     end if;

                     for O in IPv6'Range loop
                        IPv6 (O) := Anet.Byte
                          (Byte_Type (Octets.Get_Element (Pos => O)).To_Ada);
                     end loop;

                     Servers.Append
                       (New_Item => DNS.Server.Create
                          (Address => IPv6,
                           Port    => DNS.Server_Port_Default));
                     Logger.Log
                       (Level   => Logger.Info,
                        Message => "Set_Servers: received server " &
                          Anet.To_String (Address => IPv6));
                  end;
               when others =>
                  raise Constraint_Error with "Unknown address family"
                    & Family'Img;
            end case;
         end;
      end loop;
   end Parse_Servers;

   -------------------------------------------------------------------------

   procedure Set_Servers (In_Msg : D_Bus.Messages.Message_Type)
   is
      Ifindex     : Natural;
      Servers     : DNS.Server.List.Server_List;
      All_Servers : DNS.Server.List.Server_List;
   begin
      Parse_Servers (Msg     => In_Msg,
                     Index   => Ifindex,
                     Servers => Servers);

      if Server_Map.Contains (Key => Ifindex) then
         Server_Map (Ifindex) := Servers;
      else
         Server_Map.Insert (Key      => Ifindex,
                            New_Item => Servers);
      end if;

      for List of Server_Map loop
         for Server of List loop
            All_Servers.Append (New_Item => Server);
         end loop;
      end loop;

      DNS.Resolution.Manager.Set_Servers (Servers => All_Servers);

      if not D_Bus.Messages.Is_No_Reply_Expected (Msg => In_Msg) then
         D_Bus.Messagebox.Enqueue
           (M => D_Bus.Messages.New_Method_Return
              (Method_Call => In_Msg));
      end if;

   exception
      when E : others =>
         Logger.Log
           (Level   => Logger.Error,
            Message => "Set_Servers failed - "
            & Ada.Exceptions.Exception_Name (X => E) & ": "
            & Ada.Exceptions.Exception_Message (X => E));
         if not D_Bus.Messages.Is_No_Reply_Expected (Msg => In_Msg) then
            D_Bus.Messagebox.Enqueue
              (M => D_Bus.Messages.New_Error
                 (Reply_To      => In_Msg,
                  Error_Name    => "org.freedesktop.DBus.Error.InvalidArgs",
                  Error_Message => "Invalid arguments for SetLinkDNS - "
                  & Ada.Exceptions.Exception_Message (X => E)));
         end if;
   end Set_Servers;

   -------------------------------------------------------------------------

   procedure Parse_Hostname
     (Msg    :     D_Bus.Messages.Message_Type;
      Domain : out DNS.Domain.Domain_Type;
      Family : out Natural)
   is
      use D_Bus;
      use D_Bus.Arguments.Basic;

      --  Parse the arguments "isit", that is, an integer denoting the
      --  interface index (unused) followed by a string for the domain name
      --  and an integer that denotes the requested address family. The final
      --  64-bit integer is for flags (unused).

      Args    : constant Arguments.Argument_List_Type
        := Messages.Get_Arguments (Msg => Msg);
      Ifindex : constant Signed_32 := Int32_Type
        (Args.First_Element).To_Ada;
      Name    : constant String := Args.Get_Element (Pos => 2).To_String;
      Fam32   : constant Signed_32 := Int32_Type
        (Args.Get_Element (Pos => 3)).To_Ada;
      Flags   : constant Unsigned_64 := U_Int64_Type
        (Args.Last_Element).To_Ada;
   begin
      Logger.Log
        (Level   => Logger.Debug,
         Message => "Resolve_Hostname: ifindex" & Ifindex'Img & ", " & Name
         & ", family" & Fam32'Img & ", flags" & Flags'Img);

      case Fam32 is
         when Anet.Constants.Sys.AF_INET |
              Anet.Constants.Sys.AF_INET6 |
              Anet.Constants.Sys.AF_UNSPEC =>
            Family := Natural (Fam32);
         when others =>
            raise Constraint_Error with "Invalid address family" & Fam32'Img;
      end case;

      Domain := DNS.Domain.Validate (Value => Name);

   exception
      when E : DNS.Domain.Invalid_Domain =>
         raise DNS.Domain.Invalid_Domain with
           Ada.Exceptions.Exception_Message (X => E)
           & ": '" & Name & "'";
   end Parse_Hostname;

   -------------------------------------------------------------------------

   procedure Resolve_Hostname (In_Msg : D_Bus.Messages.Message_Type)
   is
      Domain : DNS.Domain.Domain_Type;
      Family : Natural;
   begin
      Parse_Hostname (Msg    => In_Msg,
                      Domain => Domain,
                      Family => Family);

      declare
         Resolutions    : DNS.Resolution.List.Resolution_List;
         Handle_Results : Dbus_Resolutions_Handler;
      begin
         if Family /= Anet.Constants.Sys.AF_INET6 then
            Resolutions.Append
              (New_Item => DNS.Resolution.Prepare (Domain => Domain,
                                                   QType  => DNS.A));
         end if;

         if Family /= Anet.Constants.Sys.AF_INET then
            Resolutions.Append
              (New_Item => DNS.Resolution.Prepare (Domain => Domain,
                                                   QType  => DNS.AAAA));
         end if;

         Handle_Results.In_Msg := D_Bus.Messages.Ref (In_Msg);

         DNS.Resolution.Manager.Resolve (Resolutions => Resolutions,
                                         Handler     => Handle_Results);
      end;
   exception
      when E : others =>
         Logger.Log
           (Level   => Logger.Error,
            Message => "Resolve_Hostname failed - "
            & Ada.Exceptions.Exception_Name (X => E) & ": "
            & Ada.Exceptions.Exception_Message (X => E));
         D_Bus.Messagebox.Enqueue
           (M => D_Bus.Messages.New_Error
              (Reply_To      => In_Msg,
               Error_Name    => "org.freedesktop.DBus.Error.InvalidArgs",
               Error_Message => "Invalid arguments for ResolveHostname - "
               & Ada.Exceptions.Exception_Message (X => E)));
   end Resolve_Hostname;

   -------------------------------------------------------------------------

   procedure Result
     (Handler     : in out Dbus_Resolutions_Handler;
      Resolutions :        DNS.Resolution.List.Resolution_List)
   is
      use D_Bus;
      use D_Bus.Arguments.Basic;
      use D_Bus.Arguments.Containers;
      use type DNS.Domain.Domain_Type;
      use type DNS.Message.Rcode_Type;
      use type DNS.Resolution.Resolution_State_Type;

      SD_RESOLVED_DNS : constant := 1;

      Out_Msg : Messages.Message_Type;

      State     : DNS.Resolution.Resolution_State_Type
        := DNS.Resolution.Pending;
      Cur_State : DNS.Resolution.Resolution_State_Type;
      Rcode     : DNS.Message.Rcode_Type := DNS.Message.Rc_No_Error;

      Response  : DNS.Message.Response_Type;
      Canonical : DNS.Domain.Domain_Type;

      Addresses : Array_Type;
   begin
      for Resolution of Resolutions loop
         Cur_State := DNS.Resolution.Get_State (Resolution => Resolution);
         case Cur_State is
            when DNS.Resolution.Resolved =>
               Response := DNS.Resolution.Get_Response
                 (Resolution => Resolution);

               if DNS.Message.Get_RCode
                 (Response => Response) = DNS.Message.Rc_No_Error
               then
                  if State /= Cur_State then
                     --  Use the canonical name of the first successful
                     --  response.
                     Canonical := DNS.Message.Get_Canonical
                       (Response => Response);
                     State := Cur_State;
                  elsif Canonical /= DNS.Message.Get_Canonical
                    (Response => Response)
                  then
                     Logger.Log
                       (Level   => Logger.Warning,
                        Message => "Received conflicting canonical names: "
                        & DNS.Domain.Get_Domain (Value => Canonical) & " and "
                        & DNS.Domain.Get_Domain
                          (Value => DNS.Message.Get_Canonical
                               (Response => Response)));
                  end if;

                  if DNS.Message.Has_Answer (Response => Response) then
                     for RR of DNS.Message.Get_Answer (Response => Response)
                     loop
                        declare
                           Addr_Rec : Struct_Type;
                           Octets   : Array_Type;
                        begin
                           --  Prepare an entry for the address array. Each
                           --  element is encoded as "iiay" with an integer for
                           --  the interface index (unused) followed by the
                           --  family and an array of address bytes.

                           Addr_Rec.Append (New_Item => +Signed_32 (0));
                           if RR in DNS.Resource_Record.A_Resource_Record_Type
                           then
                              Addr_Rec.Append
                                (New_Item =>
                                   +Signed_32 (Anet.Constants.Sys.AF_INET));
                           else
                              Addr_Rec.Append
                                (New_Item =>
                                   +Signed_32 (Anet.Constants.Sys.AF_INET6));
                           end if;
                           for Octet of RR.Get_Data loop
                              Octets.Append (New_Item => +Byte (Octet));
                           end loop;
                           Addr_Rec.Append (New_Item => Octets);

                           Addresses.Append (New_Item => Addr_Rec);
                        end;
                     end loop;
                  end if;
               else
                  --  This trumps other error states.
                  State := DNS.Resolution.Failure;
                  Rcode := DNS.Message.Get_RCode (Response);

                  declare
                     Level : Logger.Log_Level := Logger.Warning;
                  begin
                     if Rcode = DNS.Message.Rc_Name_Error then
                        Level := Logger.Debug;
                     end if;

                     Logger.Log (Level   => Level,
                                 Message => "Received negative result for "
                                 & DNS.Domain.Get_Domain
                                   (DNS.Message.Get_QName (Response)) & " ["
                                 & DNS.Message.Get_QType (Response)'Img & "]: "
                                 & Rcode'Img);
                  end;
               end if;
            when DNS.Resolution.Timeout
               | DNS.Resolution.Failure =>
               if State = DNS.Resolution.Pending then
                  State := Cur_State;
               end if;
            when DNS.Resolution.Pending =>
               Logger.Log (Level   => Logger.Error,
                           Message => "Resolution is unexpectedly pending");
         end case;
      end loop;

      case State is
         when DNS.Resolution.Resolved =>
            if Addresses.Get_Count > 0 then
               Out_Msg := Messages.New_Method_Return
                 (Method_Call => Handler.In_Msg);

               --  Set the response parameters "a(iiay)st", that is, an array
               --  of addresses (see above for details), followed by the
               --  canonical host name and a 64-bit flags field.

               Messages.Add_Arguments
                 (Msg  => Out_Msg,
                  Args => +Addresses);
               Messages.Add_Arguments
                 (Msg  => Out_Msg,
                  Args => +DNS.Domain.Get_Domain (Canonical));
               Messages.Add_Arguments
                 (Msg  => Out_Msg,
                  Args => +Unsigned_64 (SD_RESOLVED_DNS));
            else
               Out_Msg := Messages.New_Error
                 (Reply_To      => Handler.In_Msg,
                  Error_Name    => "org.freedesktop.resolve1.NoSuchRR",
                  Error_Message =>
                    "Name exists but requested RR not available");
            end if;

         when DNS.Resolution.Timeout =>
            --  We don't really know if the network is down, but it's the
            --  closest documented error code.
            Out_Msg := Messages.New_Error
              (Reply_To      => Handler.In_Msg,
               Error_Name    => "org.freedesktop.resolve1.NetworkDown",
               Error_Message => "Timeout during resolution");

         when DNS.Resolution.Failure
            | DNS.Resolution.Pending =>
            case Rcode is
               when DNS.Message.Rc_No_Error =>
                  --  Generic failure (i.e. not an error code received from a
                  --  server), most likely because we don't have any servers.
                  Out_Msg := Messages.New_Error
                    (Reply_To      => Handler.In_Msg,
                     Error_Name    => "org.freedesktop.resolve1.NoNameServers",
                     Error_Message => "Unknown error (no DNS servers?)");
               when DNS.Message.Rc_Format_Error =>
                  Out_Msg := Messages.New_Error
                    (Reply_To      => Handler.In_Msg,
                     Error_Name    =>
                        "org.freedesktop.resolve1.DnsError.FORMERR",
                     Error_Message => "Server reported a format error");
               when DNS.Message.Rc_Server_Failure =>
                  Out_Msg := Messages.New_Error
                    (Reply_To      => Handler.In_Msg,
                     Error_Name    =>
                        "org.freedesktop.resolve1.DnsError.SERVFAIL",
                     Error_Message => "Server reported an internal failure");
               when DNS.Message.Rc_Name_Error =>
                  Out_Msg := Messages.New_Error
                    (Reply_To      => Handler.In_Msg,
                     Error_Name    =>
                        "org.freedesktop.resolve1.DnsError.NXDOMAIN",
                     Error_Message => "Domain name doesn't exist");
               when DNS.Message.Rc_Not_Implemented =>
                  Out_Msg := Messages.New_Error
                    (Reply_To      => Handler.In_Msg,
                     Error_Name    =>
                        "org.freedesktop.resolve1.DnsError.NOTIMPL",
                     Error_Message =>
                        "Server doesn't support requested query");
               when DNS.Message.Rc_Refused =>
                  Out_Msg := Messages.New_Error
                    (Reply_To      => Handler.In_Msg,
                     Error_Name    =>
                        "org.freedesktop.resolve1.DnsError.REFUSED",
                     Error_Message =>
                        "Server refused to answer for policy reasons");
            end case;
      end case;

      Messagebox.Enqueue (M => Out_Msg);
      Messages.Unref (Msg => Handler.In_Msg);
   end Result;

end Resolver;
