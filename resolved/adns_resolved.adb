--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Command_Line;
with Ada.Exceptions;
with Ada.Interrupts.Names;

with GNAT.Command_Line;

with D_Bus.Connection;
with D_Bus.Messages;
with D_Bus.Messagebox;
with D_Bus.Message_Dispatcher.Proc;

with Adns_Util.Logger;
with Adns_Util.Signals;

with DNS.Client;
with DNS.Version;

with Resolver;

procedure Adns_Resolved
is
   package Logger renames Adns_Util.Logger;

   use D_Bus;

   Intro_Spect : constant String :=
   "<!DOCTYPE node PUBLIC ""-//freedesktop//DTD D-BUS Object "                &
   "Introspection 1.0//EN"""                                       & ASCII.LF &
   """http://www.freedesktop.org/standards/dbus/"                             &
   "1.0/introspect.dtd"">"                                         & ASCII.LF &
   "<node name=""/org/freedesktop/resolve1"">"                     & ASCII.LF &
   "  <interface name=""org.freedesktop.DBus.Introspectable"">"    & ASCII.LF &
   "    <method name=""Introspect"">"                              & ASCII.LF &
   "      <arg name=""introspection_xml"" direction=""out"" type=""s""/>"
     & ASCII.LF &
   "    </method>"                                                 & ASCII.LF &
   "  </interface>"                                                & ASCII.LF &
   "  <interface name=""org.freedesktop.resolve1.Manager"">"       & ASCII.LF &
   "    <method name=""ResolveHostname"">"                         & ASCII.LF &
   "     <arg type=""i"" direction=""in""/>"                       & ASCII.LF &
   "     <arg type=""s"" direction=""in""/>"                       & ASCII.LF &
   "     <arg type=""i"" direction=""in""/>"                       & ASCII.LF &
   "     <arg type=""t"" direction=""in""/>"                       & ASCII.LF &
   "     <arg type=""a(iiay)"" direction=""out""/>"                & ASCII.LF &
   "     <arg type=""s"" direction=""out""/>"                      & ASCII.LF &
   "     <arg type=""t"" direction=""out""/>"                      & ASCII.LF &
   "    </method>"                                                 & ASCII.LF &
   "    <method name=""SetLinkDNS"">"                              & ASCII.LF &
   "     <arg type=""i"" direction=""in""/>"                       & ASCII.LF &
   "     <arg type=""a(iay)"" direction=""in""/>"                  & ASCII.LF &
   "    </method>"                                                 & ASCII.LF &
   "  </interface>"                                                & ASCII.LF &
   "</node>";

   -------------------------------------------------------------------------

   Config      : GNAT.Command_Line.Command_Line_Configuration;
   Use_Session : aliased Boolean := False;
   Log_Debug   : aliased Boolean := False;
   Log_Stdout  : aliased Boolean := False;
   Bus         : Bus_Type := Bus_System;
   Handler     : Adns_Util.Signals.Handler_Type
     (Signal => Ada.Interrupts.Names.SIGTERM);
begin
   Logger.Set_Log_Level (Level => Logger.Info);

   GNAT.Command_Line.Set_Usage
     (Config => Config,
      Usage  => "[--session] [--debug] [--stdout]",
      Help   => "D-Bus DNS resolver (" & DNS.Version.Version_String & ")");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Switch      => "-h",
      Long_Switch => "--help",
      Help        => "Display usage and exit");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Output      => Use_Session'Access,
      Long_Switch => "--session",
      Help        => "Connect to the session bus instead of the system bus");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Output      => Log_Debug'Access,
      Long_Switch => "--debug",
      Help        => "Log debug messages");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Output      => Log_Stdout'Access,
      Long_Switch => "--stdout",
      Help        => "Log to stdout");

   begin
      GNAT.Command_Line.Getopt (Config => Config);

      if Use_Session then
         Bus := Bus_Session;
      end if;

      if Log_Debug then
         Logger.Set_Log_Level (Level => Logger.Debug);
      end if;

      if Log_Stdout then
         Logger.Use_Stdout;
      end if;
   exception
      when GNAT.Command_Line.Invalid_Switch =>
         --  Automatically prints output.
         raise GNAT.Command_Line.Exit_From_Command_Line;
   end;

   DNS.Client.Start;

   Logger.Log
     (Level   => Logger.Info,
      Message => "Starting D-Bus DNS resolver ("
      & DNS.Version.Version_String & ")");

   declare
      use type D_Bus.Messages.Message_Type;

      Conn       : constant Connection.Connection_Type
        := Connection.Connect (Bus => Bus);
      Dispatcher : Message_Dispatcher.Proc.Proc_Dispatcher_Type
        := Message_Dispatcher.Proc.Create (Introspect => Intro_Spect);
      Out_Msg    : Messages.Message_Type;
      Success    : Boolean;
   begin
      Connection.Request_Name
        (Connection => Conn,
         Name       => "org.freedesktop.resolve1");
      Dispatcher.Register_Method_Handler
        (Iface   => "org.freedesktop.resolve1.Manager",
         Method  => "SetLinkDNS",
         Handler => Resolver.Set_Servers'Access);
      Dispatcher.Register_Method_Handler
        (Iface   => "org.freedesktop.resolve1.Manager",
         Method  => "ResolveHostname",
         Handler => Resolver.Resolve_Hostname'Access);

      while not Handler.Has_Occurred loop
         Dispatcher.Peek
           (Conn         => Conn,
            Timeout_Msec => 100,
            Success      => Success);
         exit when not Success;

         --  Send completed replies.

         Messagebox.Consume (M => Out_Msg);
         while Out_Msg /= Messages.Null_Message loop
            Connection.Send
              (Connection => Conn,
               Message    => Out_Msg);
            Messages.Unref (Msg => Out_Msg);

            Messagebox.Consume (M => Out_Msg);
         end loop;
      end loop;
   end;

   Logger.Log
     (Level   => Logger.Info,
      Message => "Shutting down D-Bus DNS resolver");

   DNS.Client.Stop;
   Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);

exception
   when GNAT.Command_Line.Exit_From_Command_Line =>
      Logger.Stop;
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when E : others =>
      Logger.Log
        (Level   => Logger.Error,
         Message => "EXCEPTION: " &
           Ada.Exceptions.Exception_Information (X => E));
      DNS.Client.Stop;
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
end Adns_Resolved;
