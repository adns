--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private with Ada.Containers.Ordered_Maps;

with D_Bus.Messages;

private with DNS.Resolution.List;
private with DNS.Resolution.Manager;
private with DNS.Server.List;

package Resolver
is

   --  Set the servers as requested.
   procedure Set_Servers (In_Msg : D_Bus.Messages.Message_Type);

   --  Resolve the requested hostname.
   procedure Resolve_Hostname (In_Msg : D_Bus.Messages.Message_Type);

private

   use type DNS.Server.List.Server_List;

   package Server_Map_Package is new Ada.Containers.Ordered_Maps
     (Element_Type => DNS.Server.List.Server_List,
      Key_Type     => Natural);

   --  Map from interface index to server list.
   Server_Map : Server_Map_Package.Map;

   --  Handler for completed DNS resolutions.
   type Dbus_Resolutions_Handler is
     new DNS.Resolution.Manager.Resolutions_Handler with record
         In_Msg : D_Bus.Messages.Message_Type;
     end record;

   overriding
   procedure Result
     (Handler     : in out Dbus_Resolutions_Handler;
      Resolutions :        DNS.Resolution.List.Resolution_List);

end Resolver;
