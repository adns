GNATTEST_EXIT   ?= on
GNATTEST_PASSED ?= show
GNATTEST_OPTS   ?= -q --test-duration --separate-drivers=test --omit-sloc
GNATTEST_DRIVERS = $(OBJ_DIR)/tests/gnattest/harness/test_drivers.list
GNATTEST_HARNESS = $(OBJ_DIR)/tests/gnattest/harness/test_drivers
TESTS_DIR        = $(CURDIR)/tests

BIN_DIR = bin
OBJ_DIR = obj
DESTDIR = /usr/local

FUZZ_DIR  = $(OBJ_DIR)/fuzzing
AFL_SMP   = fuzzing/afl-smp.py
AFL_DATA  = fuzzing/dns-response.dat
LCOV_EXCLUDE_PATTERN  = '*fuzzing/*'
LCOV_EXCLUDE_PATTERN += '*adainclude*'

VERSION_SPEC  = src/dns-version.ads
VERSION       = $(shell cat .version | sed 's/^v//')
GIT_REV      := $(shell git describe --always --dirty="-UNCLEAN" 2> /dev/null)

NUM_CPUS   := $(shell getconf _NPROCESSORS_ONLN)
GMAKE_OPTS ?= -p -j$(NUM_CPUS)

SRC_FILES = $(wildcard src/*)
CORPUS    = fuzzing/queue

all: build_resolved build_cli

.version: FORCE
	@if [ -d .git ]; then \
		if [ -r $@ ]; then \
			if [ "$$(cat $@)" != "$(GIT_REV)" ]; then \
				echo $(GIT_REV) > $@; \
			fi; \
		else \
			echo $(GIT_REV) > $@; \
		fi \
	fi

$(VERSION_SPEC): .version
	@echo "package DNS.Version is"                  > $@
	@echo "   Version_String : constant String :=" >> $@
	@echo "     \"$(VERSION)\";"                   >> $@
	@echo "end DNS.Version;"                       >> $@

build_resolved build_cli: build_%: $(VERSION_SPEC)
	@gprbuild $(GMAKE_OPTS) -Padns_$*.gpr

$(OBJ_DIR)/.harness_stamp: $(SRC_FILES)
	@mkdir -p $(OBJ_DIR)/tests
	gnattest $(GNATTEST_OPTS) -Padns_tests.gpr
	@touch $@

build_tests: $(OBJ_DIR)/.harness_stamp $(VERSION_SPEC)
	gprbuild $(GMAKE_OPTS) -P$(GNATTEST_HARNESS) -largs -fprofile-generate

tests: build_tests
	gnattest $(GNATTEST_DRIVERS) --passed-tests=$(GNATTEST_PASSED)

cov: build_tests
	rm -f $(COV_DIR)/tests/*.gcda
	gnattest $(GNATTEST_DRIVERS) --passed-tests=$(GNATTEST_PASSED)
	lcov -c -d $(OBJ_DIR)/tests -o $(OBJ_DIR)/cov.info
	lcov -e $(OBJ_DIR)/cov.info "$(PWD)/src/*.adb" -o $(OBJ_DIR)/cov.info
	genhtml --no-branch-coverage $(OBJ_DIR)/cov.info -o $(OBJ_DIR)/cov

install: install_resolved install_cli

install_resolved: build_resolved
	install -m 2775 -d $(DESTDIR)/sbin
	install $(BIN_DIR)/adns_resolved $(DESTDIR)/sbin

install_cli: build_cli
	install -m 2775 -d $(DESTDIR)/bin
	install $(BIN_DIR)/adns_resolve_cli $(DESTDIR)/bin

build_fuzz:
	@mkdir -p $(FUZZ_DIR)
	@gprbuild --compiler-subst=Ada,afl-gcc -p -Padns_fuzz.gpr

build_fuzz_cov:
	@mkdir -p $(FUZZ_DIR)/cov
	@gprbuild --compiler-subst=Ada,afl-gcc -p -Padns_fuzz.gpr -Xbuild=coverage

fuzz_prepare: $(CORPUS) build_fuzz
	@mkdir -p $(FUZZ_DIR)/out
	@cp -r $(CORPUS) $(FUZZ_DIR)/in

fuzz: fuzz_prepare
	afl-fuzz -i $(FUZZ_DIR)/in -o $(FUZZ_DIR)/out $(FUZZ_DIR)/dns_msg_deserialize @@

fuzz_parallel: $(AFL_SMP) fuzz_prepare
	$(AFL_SMP) --threads $(NUM_CPUS) $(FUZZ_DIR)/in $(FUZZ_DIR)/out $(FUZZ_DIR)/dns_msg_deserialize

fuzz_cov: export AFL_BENCH_JUST_ONE :=1
fuzz_cov: export AFL_FAST_CAL := 1
fuzz_cov: build_fuzz_cov
	test -d $(FUZZ_DIR)/out || $(MAKE) fuzz
	afl-cov -d $(FUZZ_DIR)/out --overwrite --lcov-exclude-pattern "$(LCOV_EXCLUDE_PATTERN)" \
		--coverage-cmd "$(FUZZ_DIR)/cov/dns_msg_deserialize AFL_FILE" \
		--code-dir $(FUZZ_DIR)/cov/
	@unset AFL_BENCH_JUST_ONE
	@unset AFL_FAST_CAL

clean:
	@rm -rf $(BIN_DIR)
	@rm -rf $(OBJ_DIR)

FORCE:
