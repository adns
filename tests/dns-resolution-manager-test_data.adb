--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

with Adns_Util.Logger;
with Adns_Util.Timer;

with DNS.Socket.Mock;

package body DNS.Resolution.Manager.Test_Data is

   procedure Set_Up (Gnattest_T : in out Test) is
      pragma Unreferenced (Gnattest_T);
   begin
      Adns_Util.Logger.Set_Log_Level (Level => Adns_Util.Logger.Emergency);
      Adns_Util.Logger.Use_Stdout;
      Adns_Util.Timer.Start;
      Start;
   end Set_Up;

   procedure Tear_Down (Gnattest_T : in out Test) is
      pragma Unreferenced (Gnattest_T);
   begin
      Stop;
      Adns_Util.Timer.Stop;
      Adns_Util.Logger.Stop;
   end Tear_Down;

   -------------------------------------------------------------------------

   procedure Result
     (Handler     : in out Test_Resolutions_Handler;
      Resolutions :        Resolution.List.Resolution_List)
   is
   begin
      Last_Resolutions (Handler.Id) := Resolutions;
   end Result;

   -------------------------------------------------------------------------

   procedure Clear
   is
   begin
      for Res of Last_Resolutions loop
         Res.Clear;
      end loop;
      DNS.Socket.Mock.Clear;
   end Clear;

end DNS.Resolution.Manager.Test_Data;
