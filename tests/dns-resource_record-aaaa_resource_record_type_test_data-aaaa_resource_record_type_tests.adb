--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Resource_Record.AAAA_Resource_Record_Type_Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Exceptions;

--  begin read only
--  end read only
package body DNS.Resource_Record.AAAA_Resource_Record_Type_Test_Data.AAAA_Resource_Record_Type_Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_1_Validate (Gnattest_T : in out Test_AAAA_Resource_Record_Type);
   procedure Test_Validate_f2e4de (Gnattest_T : in out Test_AAAA_Resource_Record_Type) renames Test_1_Validate;
--  id:2.2/f2e4de95be05c98e/Validate/1/0/
   procedure Test_1_Validate (Gnattest_T : in out Test_AAAA_Resource_Record_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Data   : constant Ada.Streams.Stream_Element_Array
        := (16#0a#, 16#0b#, 16#0c#, 16#0d#);
      --  Actually expects the full message, but doesn't matter in this case
      Params : aliased Create_Params
        := (Len      => Data'Length,
            Message  => Data,
            Index    => 1,
            RType    => AAAA,
            RClass   => IN_Class,
            RDLength => Data'Length,
            Domain   => DNS.Domain.Validate ("."));
      RR     : constant AAAA_Resource_Record_Type
        := Create (Params  => Params'Access);
   begin

      begin
         RR.Validate;
         Assert (Condition => False,
                 Message   => "AAAA record incorrectly valid");
      exception
         when E : Invalid_Resource_Record =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E) =
                      "AAAA RR data size 4, expected 16",
                    Message   => "Unexpected message for invalid AAAA RR");
      end;

--  begin read only
   end Test_1_Validate;
--  end read only


--  begin read only
   procedure Test_1_Get_Data_Str (Gnattest_T : in out Test_AAAA_Resource_Record_Type);
   procedure Test_Get_Data_Str_9fba4a (Gnattest_T : in out Test_AAAA_Resource_Record_Type) renames Test_1_Get_Data_Str;
--  id:2.2/9fba4a0b895d595f/Get_Data_Str/1/0/
   procedure Test_1_Get_Data_Str (Gnattest_T : in out Test_AAAA_Resource_Record_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Message : constant Ada.Streams.Stream_Element_Array
        := (16#00#, 16#00#, 16#1c#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#10#, 16#20#, 16#01#, 16#0d#, 16#b8#, 16#00#,
            16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#0a#, 16#01#);
      Length  : Ada.Streams.Stream_Element_Count;
      RR      : Resource_Record_Type'Class
        := Deserialize (Message => Message,
                        Index   => Message'First,
                        Length  => Length);
      Actual  : AAAA_Resource_Record_Type := AAAA_Resource_Record_Type (RR);
   begin

      Assert (Expected => "2001:db8::a01",
              Actual   => Get_Data_Str (Actual),
              Message  => "Resource record data doesn't match");

--  begin read only
   end Test_1_Get_Data_Str;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Resource_Record.AAAA_Resource_Record_Type_Test_Data.AAAA_Resource_Record_Type_Tests;
