--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

with AUnit.Test_Fixtures;

package DNS.Resolution.Manager.Test_Data is

--  begin read only
   type Test is new AUnit.Test_Fixtures.Test_Fixture
--  end read only
   with null record;

   procedure Set_Up (Gnattest_T : in out Test);
   procedure Tear_Down (Gnattest_T : in out Test);

   type Test_Resolutions_Handler (Id : Natural) is
     new Resolution.Manager.Resolutions_Handler with null record;

   procedure Result
     (Handler     : in out Test_Resolutions_Handler;
      Resolutions :        Resolution.List.Resolution_List);

   procedure Clear;

   First_Handler  : Test_Resolutions_Handler (1);
   Second_Handler : Test_Resolutions_Handler (2);

   type Resolutions_Array is array (1 .. 2) of Resolution.List.Resolution_List;

   Last_Resolutions : Resolutions_Array;

end DNS.Resolution.Manager.Test_Data;
