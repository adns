--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Resolution.Transmit.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Streams;

with GNAT.Source_Info;

with DNS.Config;
with DNS.Socket.Instances;
with DNS.Socket.Mock;
with DNS.Test_Data;

--  begin read only
--  end read only
package body DNS.Resolution.Transmit.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

   procedure Assert_Socket_Counters
     (Init, Close, Send, Receive : Natural := 0;
      Source                     : String := GNAT.Source_Info.File;
      Line                       : Natural := GNAT.Source_Info.Line)
   is
      use DNS.Socket.Mock;
   begin
      Assert (Condition => Socket_Data.Init_Count = Init,
              Message   => "Init() called unexpected number of times:" &
                DNS.Socket.Mock.Socket_Data.Init_Count'Img,
              Source    => Source,
              Line      => Line);
      Assert (Condition => Socket_Data.Close_Count = Close,
              Message   => "Close() called unexpected number of times:" &
                DNS.Socket.Mock.Socket_Data.Close_Count'Img,
              Source    => Source,
              Line      => Line);
      Assert (Condition => Socket_Data.Send_Count = Send,
              Message   => "Send() called unexpected number of times:" &
                DNS.Socket.Mock.Socket_Data.Send_Count'Img,
              Source    => Source,
              Line      => Line);
      Assert (Condition => Socket_Data.Receive_Count = Receive,
              Message   => "Receive() called unexpected number of times:" &
                DNS.Socket.Mock.Socket_Data.Receive_Count'Img,
              Source    => Source,
              Line      => Line);
   end Assert_Socket_Counters;

--  begin read only
--  end read only

--  begin read only
   procedure Test_Init (Gnattest_T : in out Test);
   procedure Test_Init_48a65f (Gnattest_T : in out Test) renames Test_Init;
--  id:2.2/48a65f8a5e3b6f78/Init/1/0/
   procedure Test_Init (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin

      Assert (Condition => True,
              Message   => "Tested implicitly below");

--  begin read only
   end Test_Init;
--  end read only


--  begin read only
   procedure Test_Transmit (Gnattest_T : in out Test);
   procedure Test_Transmit_1b5c19 (Gnattest_T : in out Test) renames Test_Transmit;
--  id:2.2/1b5c19e7345982a4/Transmit/1/0/
   procedure Test_Transmit (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Ada.Real_Time.Time;
      use type Ada.Streams.Stream_Element_Offset;
      use type DNS.Message.Query.Query_Type;

      Trans      : Resolution_Transmit_Type;
      Domain     : DNS.Domain.Domain_Type
        := DNS.Domain.Validate ("www.hsr.ch");
      Query      : DNS.Message.Query.Query_Type;
      Servers    : DNS.Server.List.Server_List;
      Resolution : DNS.Resolution.Resolution_Type;
      Next       : Ada.Real_Time.Time;

      ----------------------------------------------------------------------

      procedure Prepare_Response
        (After  : Natural;
         Msg    : Ada.Streams.Stream_Element_Array;
         Server : DNS.Server.Server_Type'Class)
      is
      begin
         DNS.Socket.Mock.Socket_Data.Receive_After := After;
         DNS.Socket.Mock.Socket_Data.Receive_Msg (1 .. Msg'Length) := Msg;
         DNS.Socket.Mock.Socket_Data.Receive_Len := Msg'Length;
         DNS.Socket.Mock.Socket_Data.Receive_Server
           := DNS.Socket.Mock.SH.To_Holder (Server);
      end Prepare_Response;

      ----------------------------------------------------------------------

      procedure Assert_Pending_Transmit
        (T      : in out Resolution_Transmit_Type;
         R      : in out DNS.Resolution.Resolution_Type;
         Source : String := GNAT.Source_Info.File;
         Line   : Natural := GNAT.Source_Info.Line)
      is
         use Ada.Real_Time;

         Before : Ada.Real_Time.Time := Ada.Real_Time.Clock;
      begin
         Transmit (Trans, Resolution, Next);

         Assert (Condition => Resolution.State = Pending,
                 Message   => "Resolution isn't pending",
                 Source    => Source,
                 Line      => Line);
         Assert (Condition => Next >= Before +
                   To_Time_Span (DNS.Config.Socket_Check_Interval),
                 Message   => "Next check too early",
                 Source    => Source,
                 Line      => Line);
         Assert (Condition => Next <= Before +
                   To_Time_Span (2 * DNS.Config.Socket_Check_Interval),
                 Message   => "Next check too late",
                 Source    => Source,
                 Line      => Line);
      end Assert_Pending_Transmit;

      ----------------------------------------------------------------------

      procedure Assert_Pending_Transmit_Send
        (T      : in out Resolution_Transmit_Type;
         R      : in out DNS.Resolution.Resolution_Type;
         Msg    : Ada.Streams.Stream_Element_Array;
         Server : DNS.Server.Server_Type'Class;
         Source : String := GNAT.Source_Info.File;
         Line   : Natural := GNAT.Source_Info.Line)
      is
         use type Ada.Streams.Stream_Element_Array;
         use DNS.Socket.Mock;

         Before : Natural := Socket_Data.Send_Count;
      begin
         Assert_Pending_Transmit (Trans, Resolution, Source, Line);

         Assert (Condition => Socket_Data.Send_Count = Before + 1,
                 Message   => "Send() not called",
                 Source    => Source,
                 Line      => Line);
         Assert (Condition =>
                   Socket_Data.Send_Msg (1 .. Socket_Data.Send_Len) = Msg,
                 Message   => "Sent message is different: " &
                   Anet.To_Hex
                   (Socket_Data.Send_Msg (1 .. Socket_Data.Send_Len)),
                 Source    => Source,
                 Line      => Line);
         Assert (Condition => Socket_Data.Send_Server.Element = Server,
                 Message   => "Message sent to an unexpected server",
                 Source    => Source,
                 Line      => Line);
      end Assert_Pending_Transmit_Send;

      ----------------------------------------------------------------------

      procedure Test_Invalid_Message
        (Msg    : Ada.Streams.Stream_Element_Array;
         Server : DNS.Server.Server_Type'Class;
         Source : String := GNAT.Source_Info.File;
         Line   : Natural := GNAT.Source_Info.Line)
      is
      begin
         Resolution := Prepare (Domain => Domain,
                                QType  => A);

         Prepare_Response (After  => 2,
                           Msg    => Msg,
                           Server => Server);

         Init (Trans, Query, Servers, DNS.Socket.Instances.Sockets (1)'Access);

         for I in Natural range 1 .. 5 loop
            Assert_Pending_Transmit (Trans, Resolution, Source, Line);
            delay DNS.Config.Socket_Check_Interval;
         end loop;

         Transmit (Trans, Resolution, Next);

         Assert (Condition => Resolution.State = Timeout,
                 Message   => "No timeout occurred",
                 Source    => Source,
                 Line      => Line);

         Close (Trans);

         Assert_Socket_Counters (Init    => 1,
                                 Send    => 2,
                                 Receive => 6,
                                 Close   => 1,
                                 Source  => Source,
                                 Line    => Line);
         DNS.Socket.Mock.Clear;
      end Test_Invalid_Message;
   begin

      Query := DNS.Message.Query.Create (Id     => 16#8689#,
                                         Domain => Domain,
                                         QType  => A);

      --  No servers

      Resolution := Prepare (Domain => Domain,
                             QType  => A);

      Init (Trans, Query, Servers, DNS.Socket.Instances.Sockets (1)'Access);

      Transmit (Trans, Resolution, Next);

      Close (Trans);

      Assert (Condition => Resolution.State = Failure,
              Message   => "Resolution without servers didn't fail");
      Assert_Socket_Counters (Init    => 1,
                              Send    => 0,
                              Receive => 1,
                              Close   => 1);
      DNS.Socket.Mock.Clear;

      --  Successful response

      Resolution := Prepare (Domain => Domain,
                             QType  => A);

      Servers.Append (New_Item => DNS.Server.Create (Address => "8.8.8.8"));

      Prepare_Response (After  => 2,
                        Msg    => DNS.Test_Data.Response,
                        Server => Servers (1));

      Init (Trans, Query, Servers, DNS.Socket.Instances.Sockets (1)'Access);

      Assert_Pending_Transmit (Trans, Resolution);

      Transmit (Trans, Resolution, Next);

      Close (Trans);

      Assert (Condition => Resolution.State = Resolved,
              Message   => "Resolution isn't resolved");

      Assert_Socket_Counters (Init    => 1,
                              Send    => 1,
                              Receive => 2,
                              Close   => 1);
      DNS.Socket.Mock.Clear;

      --  Timeout (same server as above)

      Resolution := Prepare (Domain => Domain,
                             QType  => A);

      Init (Trans, Query, Servers, DNS.Socket.Instances.Sockets (1)'Access);

      for I in Natural range 1 .. 5 loop
         Assert_Pending_Transmit (Trans, Resolution);
         delay DNS.Config.Socket_Check_Interval;
      end loop;

      Transmit (Trans, Resolution, Next);

      Close (Trans);

      Assert (Condition => Resolution.State = Timeout,
              Message   => "No timeout occurred");

      Assert_Socket_Counters (Init    => 1,
                              Send    => 2,
                              Receive => 6,
                              Close   => 1);
      DNS.Socket.Mock.Clear;

      --  Timeout (multiple servers)

      Resolution := Prepare (Domain => Domain,
                             QType  => A);

      Servers.Append (New_Item => DNS.Server.Create (Address => "9.9.9.9"));

      Init (Trans, Query, Servers, DNS.Socket.Instances.Sockets (1)'Access);

      for I in Natural range 1 .. 10 loop
         Assert_Pending_Transmit (Trans, Resolution);
         delay DNS.Config.Socket_Check_Interval;
      end loop;

      Transmit (Trans, Resolution, Next);

      Close (Trans);

      Assert (Condition => Resolution.State = Timeout,
              Message   => "No timeout occurred");

      Assert_Socket_Counters (Init    => 1,
                              Send    => 4,
                              Receive => 11,
                              Close   => 1);
      DNS.Socket.Mock.Clear;

      --  First server doesn't answer

      Resolution := Prepare (Domain => Domain,
                             QType  => A);

      Prepare_Response (After  => 6,
                        Msg    => DNS.Test_Data.Response,
                        Server => Servers (2));

      Init (Trans, Query, Servers, DNS.Socket.Instances.Sockets (1)'Access);

      Assert_Pending_Transmit_Send (Trans, Resolution, DNS.Test_Data.Request,
                                    Servers (1));
      delay DNS.Config.Socket_Check_Interval;
      Assert_Pending_Transmit (Trans, Resolution);
      delay DNS.Config.Socket_Check_Interval;
      Assert_Pending_Transmit_Send (Trans, Resolution, DNS.Test_Data.Request,
                                    Servers (2));
      delay DNS.Config.Socket_Check_Interval;
      Assert_Pending_Transmit (Trans, Resolution);
      delay DNS.Config.Socket_Check_Interval;
      Assert_Pending_Transmit_Send (Trans, Resolution, DNS.Test_Data.Request,
                                    Servers (1));
      delay DNS.Config.Socket_Check_Interval;

      Transmit (Trans, Resolution, Next);

      Close (Trans);

      Assert (Condition => Resolution.State = Resolved,
              Message   => "Resolution isn't resolved");
      Assert (Condition => Resolution.Server.Element = Servers (2),
              Message   => "Response from unexpected server");

      Assert_Socket_Counters (Init    => 1,
                              Send    => 3,
                              Receive => 6,
                              Close   => 1);
      DNS.Socket.Mock.Clear;

      --  Remove second server for remaining tests

      Servers.Delete_Last;

      --  Invalid message (handled as if no response received)

      Test_Invalid_Message (DNS.Test_Data.Response_Loop, Servers (1));

      --  Same if the message is valid but not for the sent query

      Test_Invalid_Message (DNS.Test_Data.Response_NX, Servers (1));

      --  And again if it comes from an unknown server

      Test_Invalid_Message (DNS.Test_Data.Response,
                            DNS.Server.Create (Address => "9.9.9.9"));

      --  Socket errors

      Resolution := Prepare (Domain => Domain,
                             QType  => A);

      DNS.Socket.Mock.Socket_Data.Init_Error := 1;
      Init (Trans, Query, Servers, DNS.Socket.Instances.Sockets (1)'Access);
      Transmit (Trans, Resolution, Next);
      Close (Trans);

      Assert (Condition => Resolution.State = Failure,
              Message   => "Resolution didn't fail");

      Assert_Socket_Counters (Init  => 1,
                              Close => 1);
      DNS.Socket.Mock.Clear;

      Resolution := Prepare (Domain => Domain,
                             QType  => A);

      DNS.Socket.Mock.Socket_Data.Send_Error := 1;
      Init (Trans, Query, Servers, DNS.Socket.Instances.Sockets (1)'Access);
      Transmit (Trans, Resolution, Next);
      Close (Trans);

      Assert (Condition => Resolution.State = Failure,
              Message   => "Resolution didn't fail");

      Assert_Socket_Counters (Init    => 1,
                              Send    => 1,
                              Receive => 1,
                              Close   => 1);
      DNS.Socket.Mock.Clear;

      Resolution := Prepare (Domain => Domain,
                             QType  => A);

      DNS.Socket.Mock.Socket_Data.Receive_Error := 1;
      Init (Trans, Query, Servers, DNS.Socket.Instances.Sockets (1)'Access);
      Transmit (Trans, Resolution, Next);
      Close (Trans);

      Assert (Condition => Resolution.State = Failure,
              Message   => "Resolution didn't fail");

      Assert_Socket_Counters (Init    => 1,
                              Send    => 0,
                              Receive => 1,
                              Close   => 1);
      DNS.Socket.Mock.Clear;

--  begin read only
   end Test_Transmit;
--  end read only


--  begin read only
   procedure Test_Close (Gnattest_T : in out Test);
   procedure Test_Close_9ded1f (Gnattest_T : in out Test) renames Test_Close;
--  id:2.2/9ded1fcac9f9c726/Close/1/0/
   procedure Test_Close (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Trans   : Resolution_Transmit_Type;
      Domain  : DNS.Domain.Domain_Type
        := DNS.Domain.Validate ("www.hsr.ch");
      Query   : DNS.Message.Query.Query_Type;
      Servers : DNS.Server.List.Server_List;
   begin

      Query := DNS.Message.Query.Create (Id     => 16#8689#,
                                         Domain => Domain,
                                         QType  => A);

      Servers.Append (New_Item => DNS.Server.Create (Address => "8.8.8.8"));

      --  Mainly tested in the other tests, some special cases here
      Close (Trans);

      Assert_Socket_Counters;
      DNS.Socket.Mock.Clear;

      Init (Trans, Query, Servers, DNS.Socket.Instances.Sockets (1)'Access);
      Close (Trans);

      Assert_Socket_Counters;
      DNS.Socket.Mock.Clear;

--  begin read only
   end Test_Close;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Resolution.Transmit.Test_Data.Tests;
