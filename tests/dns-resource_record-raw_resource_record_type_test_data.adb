--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

package body DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data is

   --  Local_Raw_Resource_Record_Type : aliased GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type;
   procedure Set_Up (Gnattest_T : in out Test_Raw_Resource_Record_Type) is
   begin
     GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Resource_Record_Type_Test_Data.Resource_Record_Type_Tests.Test_Resource_Record_Type(Gnattest_T).Set_Up;
      null;
      --  Gnattest_T.Fixture := Local_Raw_Resource_Record_Type'Access;
   end Set_Up;

   procedure Tear_Down (Gnattest_T : in out Test_Raw_Resource_Record_Type) is
   begin
     GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Resource_Record_Type_Test_Data.Resource_Record_Type_Tests.Test_Resource_Record_Type(Gnattest_T).Tear_Down;
   end Tear_Down;

end DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data;
