--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Resource_Record.List.Resource_Record_List_Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Exceptions;

with DNS.Test_Data;

--  begin read only
--  end read only
package body DNS.Resource_Record.List.Resource_Record_List_Test_Data.Resource_Record_List_Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Deserialize (Gnattest_T : in out Test_Resource_Record_List);
   procedure Test_Deserialize_25a8de (Gnattest_T : in out Test_Resource_Record_List) renames Test_Deserialize;
--  id:2.2/25a8de23bd9717d2/Deserialize/1/0/
   procedure Test_Deserialize (Gnattest_T : in out Test_Resource_Record_List) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Message : Ada.Streams.Stream_Element_Array := DNS.Test_Data.Response;
      List    : Resource_Record_List;
      Length  : Ada.Streams.Stream_Element_Count;
   begin

      List := Deserialize (Message => Message,
                           Index   => Message'First + 28,
                           Count   => 2,
                           Length  => Length);
      Assert (Condition => Length = 42,
              Message   => "Length of resource records is incorrect");

      --  There are two RRs, but this should work too
      List := Deserialize (Message => Message,
                           Index   => Message'First + 28,
                           Count   => 1,
                           Length  => Length);
      Assert (Condition => Length = 26,
              Message   => "Length of resource record is incorrect");

      List := Deserialize (Message => Message,
                           Index   => Message'First + 28,
                           Count   => 0,
                           Length  => Length);
      Assert (Condition => Length = 0,
              Message   => "Reading nothing failed");

      begin
         List := Deserialize (Message => Message,
                              Index   => Message'First + 28,
                              Count   => 3,
                              Length  => Length);
         Assert (Condition => False,
                 Message   => "Incorrectly read 3 RRs from message");
      exception
          when E : Invalid_Resource_Record =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E) =
                      "Resource records in section incomplete",
                    Message   => "Unexpected message reading 3 not 2 RRs");
      end;

      --  Change CNAME to TXT, which is currently unknown
      Message (Message'First + 31) := 16#10#;
      begin
         List := Deserialize (Message => Message,
                              Index   => Message'First + 28,
                              Count   => 2,
                              Length  => Length);
         Assert (Condition => False,
                 Message   => "Incorrectly accepted unknown RRs");
      exception
         when E : Invalid_Resource_Record =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E) =
                      "Unknown resource record type",
                    Message   => "Unexpected message reading unknown RR type");
      end;

--  begin read only
   end Test_Deserialize;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Resource_Record.List.Resource_Record_List_Test_Data.Resource_Record_List_Tests;
