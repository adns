--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.


with AUnit.Test_Fixtures;

with GNATtest_Generated;

package DNS.Resolution.List.Resolution_List_Test_Data is

   type Resolution_List_Access is access all GNATtest_Generated.GNATtest_Standard.DNS.Resolution.List.Resolution_List'Class;

--  begin read only
   type Test_Resolution_List is new AUnit.Test_Fixtures.Test_Fixture
--  end read only
   with record
      Fixture : Resolution_List_Access;
   end record;

   procedure Set_Up (Gnattest_T : in out Test_Resolution_List);
   procedure Tear_Down (Gnattest_T : in out Test_Resolution_List);

end DNS.Resolution.List.Resolution_List_Test_Data;
