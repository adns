--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

package body DNS.Resource_Record.List.Resource_Record_List_Test_Data is

   Local_Resource_Record_List : aliased GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.List.Resource_Record_List;
   procedure Set_Up (Gnattest_T : in out Test_Resource_Record_List) is
   begin
      Gnattest_T.Fixture := Local_Resource_Record_List'Access;
   end Set_Up;

   procedure Tear_Down (Gnattest_T : in out Test_Resource_Record_List) is
   begin
      null;
   end Tear_Down;

end DNS.Resource_Record.List.Resource_Record_List_Test_Data;
