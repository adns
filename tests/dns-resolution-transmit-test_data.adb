--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

with Adns_Util.Logger;

package body DNS.Resolution.Transmit.Test_Data is

   procedure Set_Up (Gnattest_T : in out Test) is
      pragma Unreferenced (Gnattest_T);
   begin
      Adns_Util.Logger.Set_Log_Level (Level => Adns_Util.Logger.Emergency);
      Adns_Util.Logger.Use_Stdout;
   end Set_Up;

   procedure Tear_Down (Gnattest_T : in out Test) is
      pragma Unreferenced (Gnattest_T);
   begin
      Adns_Util.Logger.Stop;
   end Tear_Down;

end DNS.Resolution.Transmit.Test_Data;
