--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Server.IPv4_Server_Type_Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

--  begin read only
--  end read only
package body DNS.Server.IPv4_Server_Type_Test_Data.IPv4_Server_Type_Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_1_Get_Address (Gnattest_T : in out Test_IPv4_Server_Type);
   procedure Test_Get_Address_ffa850 (Gnattest_T : in out Test_IPv4_Server_Type) renames Test_1_Get_Address;
--  id:2.2/ffa850513234fc36/Get_Address/1/0/
   procedure Test_1_Get_Address (Gnattest_T : in out Test_IPv4_Server_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Anet.IPv4_Addr_Type;

      Server   : Server_Type'Class := Create ("10.1.0.1");
      Serverv4 : IPv4_Server_Type := IPv4_Server_Type (Server);
   begin

      Assert (Condition => Serverv4.Get_Address = (
              16#0a#, 16#01#, 16#00#, 16#01#),
              Message   => "IPv4 server address doesn't match");

--  begin read only
   end Test_1_Get_Address;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Server.IPv4_Server_Type_Test_Data.IPv4_Server_Type_Tests;
