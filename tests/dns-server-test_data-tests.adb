--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Server.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

--  begin read only
--  end read only
package body DNS.Server.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_1_Create (Gnattest_T : in out Test);
   procedure Test_Create_0b856c (Gnattest_T : in out Test) renames Test_1_Create;
--  id:2.2/0b856c07d04d6ecf/Create/1/0/
   procedure Test_1_Create (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Anet.IPv4_Addr_Type;
      use type Anet.IPv6_Addr_Type;
      use type Port_Type;

   begin

      declare
         Server   : Server_Type'Class := Create ("10.1.0.1");
         Serverv4 : IPv4_Server_Type := IPv4_Server_Type (Server);
      begin
         Assert (Condition => Server.Port = 53,
                 Message   => "IPv4 server's port doesn't match");
         Assert (Condition => Serverv4.IPv4 = (16#0a#, 16#01#, 16#00#, 16#01#),
                 Message   => "IPv4 server address doesn't match");
      end;

      declare
         Server   : Server_Type'Class := Create ("2001:db8::a01", 1053);
         Serverv6 : IPv6_Server_Type := IPv6_Server_Type (Server);
      begin
         Assert (Condition => Server.Port = 1053,
                 Message   => "IPv6 server's port doesn't match");
         Assert (Condition => Serverv6.IPv6 = (16#20#, 16#01#, 16#0d#, 16#b8#,
                 16#00#, 16#00#, 16#00#, 16#00#,
                 16#00#, 16#00#, 16#00#, 16#00#,
                 16#00#, 16#00#, 16#0a#, 16#01#),
                 Message   => "IPv6 server address doesn't match");
      end;

      begin
         declare
            Failure : Server_Type'Class := Create ("asdf");
         begin
            Assert (Condition => False,
                    Message   => "Server address was unexpectedly valid");
         end;
      exception
         when Constraint_Error => null;
      end;

--  begin read only
   end Test_1_Create;
--  end read only


--  begin read only
   procedure Test_Get_Port (Gnattest_T : in out Test);
   procedure Test_Get_Port_fd62b6 (Gnattest_T : in out Test) renames Test_Get_Port;
--  id:2.2/fd62b60a866b70d9/Get_Port/1/0/
   procedure Test_Get_Port (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Port_Type;

      Server : Server_Type'Class := Create ("10.1.0.1", 12345);
   begin

      Assert (Condition => Server.Get_Port = 12345,
              Message   => "Returned server port doesn't match");

--  begin read only
   end Test_Get_Port;
--  end read only


--  begin read only
   procedure Test_2_Create (Gnattest_T : in out Test);
   procedure Test_Create_29f2eb (Gnattest_T : in out Test) renames Test_2_Create;
--  id:2.2/29f2ebef398ca253/Create/0/0/
   procedure Test_2_Create (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Anet.IPv4_Addr_Type;
      use type Port_Type;

      IPv4     : Anet.IPv4_Addr_Type := (16#0a#, 16#01#, 16#00#, 16#01#);
      Server   : Server_Type'Class := Create (IPv4, 53);
      Serverv4 : IPv4_Server_Type := IPv4_Server_Type (Server);
   begin

      Assert (Condition => Serverv4.IPv4 = IPv4,
              Message   => "Address doesn't match");

      Assert (Condition => Serverv4.Port = 53,
              Message   => "Port doesn't match");

--  begin read only
   end Test_2_Create;
--  end read only


--  begin read only
   procedure Test_3_Create (Gnattest_T : in out Test);
   procedure Test_Create_a52651 (Gnattest_T : in out Test) renames Test_3_Create;
--  id:2.2/a5265141f4e7a236/Create/0/0/
   procedure Test_3_Create (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Anet.IPv6_Addr_Type;
      use type Port_Type;

      IPv6     : Anet.IPv6_Addr_Type := (16#20#, 16#01#, 16#0d#, 16#b8#,
                                         16#00#, 16#00#, 16#00#, 16#00#,
                                         16#00#, 16#00#, 16#00#, 16#00#,
                                         16#00#, 16#00#, 16#0a#, 16#01#);
      Server   : Server_Type'Class := Create (IPv6, 53);
      Serverv6 : IPv6_Server_Type := IPv6_Server_Type (Server);
   begin

      Assert (Condition => Serverv6.IPv6 = IPv6,
              Message   => "Address doesn't match");

      Assert (Condition => Serverv6.Port = 53,
              Message   => "Port doesn't match");

--  begin read only
   end Test_3_Create;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Server.Test_Data.Tests;
