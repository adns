--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Socket.Socket_Type_Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

--  begin read only
--  end read only
package body DNS.Socket.Socket_Type_Test_Data.Socket_Type_Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Init (Gnattest_T : in out Test_Socket_Type);
   procedure Test_Init_812d38 (Gnattest_T : in out Test_Socket_Type) renames Test_Init;
--  id:2.2/812d387e17e4fb12/Init/1/0/
   procedure Test_Init (Gnattest_T : in out Test_Socket_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin

      --  Difficult to test due to the dependency on Anet sockets.
      null;

--  begin read only
   end Test_Init;
--  end read only


--  begin read only
   procedure Test_Close (Gnattest_T : in out Test_Socket_Type);
   procedure Test_Close_5dcacf (Gnattest_T : in out Test_Socket_Type) renames Test_Close;
--  id:2.2/5dcacff2a5d5bde5/Close/1/0/
   procedure Test_Close (Gnattest_T : in out Test_Socket_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin

      --  Difficult to test due to the dependency on Anet sockets.
      null;

--  begin read only
   end Test_Close;
--  end read only


--  begin read only
   procedure Test_Send (Gnattest_T : in out Test_Socket_Type);
   procedure Test_Send_b70797 (Gnattest_T : in out Test_Socket_Type) renames Test_Send;
--  id:2.2/b7079780981f3120/Send/1/0/
   procedure Test_Send (Gnattest_T : in out Test_Socket_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin

      --  Difficult to test due to the dependency on Anet sockets.
      null;

--  begin read only
   end Test_Send;
--  end read only


--  begin read only
   procedure Test_Receive (Gnattest_T : in out Test_Socket_Type);
   procedure Test_Receive_a084a4 (Gnattest_T : in out Test_Socket_Type) renames Test_Receive;
--  id:2.2/a084a41f028969a7/Receive/1/0/
   procedure Test_Receive (Gnattest_T : in out Test_Socket_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin

      --  Difficult to test due to the dependency on Anet sockets.
      null;

--  begin read only
   end Test_Receive;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Socket.Socket_Type_Test_Data.Socket_Type_Tests;
