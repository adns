--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Message.Query.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Anet;
with DNS.Domain;
with DNS.Test_Data;

--  begin read only
--  end read only
package body DNS.Message.Query.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_1_Create (Gnattest_T : in out Test);
   procedure Test_Create_3468ca (Gnattest_T : in out Test) renames Test_1_Create;
--  id:2.2/3468ca96fd370bb0/Create/1/0/
   procedure Test_1_Create (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Anet.Double_Byte;
      use type DNS.Domain.Domain_Type;
   begin

      for Test_Pattern of Test_Patterns loop
         declare
            Query : Query_Type := Create (Id     => Test_Pattern.Id,
                                          Domain => Test_Pattern.Domain,
                                          QType  => Test_Pattern.QType);
         begin
            Assert (Condition => Query.Id = Test_Pattern.Id,
                    Message   => "Id doesn't match");
            Assert (Condition => Query.Domain = Test_Pattern.Domain,
                    Message   => "Domain doesn't match");
            Assert (Condition => Query.QType = Test_Pattern.QType,
                    Message   => "QType doesn't match");
         end;
      end loop;

--  begin read only
   end Test_1_Create;
--  end read only


--  begin read only
   procedure Test_2_Create (Gnattest_T : in out Test);
   procedure Test_Create_7801a8 (Gnattest_T : in out Test) renames Test_2_Create;
--  id:2.2/7801a8513f435b5e/Create/0/0/
   procedure Test_2_Create (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Anet.Double_Byte;
      use type DNS.Domain.Domain_Type;
   begin

      for Test_Pattern of Test_Patterns loop
         declare
            Query : Query_Type := Create (Id     => Test_Pattern.Id,
                                          Domain => Test_Pattern.Domain,
                                          QType  => Test_Pattern.QType);
            Other : Query_Type := Create (Id     => 42,
                                          Query  => Query);
         begin
            Assert (Condition => Query.Id = Test_Pattern.Id,
                    Message   => "Id doesn't match");
            Assert (Condition => Other.Id = 42,
                    Message   => "Id in derived query doesn't match");
            Assert (Condition => Other.Domain = Test_Pattern.Domain,
                    Message   => "Domain doesn't match");
            Assert (Condition => Other.QType = Test_Pattern.QType,
                    Message   => "QType doesn't match");
         end;
      end loop;

--  begin read only
   end Test_2_Create;
--  end read only


--  begin read only
   procedure Test_Serialize (Gnattest_T : in out Test);
   procedure Test_Serialize_fb4843 (Gnattest_T : in out Test) renames Test_Serialize;
--  id:2.2/fb4843143446e9ad/Serialize/1/0/
   procedure Test_Serialize (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Streams;
      use Ada.Strings.Unbounded;

   begin

      for Test_Pattern of Test_Patterns loop
         declare
            Query    : Query_Type := Create (Id     => Test_Pattern.Id,
                                             Domain => Test_Pattern.Domain,
                                             QType  => Test_Pattern.QType);
            Encoding : Stream_Element_Array := Serialize (Query);
         begin
            Assert (Condition => Test_Pattern.Encoding = Encoding,
                    Message   => "Serialization incorrect for '" &
                      DNS.Domain.Get_Domain (Test_Pattern.Domain) & "': " &
                      Anet.To_Hex (Encoding));
         end;
      end loop;

--  begin read only
   end Test_Serialize;
--  end read only


--  begin read only
   procedure Test_1_Matches (Gnattest_T : in out Test);
   procedure Test_Matches_450069 (Gnattest_T : in out Test) renames Test_1_Matches;
--  id:2.2/4500692abbf61852/Matches/1/0/
   procedure Test_1_Matches (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Message      : Ada.Streams.Stream_Element_Array
        := DNS.Test_Data.Response;
      Unequal      : Ada.Streams.Stream_Element_Array (1 .. Message'Length);
      Response     : Response_Type;
      Query        : Query_Type;
      Test_Pattern : Test_Pattern_Type := Test_Patterns (0);
   begin

      Query := Create (Id     => Test_Pattern.Id,
                       Domain => Test_Pattern.Domain,
                       QType  => Test_Pattern.QType);
      Response := Deserialize (Message);
      Assert (Condition => Matches (Query, Response),
              Message   => "Response doesn't match");

      Unequal := Message;
      Unequal (1) := 16#01#;
      Response := Deserialize (Unequal);
      Assert (Condition => not Matches (Query, Response),
              Message   => "Response with different ID incorrectly matches");

      Unequal := Message;
      Unequal (16) := 16#78#;
      Response := Deserialize (Unequal);
      Assert (Condition => not Matches (Query, Response),
              Message   => "Response with wrong name incorrectly matches");

      Unequal := Message;
      Unequal (26) := 16#1c#;
      Response := Deserialize (Unequal);
      Assert (Condition => not Matches (Query, Response),
              Message   => "Response with different type incorrectly matches");

--  begin read only
   end Test_1_Matches;
--  end read only


--  begin read only
   procedure Test_2_Matches (Gnattest_T : in out Test);
   procedure Test_Matches_912389 (Gnattest_T : in out Test) renames Test_2_Matches;
--  id:2.2/912389cca53987fb/Matches/0/0/
   procedure Test_2_Matches (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Query        : Query_Type;
      Other        : Query_Type;
      Test_Pattern : Test_Pattern_Type := Test_Patterns (0);
   begin

      Query := Create (Id     => Test_Pattern.Id,
                       Domain => Test_Pattern.Domain,
                       QType  => Test_Pattern.QType);
      Other := Create (Id     => 0,
                       Domain => Test_Pattern.Domain,
                       QType  => Test_Pattern.QType);

      Assert (Condition => Matches (Query, Other),
              Message   => "Query doesn't match");

--  begin read only
   end Test_2_Matches;
--  end read only


--  begin read only
   procedure Test_3_Matches (Gnattest_T : in out Test);
   procedure Test_Matches_c3aeaf (Gnattest_T : in out Test) renames Test_3_Matches;
--  id:2.2/c3aeaf234154a95a/Matches/0/0/
   procedure Test_3_Matches (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use DNS.Domain;

      Query        : Query_Type;
      Test_Pattern : Test_Pattern_Type := Test_Patterns (0);
   begin

      Query := Create (Id     => Test_Pattern.Id,
                       Domain => Test_Pattern.Domain,
                       QType  => Test_Pattern.QType);
      Assert (Condition => Matches (Query, Validate ("www.hsr.ch."), A),
              Message   => "Query doesn't match");

      Assert (Condition => not Matches (Query, Validate ("hsr.ch."), A),
              Message   => "Query unexpectedly matches wrong domain name");

      Assert (Condition => not Matches (Query, Validate ("www.hsr.ch."), AAAA),
              Message   => "Query unexpectedly matches wrong RR type");

--  begin read only
   end Test_3_Matches;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Message.Query.Test_Data.Tests;
