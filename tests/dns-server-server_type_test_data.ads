--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.


with AUnit.Test_Fixtures;

with GNATtest_Generated;

package DNS.Server.Server_Type_Test_Data is

   type Server_Type_Access is access all GNATtest_Generated.GNATtest_Standard.DNS.Server.Server_Type'Class;

--  begin read only
   type Test_Server_Type is new AUnit.Test_Fixtures.Test_Fixture
--  end read only
   with record
      Fixture : Server_Type_Access;
   end record;

   procedure Set_Up (Gnattest_T : in out Test_Server_Type);
   procedure Tear_Down (Gnattest_T : in out Test_Server_Type);

end DNS.Server.Server_Type_Test_Data;
