--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

package body DNS.Resource_Record.CNAME_Resource_Record_Type_Test_Data is

   -------------------------------------------------------------------------

   procedure Add
     (CName   : String;
      Valid   : Boolean;
      Offset  : Ada.Streams.Stream_Element_Offset;
      Message : Ada.Streams.Stream_Element_Array)
   is
      CN      : constant Ada.Strings.Unbounded.Unbounded_String
        := Ada.Strings.Unbounded.To_Unbounded_String (CName);
      Pattern : constant Test_Pattern_Type
        := (Message'Length, CN, Valid, Offset, Message);
   begin
      Test_Pattern_Vecs.Append (Test_Patterns, Pattern);
   end Add;

   -------------------------------------------------------------------------

   procedure Set_Up (Gnattest_T : in out Test_CNAME_Resource_Record_Type) is
   begin
      GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests.Set_Up
        (GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests.Test_Raw_Resource_Record_Type (Gnattest_T));

      Add ("a6.", True, 1,
           (16#00#, 16#00#, 16#05#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#04#, 16#02#, 16#61#, 16#36#, 16#00#));
      Add ("6a.", True, 1,
           (16#00#, 16#00#, 16#05#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#04#, 16#02#, 16#36#, 16#61#, 16#00#));
      --  Invalid CNAME (starts with a hyphen)
      Add ("", False, 1,
           (16#00#, 16#00#, 16#05#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#04#, 16#02#, 16#2d#, 16#61#, 16#00#));
      --  Invalid CNAME (longer than the actual RR)
      Add ("", False, 1,
           (16#00#, 16#00#, 16#05#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#04#, 16#03#, 16#61#, 16#36#, 16#36#, 16#00#));
      --  Real world example
      Add ("lb-ext-web1.hsr.ch.", True, 29,
           (16#86#, 16#89#, 16#81#, 16#80#, 16#00#, 16#01#, 16#00#, 16#02#,
            16#00#, 16#00#, 16#00#, 16#00#, 16#03#, 16#77#, 16#77#, 16#77#,
            16#03#, 16#68#, 16#73#, 16#72#, 16#02#, 16#63#, 16#68#, 16#00#,
            16#00#, 16#01#, 16#00#, 16#01#, 16#c0#, 16#0c#, 16#00#, 16#05#,
            16#00#, 16#01#, 16#00#, 16#00#, 16#10#, 16#f1#, 16#00#, 16#0e#,
            16#0b#, 16#6c#, 16#62#, 16#2d#, 16#65#, 16#78#, 16#74#, 16#2d#,
            16#77#, 16#65#, 16#62#, 16#31#, 16#c0#, 16#10#, 16#c0#, 16#28#,
            16#00#, 16#01#, 16#00#, 16#01#, 16#00#, 16#00#, 16#10#, 16#f1#,
            16#00#, 16#04#, 16#98#, 16#60#, 16#24#, 16#64#));
   end Set_Up;

   procedure Tear_Down (Gnattest_T : in out Test_CNAME_Resource_Record_Type) is
   begin
      GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests.Tear_Down
        (GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests.Test_Raw_Resource_Record_Type (Gnattest_T));
   end Tear_Down;

end DNS.Resource_Record.CNAME_Resource_Record_Type_Test_Data;
