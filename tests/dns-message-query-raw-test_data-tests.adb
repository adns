--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Message.Query.Raw.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Exceptions;

--  begin read only
--  end read only
package body DNS.Message.Query.Raw.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Validate (Gnattest_T : in out Test);
   procedure Test_Validate_73d0cf (Gnattest_T : in out Test) renames Test_Validate;
--  id:2.2/73d0cf4149470526/Validate/1/0/
   procedure Test_Validate (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      QBuf     : Raw_Question_Buffer_Type;
      Question : Raw_Question_Type;

      for Question'Address use QBuf'Address;
   begin

      QBuf := (16#00#, 16#01#, 16#00#, 16#01#);
      Validate (Question);
      Assert (Condition => Question.QType = A,
              Message   => "Question not valid");

      QBuf := (16#00#, 16#42#, 16#00#, 16#01#);
      begin
         Validate (Question);
         Assert (Condition => False, Message => "QType should be invalid");
      exception
         when E : Invalid_Message =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E) =
                      "Invalid question type",
                    Message   => "Unexpected message for invalid QType");
      end;

      QBuf := (16#00#, 16#01#, 16#00#, 16#02#);
      begin
         Validate (Question);
         Assert (Condition => False, Message => "QClass should be invalid");
      exception
         when E : Invalid_Message =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E) =
                      "Invalid question class",
                    Message   => "Unexpected message for invalid QClass");
      end;

--  begin read only
   end Test_Validate;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Message.Query.Raw.Test_Data.Tests;
