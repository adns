--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

with Ada.Containers.Indefinite_Vectors;
with Ada.Streams;
with Ada.Strings.Unbounded;

with DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests;

with GNATtest_Generated;

package DNS.Resource_Record.CNAME_Resource_Record_Type_Test_Data is

   --  begin read only
   type Test_CNAME_Resource_Record_Type is new
     GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests.Test_Raw_Resource_Record_Type
   --  end read only
   with null record;

   type Test_Pattern_Type
     (Msg_Len  : Ada.Streams.Stream_Element_Offset) is
      record
         CName   : Ada.Strings.Unbounded.Unbounded_String;
         Valid   : Boolean;
         Offset  : Ada.Streams.Stream_Element_Offset;
         Message : Ada.Streams.Stream_Element_Array (1 .. Msg_Len);
      end record;

   package Test_Pattern_Vecs is new Ada.Containers.Indefinite_Vectors
     (Element_Type => Test_Pattern_Type,
      Index_Type   => Natural);

   Test_Patterns : Test_Pattern_Vecs.Vector;

   procedure Set_Up (Gnattest_T : in out Test_CNAME_Resource_Record_Type);
   procedure Tear_Down (Gnattest_T : in out Test_CNAME_Resource_Record_Type);

end DNS.Resource_Record.CNAME_Resource_Record_Type_Test_Data;
