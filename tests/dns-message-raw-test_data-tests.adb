--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Message.Raw.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Exceptions;

with DNS.Test_Data;

--  begin read only
--  end read only
package body DNS.Message.Raw.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Validate (Gnattest_T : in out Test);
   procedure Test_Validate_5e779d (Gnattest_T : in out Test) renames Test_Validate;
--  id:2.2/5e779d9fba84f905/Validate/1/0/
   procedure Test_Validate (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Msg    : Ada.Streams.Stream_Element_Array := DNS.Test_Data.Response;
      HBuf   : Raw_Header_Buffer_Type;
      Header : Raw_Header_Type
        with Address => HBuf'Address;
   begin

      HBuf := Msg (Msg'First .. Msg'First + HBuf'Length - 1);
      Validate (Header);
      Assert (Condition => Header.RCode = Rc_No_Error,
              Message   => "Header not valid");

      HBuf (HBuf'First + 2) := 16#c1#;
      begin
         Validate (Header);
         Assert (Condition => False, Message => "OpCode should be invalid");
      exception
         when E : Invalid_Message =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E) =
                      "Invalid OpCode",
                    Message   => "Unexpected message for invalid OpCode");
      end;

      HBuf := Msg (Msg'First .. Msg'First + HBuf'Length - 1);
      HBuf (HBuf'First + 3) := 16#88#;
      begin
         Validate (Header);
         Assert (Condition => False, Message => "RCode should be invalid");
      exception
         when E : Invalid_Message =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E) =
                      "Invalid RCode",
                    Message   => "Unexpected message for invalid RCode");
      end;

--  begin read only
   end Test_Validate;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Message.Raw.Test_Data.Tests;
