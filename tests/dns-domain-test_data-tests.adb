--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Domain.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Assertions;
with Anet;

--  begin read only
--  end read only
package body DNS.Domain.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_1_Validate (Gnattest_T : in out Test);
   procedure Test_Validate_079e5c (Gnattest_T : in out Test) renames Test_1_Validate;
--  id:2.2/079e5c659f4dc4b5/Validate/1/0/
   procedure Test_1_Validate (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Strings.Unbounded;

      Domain : Domain_Type;
   begin

      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            Domain := Validate (Value => Test_Pattern.Pattern);
            Assert (Expected => To_String (Test_Pattern.Pattern)
                    & (if Test_Pattern.Absolute then "" else "."),
                    Actual   => To_String (Domain.Name),
                    Message  => "Domain not valid");
         else
            begin
               Domain := Validate (Value => Test_Pattern.Pattern);
               Assert (Condition => False,
                       Message   => "Domain incorrectly valid");
            exception
               when Invalid_Domain => null;
            end;
         end if;
      end loop;

--  begin read only
   end Test_1_Validate;
--  end read only


--  begin read only
   procedure Test_2_Validate (Gnattest_T : in out Test);
   procedure Test_Validate_4483e7 (Gnattest_T : in out Test) renames Test_2_Validate;
--  id:2.2/4483e7e21ee1c458/Validate/0/0/
   procedure Test_2_Validate (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Strings.Unbounded;

      Domain : Domain_Type;
   begin

      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            Domain := Validate (Value => To_String (Test_Pattern.Pattern));
            Assert (Expected => To_String (Test_Pattern.Pattern)
                    & (if Test_Pattern.Absolute then "" else "."),
                    Actual   => To_String (Domain.Name),
                    Message  => "Domain not valid");
         else
            begin
               Domain := Validate (Value => To_String (Test_Pattern.Pattern));
               Assert (Condition => False,
                       Message   => "Domain incorrectly valid");
            exception
               when Invalid_Domain => null;
            end;
         end if;
      end loop;

--  begin read only
   end Test_2_Validate;
--  end read only


--  begin read only
   procedure Test_Serialize (Gnattest_T : in out Test);
   procedure Test_Serialize_893bb5 (Gnattest_T : in out Test) renames Test_Serialize;
--  id:2.2/893bb5eb097401ec/Serialize/1/0/
   procedure Test_Serialize (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Streams;
      use Ada.Strings.Unbounded;

   begin
      for Test_Pattern of Test_Patterns loop
         declare
            Domain : Domain_Type;
         begin
            if Test_Pattern.Valid then
               Domain := Validate (Value => Test_Pattern.Pattern);
               declare
                  Labels : constant Stream_Element_Array := Serialize (Domain);
               begin
                  Assert (Condition => Test_Pattern.Encoding = Labels,
                          Message   => "Serialization incorrect for '" &
                            To_String (Test_Pattern.Pattern) & "': " &
                            Anet.To_Hex (Labels));
               end;
            end if;
         end;
      end loop;

--  begin read only
   end Test_Serialize;
--  end read only


--  begin read only
   procedure Test_Deserialize (Gnattest_T : in out Test);
   procedure Test_Deserialize_4df1d2 (Gnattest_T : in out Test) renames Test_Deserialize;
--  id:2.2/4df1d2db4957bd90/Deserialize/1/0/
   procedure Test_Deserialize (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Streams;
      use Ada.Strings.Unbounded;

      Domain : Domain_Type;
      Length : Stream_Element_Count;
   begin
      --  Test pre-condition
      begin
         Domain := Deserialize (Message => (1 .. 0 => 0),
                                Index   => 1,
                                Length  => Length);
         Assert (Condition => False,
                 Message   => "Domain incorrectly valid");
      exception
         when Ada.Assertions.Assertion_Error => null;
      end;

      --  Simple cases, no compression
      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            Domain := Deserialize (Message => Test_Pattern.Encoding,
                                   Index  => 1, Length  => Length);
            Assert (Expected => To_String (Test_Pattern.Pattern)
                    & (if Test_Pattern.Absolute then "" else "."),
                    Actual   => To_String (Domain.Name),
                    Message  => "Deserialization invalid");
            Assert (Condition => Length = Test_Pattern.Encoding'Length,
                    Message   => "Length incorrect - got " & Length'Image &
                      " instead of " & Test_Pattern.Encoding'Length'Image);
         end if;
      end loop;

      --  Compressed domain names
      for Test_Pattern of Compressed_Patterns loop
         if Test_Pattern.Valid then
            Domain := Deserialize (Message => Test_Pattern.Encoding,
                                   Index   => Test_Pattern.Offset,
                                   Length  => Length);
            Assert (Expected => To_String (Test_Pattern.Pattern),
                    Actual   => To_String (Domain.Name),
                    Message  => "Deserialization invalid");
            Assert (Condition => Length = Test_Pattern.Length,
                    Message   => "Length incorrect - got " & Length'Image &
                      " instead of " & Test_Pattern.Length'Image);
         else
            begin
               Domain := Deserialize (Message => Test_Pattern.Encoding,
                                      Index   => Test_Pattern.Offset,
                                      Length  => Length);
               Assert (Condition => False,
                       Message   => "Domain incorrectly valid");
            exception
               when Invalid_Domain => null;
            end;
         end if;
      end loop;

--  begin read only
   end Test_Deserialize;
--  end read only


--  begin read only
   procedure Test_Get_Domain (Gnattest_T : in out Test);
   procedure Test_Get_Domain_4e4d95 (Gnattest_T : in out Test) renames Test_Get_Domain;
--  id:2.2/4e4d95fec2def385/Get_Domain/1/0/
   procedure Test_Get_Domain (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Strings.Unbounded;
   begin

      for Test_Pattern of Test_Patterns loop
         declare
            Domain : Domain_Type;
         begin
            if Test_Pattern.Valid then
               Domain := Validate (Value => Test_Pattern.Pattern);
               Assert (Expected => To_String (Test_Pattern.Pattern)
                       & (if Test_Pattern.Absolute then "" else "."),
                       Actual   => Get_Domain (Domain),
                       Message  => "Domain not available");
            end if;
         end;
      end loop;

--  begin read only
   end Test_Get_Domain;
--  end read only


--  begin read only
   procedure Test_Equal (Gnattest_T : in out Test);
   procedure Test_Equal_c5cb72 (Gnattest_T : in out Test) renames Test_Equal;
--  id:2.2/c5cb729c9dbc2fea/Equal/1/0/
   procedure Test_Equal (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Orig  : Domain_Type := Validate ("www.hsr.ch");
      Upper : Domain_Type := Validate ("WWW.HSR.CH");
      Mixed : Domain_Type := Validate ("Www.HsR.Ch");
   begin

      Assert (Condition => Orig = Orig,
              Message   => "Original domain name is not equal");

      Assert (Condition => Orig = Upper,
              Message   => "Upper case domain name is not equal");

      Assert (Condition => Orig = Mixed,
              Message   => "Mixed case domain name is not equal");

--  begin read only
   end Test_Equal;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Domain.Test_Data.Tests;
