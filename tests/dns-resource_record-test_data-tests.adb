--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Resource_Record.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

--  begin read only
--  end read only
package body DNS.Resource_Record.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Get_Domain (Gnattest_T : in out Test);
   procedure Test_Get_Domain_488e1c (Gnattest_T : in out Test) renames Test_Get_Domain;
--  id:2.2/488e1cd589530e91/Get_Domain/1/0/
   procedure Test_Get_Domain (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Strings.Unbounded;

      Length : Ada.Streams.Stream_Element_Count;
   begin

      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            declare
               RR : Resource_Record_Type'Class
                 := Deserialize (Message => Test_Pattern.Message,
                                 Index   => Test_Pattern.Offset,
                                 Length  => Length);
            begin
               Assert (Expected => To_String (Test_Pattern.Domain),
                       Actual   => DNS.Domain.Get_Domain (Get_Domain (RR)),
                       Message  => "Domain in resource record not valid");
            end;
         end if;
      end loop;

--  begin read only
   end Test_Get_Domain;
--  end read only


--  begin read only
   procedure Test_Get_Type (Gnattest_T : in out Test);
   procedure Test_Get_Type_3e9b31 (Gnattest_T : in out Test) renames Test_Get_Type;
--  id:2.2/3e9b310b8f2874d6/Get_Type/1/0/
   procedure Test_Get_Type (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Length : Ada.Streams.Stream_Element_Count;
   begin

      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            declare
               RR : Resource_Record_Type'Class
                 := Deserialize (Message => Test_Pattern.Message,
                                 Index   => Test_Pattern.Offset,
                                 Length  => Length);
            begin
               Assert (Condition => Get_Type (RR) = Test_Pattern.RType,
                       Message   => "Resource record class doesn't match");
            end;
         end if;
      end loop;

--  begin read only
   end Test_Get_Type;
--  end read only


--  begin read only
   procedure Test_Get_Class (Gnattest_T : in out Test);
   procedure Test_Get_Class_9fd1b2 (Gnattest_T : in out Test) renames Test_Get_Class;
--  id:2.2/9fd1b22bcb6ea1d3/Get_Class/1/0/
   procedure Test_Get_Class (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Length : Ada.Streams.Stream_Element_Count;
   begin

      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            declare
               RR : Resource_Record_Type'Class
                 := Deserialize (Message => Test_Pattern.Message,
                                 Index   => Test_Pattern.Offset,
                                 Length  => Length);
            begin
               Assert (Condition => Get_Class (RR) = IN_Class,
                       Message   => "Resource record class doesn't match");
            end;
         end if;
      end loop;

--  begin read only
   end Test_Get_Class;
--  end read only


--  begin read only
   procedure Test_Get_Data (Gnattest_T : in out Test);
   procedure Test_Get_Data_58e4e4 (Gnattest_T : in out Test) renames Test_Get_Data;
--  id:2.2/58e4e4e0b9bbee8e/Get_Data/1/0/
   procedure Test_Get_Data (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Streams;

      Length : Ada.Streams.Stream_Element_Count;
   begin

      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            declare
               RR : Resource_Record_Type'Class
                 := Deserialize (Message => Test_Pattern.Message,
                                 Index   => Test_Pattern.Offset,
                                 Length  => Length);
            begin
               Assert (Condition => Get_Data (RR) = Test_Pattern.Data,
                       Message   => "Data in resource record doesn't match");
            end;
         end if;
      end loop;

--  begin read only
   end Test_Get_Data;
--  end read only


--  begin read only
   procedure Test_Deserialize (Gnattest_T : in out Test);
   procedure Test_Deserialize_c47c8b (Gnattest_T : in out Test) renames Test_Deserialize;
--  id:2.2/c47c8b73fab877f7/Deserialize/1/0/
   procedure Test_Deserialize (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Streams;
      use Ada.Strings.Unbounded;

      Length : Ada.Streams.Stream_Element_Count;
   begin

      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            declare
               RR : Resource_Record_Type'Class
                 := Deserialize (Message => Test_Pattern.Message,
                                 Index   => Test_Pattern.Offset,
                                 Length  => Length);
            begin
               Assert (Expected => To_String (Test_Pattern.Domain),
                       Actual   => DNS.Domain.Get_Domain (RR.Domain),
                       Message  => "Domain in resource record not valid");
               Assert (Condition => RR.RType = Test_Pattern.RType,
                       Message   => "Resource record type doesn't match");
               Assert (Condition => RR.RClass = IN_Class,
                       Message   => "Resource record class doesn't match");
               Assert (Condition => RR.Data = Test_Pattern.Data,
                       Message   => "Data in resource record doesn't match");
               Assert (Condition => Length = Test_Pattern.Length,
                       Message   => "Resource record length doesn't match");
            end;
         else
            begin
               declare
                  RR : Resource_Record_Type'Class
                    := Deserialize (Message => Test_Pattern.Message,
                                    Index   => Test_Pattern.Offset,
                                    Length  => Length);
               begin
                  Assert (Condition => False,
                          Message   => "Resource record incorrectly valid");
               end;
            exception
               when Invalid_Resource_Record => null;
            end;
         end if;
      end loop;

--  begin read only
   end Test_Deserialize;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Resource_Record.Test_Data.Tests;
