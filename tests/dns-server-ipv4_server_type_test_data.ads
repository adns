--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

with DNS.Server.Server_Type_Test_Data.Server_Type_Tests;

with GNATtest_Generated;

package DNS.Server.IPv4_Server_Type_Test_Data is

--  begin read only
   type Test_IPv4_Server_Type is new
     GNATtest_Generated.GNATtest_Standard.DNS.Server.Server_Type_Test_Data.Server_Type_Tests.Test_Server_Type
--  end read only
   with null record;

   procedure Set_Up (Gnattest_T : in out Test_IPv4_Server_Type);
   procedure Tear_Down (Gnattest_T : in out Test_IPv4_Server_Type);

end DNS.Server.IPv4_Server_Type_Test_Data;
