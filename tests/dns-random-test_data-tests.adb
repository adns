--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Random.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

--  begin read only
--  end read only
package body DNS.Random.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Get (Gnattest_T : in out Test);
   procedure Test_Get_d92bd9 (Gnattest_T : in out Test) renames Test_Get;
--  id:2.2/d92bd93704599cb3/Get/1/0/
   procedure Test_Get (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Anet.Double_Byte;

      Data : Anet.Double_Byte;
   begin

      Data := Random.Get;

      Assert (Condition => Data /= 0,
              Message   => "Random data is 0");
      Assert (Condition => Data /= Random.Get,
              Message   => "Not really random");

--  begin read only
   end Test_Get;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Random.Test_Data.Tests;
