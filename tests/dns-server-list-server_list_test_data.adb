--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

package body DNS.Server.List.Server_List_Test_Data is

   Local_Server_List : aliased GNATtest_Generated.GNATtest_Standard.DNS.Server.List.Server_List;
   procedure Set_Up (Gnattest_T : in out Test_Server_List) is
   begin
      Gnattest_T.Fixture := Local_Server_List'Access;
   end Set_Up;

   procedure Tear_Down (Gnattest_T : in out Test_Server_List) is
   begin
      null;
   end Tear_Down;

end DNS.Server.List.Server_List_Test_Data;
