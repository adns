--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Resolution.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Assertions;

with DNS.Domain;
with DNS.Message.Query;

--  begin read only
--  end read only
package body DNS.Resolution.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

   Domain : DNS.Domain.Domain_Type := DNS.Domain.Validate ("www.hsr.ch");

   Query : DNS.Message.Query.Query_Type
     := DNS.Message.Query.Create (Id     => 0,
                                  Domain => Domain,
                                  QType  => A);

   Response : DNS.Message.Response_Type
     := DNS.Message.Deserialize
       ((16#86#, 16#89#, 16#81#, 16#80#, 16#00#, 16#01#, 16#00#, 16#02#,
        16#00#, 16#00#, 16#00#, 16#00#, 16#03#, 16#77#, 16#77#, 16#77#,
        16#03#, 16#68#, 16#73#, 16#72#, 16#02#, 16#63#, 16#68#, 16#00#,
        16#00#, 16#01#, 16#00#, 16#01#, 16#c0#, 16#0c#, 16#00#, 16#05#,
        16#00#, 16#01#, 16#00#, 16#00#, 16#10#, 16#f1#, 16#00#, 16#0e#,
        16#0b#, 16#6c#, 16#62#, 16#2d#, 16#65#, 16#78#, 16#74#, 16#2d#,
        16#77#, 16#65#, 16#62#, 16#31#, 16#c0#, 16#10#, 16#c0#, 16#28#,
        16#00#, 16#01#, 16#00#, 16#01#, 16#00#, 16#00#, 16#10#, 16#f1#,
        16#00#, 16#04#, 16#98#, 16#60#, 16#24#, 16#64#));

   Server : DNS.Server.Server_Type'Class := DNS.Server.Create ("8.8.8.8");

--  begin read only
--  end read only

--  begin read only
   procedure Test_Prepare (Gnattest_T : in out Test);
   procedure Test_Prepare_b1d826 (Gnattest_T : in out Test) renames Test_Prepare;
--  id:2.2/b1d8262694df0e84/Prepare/1/0/
   procedure Test_Prepare (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Resolution : Resolution_Type := Prepare (Domain => Domain,
                                               QType  => A);
   begin

      Assert (Condition => Resolution.State = Pending,
              Message   => "State is not pending");

      Assert (Condition => DNS.Message.Query.Matches
              (Resolution.Query, Domain, A),
              Message   => "State is not pending");

--  begin read only
   end Test_Prepare;
--  end read only


--  begin read only
   procedure Test_Get_State (Gnattest_T : in out Test);
   procedure Test_Get_State_f95513 (Gnattest_T : in out Test) renames Test_Get_State;
--  id:2.2/f95513462f08f917/Get_State/1/0/
   procedure Test_Get_State (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Resolution : Resolution_Type := (State => Pending,
                                       Query => Query);
   begin

      Assert (Condition => Get_State (Resolution => Resolution) = Pending,
              Message   => "State is not pending");

      Resolution := (State    => Resolved,
                     Query    => Query,
                     Response => Response,
                     Server   => DNS.Resolution.SH.To_Holder (Server));

      Assert (Condition => Get_State (Resolution => Resolution) = Resolved,
              Message   => "State is not resolved");

--  begin read only
   end Test_Get_State;
--  end read only


--  begin read only
   procedure Test_Get_Response (Gnattest_T : in out Test);
   procedure Test_Get_Response_604023 (Gnattest_T : in out Test) renames Test_Get_Response;
--  id:2.2/604023e5fc594297/Get_Response/1/0/
   procedure Test_Get_Response (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type DNS.Message.Response_Type;

      Resolution : Resolution_Type := (State => Pending,
                                       Query => Query);
      R          : DNS.Message.Response_Type;
   begin

      begin
         R := Get_Response (Resolution => Resolution);
         Assert (Condition => False,
                 Message   => "Incorrectly got a response");
      exception
         when Ada.Assertions.Assertion_Error => null;
      end;

      Resolution := (State    => Resolved,
                     Query    => Query,
                     Response => Response,
                     Server   => DNS.Resolution.SH.To_Holder (Server));

      R := Get_Response (Resolution => Resolution);

      Assert (Condition => Response = R,
              Message   => "Response doesn't match");

--  begin read only
   end Test_Get_Response;
--  end read only


--  begin read only
   procedure Test_Get_Server (Gnattest_T : in out Test);
   procedure Test_Get_Server_cdd5e4 (Gnattest_T : in out Test) renames Test_Get_Server;
--  id:2.2/cdd5e4ef533c88d2/Get_Server/1/0/
   procedure Test_Get_Server (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Resolution : Resolution_Type := (State => Pending,
                                       Query => Query);
   begin

      begin
         declare
            S : DNS.Server.Server_Type'Class := Get_Server (Resolution);
         begin
            Assert (Condition => False,
                    Message   => "Incorrectly got a server");
         end;
      exception
         when Ada.Assertions.Assertion_Error => null;
      end;

      Resolution := (State    => Resolved,
                     Query    => Query,
                     Response => Response,
                     Server   => DNS.Resolution.SH.To_Holder (Server));

      declare
         S : DNS.Server.Server_Type'Class := Get_Server (Resolution);
      begin
         Assert (Condition => Server = S,
                 Message   => "Server doesn't match");
      end;

--  begin read only
   end Test_Get_Server;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Resolution.Test_Data.Tests;
