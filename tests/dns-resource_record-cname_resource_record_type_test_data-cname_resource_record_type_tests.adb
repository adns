--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Resource_Record.CNAME_Resource_Record_Type_Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

--  begin read only
--  end read only
package body DNS.Resource_Record.CNAME_Resource_Record_Type_Test_Data.CNAME_Resource_Record_Type_Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_1_Create (Gnattest_T : in out Test_CNAME_Resource_Record_Type);
   procedure Test_Create_113a91 (Gnattest_T : in out Test_CNAME_Resource_Record_Type) renames Test_1_Create;
--  id:2.2/113a917f3ce364c6/Create/1/0/
   procedure Test_1_Create (Gnattest_T : in out Test_CNAME_Resource_Record_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use DNS.Domain;

      Length : Ada.Streams.Stream_Element_Count;
   begin

      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            declare
               Expected : DNS.Domain.Domain_Type
                 := DNS.Domain.Validate (Test_Pattern.CName);
               RR       : Resource_Record_Type'Class
                 :=          Deserialize (Message => Test_Pattern.Message,
                                          Index   => Test_Pattern.Offset,
                                          Length  => Length);
               CN       : CNAME_Resource_Record_Type
                 := CNAME_Resource_Record_Type (RR);
            begin
               Assert (Condition => Expected = CN.CName,
                       Message   => "CNAME in resource record not valid");
            end;
         else
            begin
               declare
                  RR : Resource_Record_Type'Class
                    := Deserialize (Message => Test_Pattern.Message,
                                    Index   => Test_Pattern.Offset,
                                    Length  => Length);
               begin
                  Assert (Condition => False,
                          Message   => "CNAME resource record unexpectedly valid");
               end;
            exception
               when Invalid_Resource_Record => null;
            end;
         end if;
      end loop;

--  begin read only
   end Test_1_Create;
--  end read only


--  begin read only
   procedure Test_1_Get_Data_Str (Gnattest_T : in out Test_CNAME_Resource_Record_Type);
   procedure Test_Get_Data_Str_9fba4a (Gnattest_T : in out Test_CNAME_Resource_Record_Type) renames Test_1_Get_Data_Str;
--  id:2.2/9fba4a0b895d595f/Get_Data_Str/1/0/
   procedure Test_1_Get_Data_Str (Gnattest_T : in out Test_CNAME_Resource_Record_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Strings.Unbounded;

      Length : Ada.Streams.Stream_Element_Count;
   begin

      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            declare
               RR : Resource_Record_Type'Class
                 := Deserialize (Message => Test_Pattern.Message,
                                 Index   => Test_Pattern.Offset,
                                 Length  => Length);
            begin
               Assert (Expected => To_String (Test_Pattern.CName),
                       Actual   => Get_Data_Str (RR),
                       Message  => "CNAME in resource record not valid");
            end;
         end if;
      end loop;

--  begin read only
   end Test_1_Get_Data_Str;
--  end read only


--  begin read only
   procedure Test_Get_Value (Gnattest_T : in out Test_CNAME_Resource_Record_Type);
   procedure Test_Get_Value_8d4f82 (Gnattest_T : in out Test_CNAME_Resource_Record_Type) renames Test_Get_Value;
--  id:2.2/8d4f8216ce8d1299/Get_Value/1/0/
   procedure Test_Get_Value (Gnattest_T : in out Test_CNAME_Resource_Record_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Strings.Unbounded;
      use DNS.Domain;

      Length  : Ada.Streams.Stream_Element_Count;
   begin

      for Test_Pattern of Test_Patterns loop
         if Test_Pattern.Valid then
            declare
               RR       : Resource_Record_Type'Class
                 := Deserialize (Message => Test_Pattern.Message,
                                 Index   => Test_Pattern.Offset,
                                 Length  => Length);
               CNAME_RR : CNAME_Resource_Record_Type
                 := CNAME_Resource_Record_Type (RR);
            begin
               Assert (Expected => To_String (Test_Pattern.CName),
                       Actual   => Get_Domain (Get_Value (CNAME_RR)),
                       Message  => "CNAME in resource record not valid");
            end;
         end if;
      end loop;

--  begin read only
   end Test_Get_Value;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Resource_Record.CNAME_Resource_Record_Type_Test_Data.CNAME_Resource_Record_Type_Tests;
