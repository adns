--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

package body DNS.Resource_Record.AAAA_Resource_Record_Type_Test_Data is

   --  Local_AAAA_Resource_Record_Type : aliased GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.AAAA_Resource_Record_Type;
   procedure Set_Up (Gnattest_T : in out Test_AAAA_Resource_Record_Type) is
   begin
      GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests.Set_Up
        (GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests.Test_Raw_Resource_Record_Type (Gnattest_T));
      null;
      --  Gnattest_T.Fixture := Local_AAAA_Resource_Record_Type'Access;
   end Set_Up;

   procedure Tear_Down (Gnattest_T : in out Test_AAAA_Resource_Record_Type) is
   begin
      GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests.Tear_Down
        (GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests.Test_Raw_Resource_Record_Type (Gnattest_T));
   end Tear_Down;

end DNS.Resource_Record.AAAA_Resource_Record_Type_Test_Data;
