--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

with Ada.Containers.Indefinite_Vectors;
with Ada.Streams;
with Ada.Strings.Unbounded;

with DNS.Domain;

with AUnit.Test_Fixtures;

package DNS.Message.Query.Test_Data is

--  begin read only
   type Test is new AUnit.Test_Fixtures.Test_Fixture
--  end read only
   with null record;

   type Test_Pattern_Type (Enc_Len : Ada.Streams.Stream_Element_Offset)
   is record
      Domain   : DNS.Domain.Domain_Type;
      Id       : DNS.ID_Type;
      QType    : DNS.RR_Type_Type;
      Encoding : Ada.Streams.Stream_Element_Array (1 .. Enc_Len);
   end record;

   package Test_Pattern_Vecs is new Ada.Containers.Indefinite_Vectors
     (Element_Type => Test_Pattern_Type,
      Index_Type   => Natural);

   Test_Patterns : Test_Pattern_Vecs.Vector;

   procedure Set_Up (Gnattest_T : in out Test);
   procedure Tear_Down (Gnattest_T : in out Test);

end DNS.Message.Query.Test_Data;
