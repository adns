--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Server.Manager.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

--  begin read only
--  end read only
package body DNS.Server.Manager.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Set (Gnattest_T : in out Test);
   procedure Test_Set_c55208 (Gnattest_T : in out Test) renames Test_Set;
--  id:2.2/c5520865dd2b681b/Set/1/0/
   procedure Test_Set (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);
      use type Dns.Server.List.Server_List;

      Manager : Server_Manager_Type;
      Servers : DNS.Server.List.Server_List;
      Empty   : DNS.Server.List.Server_List;
   begin

      Servers.Append (New_Item => DNS.Server.Create (Address => "8.8.8.8"));

      Set (Manager => Manager,
           Servers => Servers);

      Assert (Condition => Get (Manager) = Servers,
              Message   => "Server list does not match");

      Servers.Append (New_Item => DNS.Server.Create (Address => "1.1.1.1"));

      Assert (Condition => Get (Manager) /= Servers,
              Message   => "Server list does match");

      Set (Manager => Manager,
           Servers => Servers);

      Assert (Condition => Get (Manager) = Servers,
              Message   => "Server list does not match");

      Set (Manager => Manager,
           Servers => Empty);

      Assert (Condition => Get (Manager) = Empty,
              Message   => "Server list does not match");

--  begin read only
   end Test_Set;
--  end read only


--  begin read only
   procedure Test_Get (Gnattest_T : in out Test);
   procedure Test_Get_86f050 (Gnattest_T : in out Test) renames Test_Get;
--  id:2.2/86f0505b31abca49/Get/1/0/
   procedure Test_Get (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin

      Assert (Condition => True,
              Message   => "Tested in test for Set");

--  begin read only
   end Test_Get;
--  end read only


--  begin read only
   procedure Test_Success (Gnattest_T : in out Test);
   procedure Test_Success_f22215 (Gnattest_T : in out Test) renames Test_Success;
--  id:2.2/f2221567125990b3/Success/1/0/
   procedure Test_Success (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);
      use type Dns.Server.List.Server_List;

      Manager : Server_Manager_Type;
      Servers : DNS.Server.List.Server_List;
      Empty   : DNS.Server.List.Server_List;
   begin

      Servers.Append (New_Item => DNS.Server.Create (Address => "8.8.8.8"));
      Servers.Append (New_Item => DNS.Server.Create (Address => "1.1.1.1"));

      Set (Manager => Manager,
           Servers => Servers);

      Success (Manager => Manager,
               Server  => DNS.Server.Create (Address => "8.8.8.8"));

      Assert (Condition => Get (Manager) = Servers,
              Message   => "Server list does not match");

      Success (Manager => Manager,
               Server  => DNS.Server.Create (Address => "1.1.1.1"));

      Servers.Delete_First;
      Servers.Append (New_Item => DNS.Server.Create (Address => "8.8.8.8"));

      Assert (Condition => Get (Manager) = Servers,
              Message   => "Server list does not match");

      Success (Manager => Manager,
               Server  => DNS.Server.Create (Address => "9.9.9.9"));

--  begin read only
   end Test_Success;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Server.Manager.Test_Data.Tests;
