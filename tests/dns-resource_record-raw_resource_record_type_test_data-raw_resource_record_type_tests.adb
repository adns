--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Strings.Unbounded;
with Anet;

--  begin read only
--  end read only
package body DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_1_Create (Gnattest_T : in out Test_Raw_Resource_Record_Type);
   procedure Test_Create_113a91 (Gnattest_T : in out Test_Raw_Resource_Record_Type) renames Test_1_Create;
--  id:2.2/113a917f3ce364c6/Create/1/0/
   procedure Test_1_Create (Gnattest_T : in out Test_Raw_Resource_Record_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);
   begin

      Assert (Condition => True,
              Message   => "Tested implicitly via Deserialize()");

--  begin read only
   end Test_1_Create;
--  end read only


--  begin read only
   procedure Test_1_Get_Data_Str (Gnattest_T : in out Test_Raw_Resource_Record_Type);
   procedure Test_Get_Data_Str_9fba4a (Gnattest_T : in out Test_Raw_Resource_Record_Type) renames Test_1_Get_Data_Str;
--  id:2.2/9fba4a0b895d595f/Get_Data_Str/1/0/
   procedure Test_1_Get_Data_Str (Gnattest_T : in out Test_Raw_Resource_Record_Type) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Streams;
      use Ada.Strings.Unbounded;

      Message : constant Ada.Streams.Stream_Element_Array
        := (16#00#, 16#00#, 16#01#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#04#, 16#0a#, 16#0b#, 16#0c#, 16#0d#);
      Length  : Ada.Streams.Stream_Element_Count;
      RR      : Resource_Record_Type'Class
        := Deserialize (Message => Message,
                        Index   => Message'First,
                        Length  => Length);
      Raw     : Raw_Resource_Record_Type := Raw_Resource_Record_Type (RR);
   begin

      Assert (Expected => "0A0B0C0D",
              Actual   => Get_Data_Str (Raw),
              Message  => "Resource record data doesn't match");

--  begin read only
   end Test_1_Get_Data_Str;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests;
