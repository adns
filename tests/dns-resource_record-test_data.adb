--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

with DNS.Test_Data;

package body DNS.Resource_Record.Test_Data is

   -------------------------------------------------------------------------

   procedure Add
     (Domain  : String;
      Valid   : Boolean;
      RType   : RR_Type_Type;
      Offset  : Ada.Streams.Stream_Element_Offset;
      Length  : Ada.Streams.Stream_Element_Count;
      Data    : Ada.Streams.Stream_Element_Array;
      Message : Ada.Streams.Stream_Element_Array)
   is
      Dom     : constant Ada.Strings.Unbounded.Unbounded_String
        := Ada.Strings.Unbounded.To_Unbounded_String (Domain);
      Pattern : constant Test_Pattern_Type
        := (Data'Length, Message'Length, Dom, Valid, RType, Offset, Length,
            Data, Message);
   begin
      Test_Pattern_Vecs.Append (Test_Patterns, Pattern);
   end Add;

   -------------------------------------------------------------------------

   procedure Set_Up (Gnattest_T : in out Test) is
      pragma Unreferenced (Gnattest_T);
   begin
      Add (".", True, A, 1, 15, (16#0a#, 16#0a#, 16#0a#, 16#0a#),
           (16#00#, 16#00#, 16#01#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#04#, 16#0a#, 16#0a#, 16#0a#, 16#0a#));
      Add (".", True, AAAA, 1, 27,
           (16#20#, 16#01#, 16#0d#, 16#b8#, 16#00#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#0a#, 16#01#),
           (16#00#, 16#00#, 16#1c#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#10#, 16#20#, 16#01#, 16#0d#, 16#b8#, 16#00#,
            16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#0a#, 16#01#));
      --  Technically not correct, but NS RRs are currently not validated
      Add (".", True, NS, 1, 11, (1 .. 0 => 0),
           (16#00#, 16#00#, 16#02#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#00#));
      --  Too short
      Add ("", False, A, 1, 0, (1 => 16#00#), (1 => 16#00#));
      --  Invalid domain
      Add ("", False, A, 1, 0, (1 => 16#00#),
           (16#01#, 16#00#, 16#01#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#04#, 16#0a#, 16#0a#, 16#0a#, 16#0a#));
      --  Invalid RDLength
      Add ("", False, A, 1, 0, (1 => 16#00#),
           (16#00#, 16#00#, 16#01#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#04#));
      Add ("", False, A, 1, 0, (1 => 16#00#),
           (16#00#, 16#00#, 16#01#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#05#, 16#0a#, 16#0a#, 16#0a#, 16#0a#));
      Add ("", False, A, 1, 0, (1 => 16#00#),
           (16#00#, 16#00#, 16#01#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#02#, 16#0a#, 16#0a#));
      Add ("", False, AAAA, 1, 0, (1 => 16#00#),
           (16#00#, 16#00#, 16#1c#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#04#, 16#20#, 16#01#, 16#0d#, 16#b8#));

      --  Real world example
      Add ("www.hsr.ch.", True, CNAME, 29, 26,
           (16#0b#, 16#6c#, 16#62#, 16#2d#, 16#65#, 16#78#, 16#74#, 16#2d#,
            16#77#, 16#65#, 16#62#, 16#31#, 16#c0#, 16#10#),
           DNS.Test_Data.Response);
      Add ("lb-ext-web1.hsr.ch.", True, A, 55, 16,
           (16#98#, 16#60#, 16#24#, 16#64#), DNS.Test_Data.Response);
   end Set_Up;

   procedure Tear_Down (Gnattest_T : in out Test) is
      pragma Unreferenced (Gnattest_T);
   begin
      null;
   end Tear_Down;

end DNS.Resource_Record.Test_Data;
