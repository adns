--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Message.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Containers;
with DNS.Test_Data;

--  begin read only
--  end read only
package body DNS.Message.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Deserialize (Gnattest_T : in out Test);
   procedure Test_Deserialize_3b5414 (Gnattest_T : in out Test) renames Test_Deserialize;
--  id:2.2/3b54141d8024d3b6/Deserialize/1/0/
   procedure Test_Deserialize (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Streams;
      use type Ada.Containers.Count_Type;
      use Anet;
      use DNS.Domain;

      R       : Response_Type;
      Message : Ada.Streams.Stream_Element_Array := DNS.Test_Data.Response;
      Invalid : Stream_Element_Array (1 .. DNS.Test_Data.Response'Length);

      procedure Invalid_Response (Msg : Stream_Element_Array) is
      begin
         R := Deserialize (Msg);
         Assert (Condition => False,
                 Message   => "DNS message unexpectedly valid");
      exception
         when Invalid_Message => null;
      end Invalid_Response;

   begin

      R := Deserialize (DNS.Test_Data.Response);
      Assert (Condition => R.Id = 16#8689#,
              Message   => "Parsed ID doesn't match");
      Assert (Condition => R.RCode = Rc_No_Error,
              Message   => "Parsed RCode doesn't match");
      Assert (Condition => R.QType = A,
              Message   => "Parsed QType doesn't match");
      Assert (Condition => R.Query = Validate ("www.hsr.ch."),
              Message   => "Parsed query doesn't match");
      Assert (Condition => R.Canonical = Validate ("lb-ext-web1.hsr.ch."),
              Message   => "Canonical name doesn't match");
      Assert (Condition => R.Answer.Length = 2,
              Message   => "Number of answers doesn't match");
      Assert (Condition => R.Filtered.Length = 1,
              Message   => "Number of filtered answers doesn't match");

      R := Deserialize (DNS.Test_Data.Response_NX);
      Assert (Condition => R.Id = 16#e4d0#,
              Message   => "Parsed ID doesn't match");
      Assert (Condition => R.RCode = Rc_Name_Error,
              Message   => "Parsed RCode doesn't match");
      Assert (Condition => R.QType = A,
              Message   => "Parsed QType doesn't match");
      Assert (Condition => R.Query = Validate ("hsrhsr.ch."),
              Message   => "Parsed query doesn't match");
      Assert (Condition => R.Canonical = Validate ("hsrhsr.ch."),
              Message   => "Canonical name doesn't match");
      Assert (Condition => R.Answer.Length = 0,
              Message   => "Number of answers doesn't match");
      Assert (Condition => R.Filtered.Length = 0,
              Message   => "Number of filtered answers doesn't match");

      R := Deserialize (DNS.Test_Data.Response_NA);
      Assert (Condition => R.Id = 16#c30b#,
              Message   => "Parsed ID doesn't match");
      Assert (Condition => R.RCode = Rc_No_Error,
              Message   => "Parsed RCode doesn't match");
      Assert (Condition => R.QType = A,
              Message   => "Parsed QType doesn't match");
      Assert (Condition => R.Query = Validate ("ipv6.google.com."),
              Message   => "Parsed query doesn't match");
      Assert (Condition => R.Canonical = Validate ("ipv6.l.google.com."),
              Message   => "Canonical name doesn't match");
      Assert (Condition => R.Answer.Length = 1,
              Message   => "Number of answers doesn't match");
      Assert (Condition => R.Filtered.Length = 0,
              Message   => "Number of filtered answers doesn't match");

      R := Deserialize (DNS.Test_Data.Response_Chain);
      Assert (Condition => R.Id = 16#8551#,
              Message   => "Parsed ID doesn't match");
      Assert (Condition => R.RCode = Rc_No_Error,
              Message   => "Parsed RCode doesn't match");
      Assert (Condition => R.QType = A,
              Message   => "Parsed QType doesn't match");
      Assert (Condition => R.Query = Validate ("newsletter.example.com."),
              Message   => "Parsed query doesn't match");
      Assert (Condition => R.Canonical = Validate ("be4de3fb6a.cdn-example.com."),
              Message   => "Canonical name doesn't match");
      Assert (Condition => R.Answer.Length = 4,
              Message   => "Number of answers doesn't match");
      Assert (Condition => R.Filtered.Length = 2,
              Message   => "Number of filtered answers doesn't match");

      R := Deserialize (DNS.Test_Data.Response_Interleaved);
      Assert (Condition => R.Id = 16#a23b#,
              Message   => "Parsed ID doesn't match");
      Assert (Condition => R.RCode = Rc_No_Error,
              Message   => "Parsed RCode doesn't match");
      Assert (Condition => R.QType = A,
              Message   => "Parsed QType doesn't match");
      Assert (Condition => R.Query = Validate ("a-example.com."),
              Message   => "Parsed query doesn't match");
      Assert (Condition => R.Canonical = Validate ("c-example.com."),
              Message   => "Canonical name doesn't match");
      Assert (Condition => R.Answer.Length = 4,
              Message   => "Number of answers doesn't match");
      Assert (Condition => R.Filtered.Length = 1,
              Message   => "Number of filtered answers doesn't match");

      --  Empty message
      Invalid_Response ((0 .. 1 => 0));
      --  Only the header
      Invalid_Response (Message (Message'First .. Message'First + 11));
      --  Incomplete query domain name
      Invalid_Response (Message (Message'First .. Message'First + 15));
      --  Missing question suffix
      Invalid_Response (Message (Message'First .. Message'First + 23));
      --  Answer missing
      Invalid_Response (Message (Message'First .. Message'First + 27));
      --  Answer incomplete
      Invalid_Response (Message (Message'First .. Message'First + 30));

      --  Not a response
      Invalid := Message;
      Invalid (3) := 16#01#;
      Invalid_Response (Invalid);
      --  Not a query
      Invalid (3) := 16#89#;
      Invalid_Response (Invalid);
      --  Invalid RCode
      Invalid := Message;
      Invalid (4) := 16#88#;
      Invalid_Response (Invalid);
      --  Invalid question count
      Invalid := Message;
      Invalid (6) := 16#00#;
      Invalid_Response (Invalid);
      Invalid (6) := 16#02#;
      Invalid_Response (Invalid);
      --  Invalid QType
      Invalid := Message;
      Invalid (26) := 16#42#;
      Invalid_Response (Invalid);

      declare
         Too_Long : Stream_Element_Array (1 .. Message'Length + 1);
      begin
         Too_Long (1 .. Message'Length) := Message (Message'Range);
         Invalid_Response (Too_Long);
      end;

      --  CNAME loop
      Invalid_Response (DNS.Test_Data.Response_Loop);

--  begin read only
   end Test_Deserialize;
--  end read only


--  begin read only
   procedure Test_Get_RCode (Gnattest_T : in out Test);
   procedure Test_Get_RCode_bd1481 (Gnattest_T : in out Test) renames Test_Get_RCode;
--  id:2.2/bd1481002d2ff329/Get_RCode/1/0/
   procedure Test_Get_RCode (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      R : Response_Type;
   begin

      R := Deserialize (DNS.Test_Data.Response);
      Assert (Condition => Get_RCode (R) = Rc_No_Error,
              Message   => "Returned RCode isn't Rc_No_Error");

      R := Deserialize (DNS.Test_Data.Response_NX);
      Assert (Condition => Get_RCode (R) = Rc_Name_Error,
              Message   => "Returned RCode isn't Rc_Name_Error");

--  begin read only
   end Test_Get_RCode;
--  end read only


--  begin read only
   procedure Test_Get_QName (Gnattest_T : in out Test);
   procedure Test_Get_QName_3f29c9 (Gnattest_T : in out Test) renames Test_Get_QName;
--  id:2.2/3f29c9c8f3a58385/Get_QName/1/0/
   procedure Test_Get_QName (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type DNS.Domain.Domain_Type;

      R : Response_Type;
   begin

      R := Deserialize (DNS.Test_Data.Response);
      Assert (Condition => Get_QName (R) = DNS.Domain.Validate ("www.hsr.ch"),
              Message   => "Unexpected name for regular response");

      R := Deserialize (DNS.Test_Data.Response_NX);
      Assert (Condition => Get_QName (R) = DNS.Domain.Validate ("hsrhsr.ch"),
              Message   => "Unexpected name for failed response");

--  begin read only
   end Test_Get_QName;
--  end read only


--  begin read only
   procedure Test_Get_QType (Gnattest_T : in out Test);
   procedure Test_Get_QType_dbdd3b (Gnattest_T : in out Test) renames Test_Get_QType;
--  id:2.2/dbdd3b235722cdf3/Get_QType/1/0/
   procedure Test_Get_QType (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      R : Response_Type;
   begin

      R := Deserialize (DNS.Test_Data.Response);
      Assert (Condition => Get_QType (R) = A,
              Message   => "Returned QType isn't A");

      R := Deserialize (DNS.Test_Data.Response_AAAA);
      Assert (Condition => Get_QType (R) = AAAA,
              Message   => "Returned QType isn't AAAA");

--  begin read only
   end Test_Get_QType;
--  end read only


--  begin read only
   procedure Test_Get_Answer (Gnattest_T : in out Test);
   procedure Test_Get_Answer_fcbd94 (Gnattest_T : in out Test) renames Test_Get_Answer;
--  id:2.2/fcbd9472141570c1/Get_Answer/1/0/
   procedure Test_Get_Answer (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Ada.Containers.Count_Type;

      R : Response_Type;
   begin

      R := Deserialize (DNS.Test_Data.Response);
      Assert (Condition => Get_Answer (R).Length = 1,
              Message   => "Number of answers doesn't match");
      Assert (Condition => Get_Answer (R, Filtered => False).Length = 2,
              Message   => "Number of raw answers doesn't match");

      R := Deserialize (DNS.Test_Data.Response_NX);
      Assert (Condition => Get_Answer (R).Length = 0,
              Message   => "Number of answers doesn't match");
      Assert (Condition => Get_Answer (R, Filtered => False).Length = 0,
              Message   => "Number of raw answers doesn't match");

      R := Deserialize (DNS.Test_Data.Response_Interleaved);
      Assert (Condition => Get_Answer (R).Length = 1,
              Message   => "Number of answers doesn't match");
      Assert (Condition => Get_Answer (R, Filtered => False).Length = 4,
              Message   => "Number of raw answers doesn't match");

--  begin read only
   end Test_Get_Answer;
--  end read only


--  begin read only
   procedure Test_Has_Answer (Gnattest_T : in out Test);
   procedure Test_Has_Answer_6341f6 (Gnattest_T : in out Test) renames Test_Has_Answer;
--  id:2.2/6341f64f741aebef/Has_Answer/1/0/
   procedure Test_Has_Answer (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      R : Response_Type;
   begin

      R := Deserialize (DNS.Test_Data.Response);
      Assert (Condition => Has_Answer (R),
              Message   => "Unexpectedly found no answer");

      R := Deserialize (DNS.Test_Data.Response_NX);
      Assert (Condition => not Has_Answer (R),
              Message   => "Unexpectedly found an answer");

      R := Deserialize (DNS.Test_Data.Response_NA);
      Assert (Condition => not Has_Answer (R),
              Message   => "Unexpectedly found an answer");

      R := Deserialize (DNS.Test_Data.Response_Chain);
      Assert (Condition => Has_Answer (R),
              Message   => "Unexpectedly found no answer");

      R := Deserialize (DNS.Test_Data.Response_Interleaved);
      Assert (Condition => Has_Answer (R),
              Message   => "Unexpectedly found no answer");

--  begin read only
   end Test_Has_Answer;
--  end read only


--  begin read only
   procedure Test_Get_Canonical (Gnattest_T : in out Test);
   procedure Test_Get_Canonical_aca51f (Gnattest_T : in out Test) renames Test_Get_Canonical;
--  id:2.2/aca51f9eb065f4b5/Get_Canonical/1/0/
   procedure Test_Get_Canonical (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use DNS.Domain;

      R : Response_Type;
   begin

      R := Deserialize (DNS.Test_Data.Response);
      Assert (Condition => Get_Canonical (R) = Validate ("lb-ext-web1.hsr.ch."),
              Message   => "Canonical name doesn't match");

      R := Deserialize (DNS.Test_Data.Response_NX);
      Assert (Condition => Get_Canonical (R) = Validate ("hsrhsr.ch."),
              Message   => "Canonical name doesn't match");

      R := Deserialize (DNS.Test_Data.Response_NA);
      Assert (Condition => Get_Canonical (R) = Validate ("ipv6.l.google.com."),
              Message   => "Canonical name doesn't match");

      R := Deserialize (DNS.Test_Data.Response_Chain);
      Assert (Condition => Get_Canonical (R) = Validate ("be4de3fb6a.cdn-example.com."),
              Message   => "Canonical name doesn't match");

      R := Deserialize (DNS.Test_Data.Response_Interleaved);
      Assert (Condition => Get_Canonical (R) = Validate ("c-example.com."),
              Message   => "Canonical name doesn't match");

--  begin read only
   end Test_Get_Canonical;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Message.Test_Data.Tests;
