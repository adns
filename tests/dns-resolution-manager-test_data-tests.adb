--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Resolution.Manager.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Exceptions;
with Ada.Streams;

with GNAT.Source_Info;

with Adns_Util.Logger;
with Adns_Util.Timer;

with DNS.Socket.Mock;
with DNS.Test_Data;

--  begin read only
--  end read only
package body DNS.Resolution.Manager.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

   ----------------------------------------------------------------------

   procedure Prepare_Response
     (Sent   : Natural;
      After  : Natural;
      Msg    : Ada.Streams.Stream_Element_Array;
      Server : DNS.Server.Server_Type'Class)
   is
   begin
      --  Wait until the request has been sent.
      while DNS.Socket.Mock.Socket_Data.Send_Count = Sent - 1 loop
         delay 0.01;
      end loop;
      DNS.Socket.Mock.Socket_Data.Receive_After := After;
      DNS.Socket.Mock.Socket_Data.Receive_Msg (1 .. Msg'Length) := Msg;
      DNS.Socket.Mock.Socket_Data.Receive_Len := Msg'Length;
      DNS.Socket.Mock.Socket_Data.Receive_Server
        := DNS.Socket.Mock.SH.To_Holder (Server);
      --  Copy random Id to header of received message.
      DNS.Socket.Mock.Socket_Data.Receive_Msg (1 .. 2)
        := DNS.Socket.Mock.Socket_Data.Send_Msg (1 .. 2);
   end Prepare_Response;

   ----------------------------------------------------------------------

   procedure Assert_Socket_Counters
     (Init, Close, Send, Receive : Natural := 0;
      Source                     : String  := GNAT.Source_Info.File;
      Line                       : Natural := GNAT.Source_Info.Line)
   is
      use DNS.Socket.Mock;
   begin
      Assert (Condition => Socket_Data.Init_Count = Init,
              Message   => "Init() called unexpected number of times:" &
                DNS.Socket.Mock.Socket_Data.Init_Count'Img,
              Source    => Source,
              Line      => Line);
      Assert (Condition => Socket_Data.Close_Count = Close,
              Message   => "Close() called unexpected number of times:" &
                DNS.Socket.Mock.Socket_Data.Close_Count'Img,
              Source    => Source,
              Line      => Line);
      Assert (Condition => Socket_Data.Send_Count = Send,
              Message   => "Send() called unexpected number of times:" &
                DNS.Socket.Mock.Socket_Data.Send_Count'Img,
              Source    => Source,
              Line      => Line);
      Assert (Condition => Socket_Data.Receive_Count = Receive,
              Message   => "Receive() called unexpected number of times:" &
                DNS.Socket.Mock.Socket_Data.Receive_Count'Img,
              Source    => Source,
              Line      => Line);
   end Assert_Socket_Counters;

--  begin read only
--  end read only

--  begin read only
   procedure Test_1_Resolve (Gnattest_T : in out Test);
   procedure Test_Resolve_ee11d7 (Gnattest_T : in out Test) renames Test_1_Resolve;
--  id:2.2/ee11d7e204763fde/Resolve/1/0/
   procedure Test_1_Resolve (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin

      Assert (Condition => True,
              Message   => "Tested in other Resolve test");

--  begin read only
   end Test_1_Resolve;
--  end read only


--  begin read only
   procedure Test_2_Resolve (Gnattest_T : in out Test);
   procedure Test_Resolve_982741 (Gnattest_T : in out Test) renames Test_2_Resolve;
--  id:2.2/982741a9a902dd12/Resolve/0/0/
   procedure Test_2_Resolve (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Ada.Containers.Count_Type;

      Domain      : DNS.Domain.Domain_Type
        := DNS.Domain.Validate ("www.hsr.ch");
      Servers     : DNS.Server.List.Server_List;
      Resolutions : DNS.Resolution.List.Resolution_List;
      Resolution  : DNS.Resolution.Resolution_Type;
   begin
      Servers.Append (New_Item => DNS.Server.Create (Address => "8.8.8.8"));
      Set_Servers (Servers => Servers);

      --  Empty list

      begin
         Resolve (Resolutions => Resolutions,
                  Handler     => First_Handler);
         Assert (Condition => False,
                 Message   => "Empty resolution list unexpectedly accepted");
      exception
         when E : Constraint_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E) =
                      "No resolutions supplied",
                    Message   => "Unexpected message for empty list");
      end;

      --  Timeout

      Resolutions.Append (New_Item => DNS.Resolution.Prepare (Domain => Domain,
                                                              QType  => A));
      Resolve (Resolutions => Resolutions,
               Handler     => First_Handler);

      delay 0.6;

      Assert (Condition => not Last_Resolutions (1).Is_Empty,
              Message   => "No result received");
      Resolution := Last_Resolutions (1).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Timeout,
              Message   => "No timeout occurred");
      Assert_Socket_Counters (Init    => 1,
                              Close   => 1,
                              Send    => 2,
                              Receive => 6);
      Clear;

      --  Socket error during Init

      DNS.Socket.Mock.Socket_Data.Init_Error := 1;

      Resolve (Resolutions => Resolutions,
               Handler     => First_Handler);

      delay 0.1;

      Assert (Condition => not Last_Resolutions (1).Is_Empty,
              Message   => "No result received");
      Resolution := Last_Resolutions (1).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Failure,
              Message   => "No failure occurred");
      Assert_Socket_Counters (Init    => 1,
                              Close   => 1);
      Clear;

      --  Successful resolution (using the simple interface)

      Resolve (Resolution => Resolutions.First_Element,
               Handler    => First_Handler);

      Prepare_Response (Sent   => 1,
                        After  => 2,
                        Msg    => DNS.Test_Data.Response,
                        Server => Servers (1));

      delay 0.3;

      Assert (Condition => not Last_Resolutions (1).Is_Empty,
              Message   => "No result received");
      Resolution := Last_Resolutions (1).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Resolved,
              Message   => "Resolution isn't resolved");
      Assert_Socket_Counters (Init    => 1,
                              Close   => 1,
                              Send    => 1,
                              Receive => 2);
      Clear;

      --  Test with multiple successful queries (only one socket, so the second
      --  query has to wait)

      Resolutions.Append (New_Item => DNS.Resolution.Prepare (Domain => Domain,
                                                              QType  => AAAA));
      Resolve (Resolutions => Resolutions,
               Handler     => First_Handler);

      Prepare_Response (Sent   => 1,
                        After  => 2,
                        Msg    => DNS.Test_Data.Response,
                        Server => Servers (1));

      Prepare_Response (Sent   => 2,
                        After  => 4,
                        Msg    => DNS.Test_Data.Response_AAAA,
                        Server => Servers (1));
      delay 0.3;

      Assert (Condition => Last_Resolutions (1).Length = 2,
              Message   => "No results received");
      Resolution := Last_Resolutions (1).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Resolved,
              Message   => "Resolution for A isn't resolved");
      Resolution := Last_Resolutions (1).Last_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Resolved,
              Message   => "Resolution for AAAA isn't resolved");
      Assert_Socket_Counters (Init    => 2,
                              Close   => 2,
                              Send    => 2,
                              Receive => 4);
      Clear;

      --  Test concurrent queries for the same domain

      Resolve (Resolution => Resolutions.First_Element,
               Handler    => First_Handler);
      Resolve (Resolutions => Resolutions,
               Handler     => Second_Handler);

      Prepare_Response (Sent   => 1,
                        After  => 2,
                        Msg    => DNS.Test_Data.Response,
                        Server => Servers (1));

      Prepare_Response (Sent   => 2,
                        After  => 4,
                        Msg    => DNS.Test_Data.Response_AAAA,
                        Server => Servers (1));
      delay 0.3;

      Assert (Condition => not Last_Resolutions (1).Is_Empty,
              Message   => "No results received by first handler");
      Resolution := Last_Resolutions (1).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Resolved,
              Message   => "Resolution for A isn't resolved");
      Assert (Condition => Last_Resolutions (2).Length = 2,
              Message   => "No results received by second handler");
      Resolution := Last_Resolutions (2).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Resolved,
              Message   => "Resolution for A isn't resolved");
      Resolution := Last_Resolutions (2).Last_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Resolved,
              Message   => "Resolution for AAAA isn't resolved");
      Assert_Socket_Counters (Init    => 2,
                              Close   => 2,
                              Send    => 2,
                              Receive => 4);

--  begin read only
   end Test_2_Resolve;
--  end read only


--  begin read only
   procedure Test_Set_Servers (Gnattest_T : in out Test);
   procedure Test_Set_Servers_75f97a (Gnattest_T : in out Test) renames Test_Set_Servers;
--  id:2.2/75f97adcdac8ca35/Set_Servers/1/0/
   procedure Test_Set_Servers (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Domain       : DNS.Domain.Domain_Type
        := DNS.Domain.Validate ("www.hsr.ch");
      Servers      : DNS.Server.List.Server_List;
      First_Server : DNS.Server.Server_Type'Class
        := DNS.Server.Create (Address => "8.8.8.8");
      Resolutions  : DNS.Resolution.List.Resolution_List;
      Resolution   : DNS.Resolution.Resolution_Type;
   begin

      --  Implicitly tested above, some special cases here.

      --  No servers

      Resolutions.Append (New_Item => DNS.Resolution.Prepare (Domain => Domain,
                                                              QType  => A));
      Resolve (Resolutions => Resolutions,
               Handler     => First_Handler);

      delay 0.2;

      Assert (Condition => not Last_Resolutions (1).Is_Empty,
              Message   => "No result received");
      Resolution := Last_Resolutions (1).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Failure,
              Message   => "No failure occurred");
      Assert_Socket_Counters (Init    => 1,
                              Close   => 1,
                              Send    => 0,
                              Receive => 1);
      Clear;

      --  Change servers after starting resolution

      Servers.Append (New_Item => First_Server);
      Set_Servers (Servers => Servers);

      Resolve (Resolutions => Resolutions,
               Handler     => First_Handler);

      delay 0.1;

      Servers.Delete_First;
      Servers.Append (New_Item => DNS.Server.Create (Address => "9.9.9.9"));
      Set_Servers (Servers => Servers);

      delay 0.2;

      Assert (Condition => Socket.Mock.Socket_Data.Send_Server.Element =
                First_Server,
              Message   => "Message sent to an unexpected server");

      delay 0.3;

      Assert (Condition => not Last_Resolutions (1).Is_Empty,
              Message   => "No result received");
      Resolution := Last_Resolutions (1).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Timeout,
              Message   => "No timeout occurred");

      Resolve (Resolutions => Resolutions,
               Handler     => Second_Handler);

      delay 0.1;

      Prepare_Response (Sent   => 3,
                        After  => 8,
                        Msg    => DNS.Test_Data.Response,
                        Server => Servers (1));

      Assert (Condition => Socket.Mock.Socket_Data.Send_Server.Element =
                Servers (1),
              Message   => "Message sent to an unexpected server");

      delay 0.5;

      Assert (Condition => not Last_Resolutions (2).Is_Empty,
              Message   => "No result received");
      Resolution := Last_Resolutions (2).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Resolved,
              Message   => "No timeout occurred");
      Assert_Socket_Counters (Init    => 2,
                              Close   => 2,
                              Send    => 3,
                              Receive => 8);
      Clear;

      --  A successful server should be promoted

      Servers.Append (New_Item => First_Server);
      Set_Servers (Servers => Servers);

      Resolve (Resolutions => Resolutions,
               Handler     => First_Handler);

      delay 0.1;

      Assert (Condition => Socket.Mock.Socket_Data.Send_Server.Element =
                Servers (1),
              Message   => "Message sent to an unexpected server");

      Prepare_Response (Sent   => 2,
                        After  => 4,
                        Msg    => DNS.Test_Data.Response,
                        Server => Servers (2));

      Assert (Condition => Socket.Mock.Socket_Data.Send_Server.Element =
                Servers (2),
              Message   => "Message sent to an unexpected server");

      delay 0.5;

      Assert (Condition => not Last_Resolutions (1).Is_Empty,
              Message   => "No result received");
      Resolution := Last_Resolutions (1).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Resolved,
              Message   => "No timeout occurred");

      Resolve (Resolutions => Resolutions,
               Handler     => Second_Handler);

      delay 0.1;

      Assert (Condition => Socket.Mock.Socket_Data.Send_Server.Element =
                Servers (2),
              Message   => "Message sent to an unexpected server");

      Prepare_Response (Sent   => 3,
                        After  => 6,
                        Msg    => DNS.Test_Data.Response,
                        Server => Servers (2));

      delay 0.5;

      Assert (Condition => not Last_Resolutions (2).Is_Empty,
              Message   => "No result received");
      Resolution := Last_Resolutions (2).First_Element;
      Assert (Condition => Get_State (Resolution => Resolution) = Resolved,
              Message   => "No timeout occurred");
      Assert_Socket_Counters (Init    => 2,
                              Close   => 2,
                              Send    => 3,
                              Receive => 6);
      Clear;

--  begin read only
   end Test_Set_Servers;
--  end read only


--  begin read only
   procedure Test_Start (Gnattest_T : in out Test);
   procedure Test_Start_509954 (Gnattest_T : in out Test) renames Test_Start;
--  id:2.2/509954b8e23602c5/Start/1/0/
   procedure Test_Start (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin

      Assert (Condition => True,
              Message   => "Tested implicitly with other tests");

--  begin read only
   end Test_Start;
--  end read only


--  begin read only
   procedure Test_Stop (Gnattest_T : in out Test);
   procedure Test_Stop_13100d (Gnattest_T : in out Test) renames Test_Stop;
--  id:2.2/13100dbeb2b3f6bc/Stop/1/0/
   procedure Test_Stop (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin

      Assert (Condition => True,
              Message   => "Tested implicitly with other tests");

--  begin read only
   end Test_Stop;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Resolution.Manager.Test_Data.Tests;
