--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

package body DNS.Server.IPv4_Server_Type_Test_Data is

   Local_IPv4_Server_Type : aliased GNATtest_Generated.GNATtest_Standard.DNS.Server.IPv4_Server_Type;
   procedure Set_Up (Gnattest_T : in out Test_IPv4_Server_Type) is
   begin
      GNATtest_Generated.GNATtest_Standard.DNS.Server.Server_Type_Test_Data.Server_Type_Tests.Set_Up
        (GNATtest_Generated.GNATtest_Standard.DNS.Server.Server_Type_Test_Data.Server_Type_Tests.Test_Server_Type (Gnattest_T));
      Gnattest_T.Fixture := Local_IPv4_Server_Type'Access;
   end Set_Up;

   procedure Tear_Down (Gnattest_T : in out Test_IPv4_Server_Type) is
   begin
      GNATtest_Generated.GNATtest_Standard.DNS.Server.Server_Type_Test_Data.Server_Type_Tests.Tear_Down
        (GNATtest_Generated.GNATtest_Standard.DNS.Server.Server_Type_Test_Data.Server_Type_Tests.Test_Server_Type (Gnattest_T));
   end Tear_Down;

end DNS.Server.IPv4_Server_Type_Test_Data;
