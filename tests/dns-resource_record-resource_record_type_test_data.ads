--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.


with AUnit.Test_Fixtures;

with GNATtest_Generated;

package DNS.Resource_Record.Resource_Record_Type_Test_Data is

   type Resource_Record_Type_Access is access all GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Resource_Record_Type'Class;

--  begin read only
   type Test_Resource_Record_Type is abstract new AUnit.Test_Fixtures.Test_Fixture
--  end read only
   with record
      Fixture : Resource_Record_Type_Access;
   end record;

end DNS.Resource_Record.Resource_Record_Type_Test_Data;
