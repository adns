--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

package body DNS.Message.Query.Test_Data is

   -------------------------------------------------------------------------

   procedure Add
     (Domain   : String;
      Id       : DNS.ID_Type;
      QType    : DNS.RR_Type_Type;
      Encoding : Ada.Streams.Stream_Element_Array)
   is
      Dom     : DNS.Domain.Domain_Type := DNS.Domain.Validate
        (Ada.Strings.Unbounded.To_Unbounded_String (Domain));
      Pattern : Test_Pattern_Type := (Encoding'Length, Dom, Id, QType,
                                      Encoding);
   begin
      Test_Pattern_Vecs.Append (Test_Patterns, Pattern);
   end Add;

   -------------------------------------------------------------------------

   procedure Set_Up (Gnattest_T : in out Test) is
      pragma Unreferenced (Gnattest_T);
   begin
      Add ("www.hsr.ch", 16#8689#, A,
           (16#86#, 16#89#, 16#01#, 16#00#, 16#00#, 16#01#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#00#, 16#00#, 16#03#, 16#77#, 16#77#, 16#77#,
            16#03#, 16#68#, 16#73#, 16#72#, 16#02#, 16#63#, 16#68#, 16#00#,
            16#00#, 16#01#, 16#00#, 16#01#));
      Add ("codelabs.ch", 16#9809#, AAAA,
           (16#98#, 16#09#, 16#01#, 16#00#, 16#00#, 16#01#, 16#00#, 16#00#,
            16#00#, 16#00#, 16#00#, 16#00#, 16#08#, 16#63#, 16#6f#, 16#64#,
            16#65#, 16#6c#, 16#61#, 16#62#, 16#73#, 16#02#, 16#63#, 16#68#,
            16#00#, 16#00#, 16#1c#, 16#00#, 16#01#));
   end Set_Up;

   procedure Tear_Down (Gnattest_T : in out Test) is
      pragma Unreferenced (Gnattest_T);
   begin
      null;
   end Tear_Down;

end DNS.Message.Query.Test_Data;
