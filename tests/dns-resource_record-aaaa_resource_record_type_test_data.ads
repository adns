--  This package is intended to set up and tear down  the test environment.
--  Once created by GNATtest, this package will never be overwritten
--  automatically. Contents of this package can be modified in any way
--  except for sections surrounded by a 'read only' marker.

with DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests;

with GNATtest_Generated;

package DNS.Resource_Record.AAAA_Resource_Record_Type_Test_Data is

--  begin read only
   type Test_AAAA_Resource_Record_Type is new
     GNATtest_Generated.GNATtest_Standard.DNS.Resource_Record.Raw_Resource_Record_Type_Test_Data.Raw_Resource_Record_Type_Tests.Test_Raw_Resource_Record_Type
--  end read only
   with null record;

   procedure Set_Up (Gnattest_T : in out Test_AAAA_Resource_Record_Type);
   procedure Tear_Down (Gnattest_T : in out Test_AAAA_Resource_Record_Type);

end DNS.Resource_Record.AAAA_Resource_Record_Type_Test_Data;
