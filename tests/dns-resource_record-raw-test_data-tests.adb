--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into DNS.Resource_Record.Raw.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only

with Ada.Exceptions;

--  begin read only
--  end read only
package body DNS.Resource_Record.Raw.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Validate (Gnattest_T : in out Test);
   procedure Test_Validate_5545fa (Gnattest_T : in out Test) renames Test_Validate;
--  id:2.2/5545fa7910043e8a/Validate/1/0/
   procedure Test_Validate (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      RRBuf : Raw_RR_Header_Buffer_Type;
      RR    : Raw_RR_Header_Type
        with Address => RRBuf'Address;
   begin

      RRBuf := (16#00#, 16#01#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#, 16#00#,
                16#00#, 16#00#);
      Validate (RR);
      Assert (Condition => RR.RType = A, Message => "RR not valid");

      RRBuf := (16#aa#, 16#aa#, 16#00#, 16#01#, 16#00#, 16#00#, 16#00#, 16#00#,
                16#00#, 16#00#);
      begin
         Validate (RR);
         Assert (Condition => False, Message => "RR type should be invalid");
      exception
         when E : Invalid_Resource_Record =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E) =
                      "Unknown resource record type",
                    Message   => "Unexpected message reading unknown RR type");
      end;

      RRBuf := (16#00#, 16#01#, 16#aa#, 16#aa#, 16#00#, 16#00#, 16#00#, 16#00#,
                16#00#, 16#00#);

      begin
         Validate (RR);
         Assert (Condition => False, Message => "RR class should be invalid");
      exception
         when E : Invalid_Resource_Record =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E) =
                      "Unknown resource record class",
                    Message   => "Unexpected message reading unknown RR class");
      end;

--  begin read only
   end Test_Validate;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end DNS.Resource_Record.Raw.Test_Data.Tests;
