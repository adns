--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;

with DNS.Domain;

package DNS.Resource_Record
is

   use type Ada.Streams.Stream_Element_Count;

   --  Type for the length of resource record data.
   subtype RDLength_Type is
     Ada.Streams.Stream_Element_Count range 0 .. 2 ** 16 - 1;

   --  Type to represent resource records with data of a certain length.
   type Resource_Record_Type (Len : RDLength_Type) is abstract tagged private;

   --  Type used to generically construct resource record objects and
   --  deserializing the resource record data.
   type Create_Params (Len : Ada.Streams.Stream_Element_Count) is private;

   --  Construct a resource record from the given parameters.
   function Create
     (Params : not null access Create_Params)
      return Resource_Record_Type
      is abstract;

   --  Validate the data of a resource record, if not already done in a custom
   --  constructor. Raises an exception if the data is invalid for a specific
   --  resource record type.
   procedure Validate (RR : Resource_Record_Type) is abstract;

   --  Return a string representation of the resource record's data.
   function Get_Data_Str (RR : Resource_Record_Type) return String is abstract;

   --  Return the associated name of a resource record.
   function Get_Domain
     (RR : Resource_Record_Type'Class)
      return Domain.Domain_Type;

   --  Return the type of a resource record.
   function Get_Type (RR : Resource_Record_Type'Class) return RR_Type_Type;

   --  Return the class of a resource record.
   function Get_Class (RR : Resource_Record_Type'Class) return RR_Class_Type;

   --  Get the raw data of a resource record.
   function Get_Data
     (RR : Resource_Record_Type'Class)
      return Ada.Streams.Stream_Element_Array;

   --  Parse the resource record starting at the given index in the message.
   --  Returns the length of the resource record. Raises an
   --  Unknown_Resource_Record exception if the resource record's type is not
   --  known and an Invalid_Resource_Record exception if the resource record is
   --  invalid for some reason.
   function Deserialize
     (Message :     Ada.Streams.Stream_Element_Array;
      Index   :     Ada.Streams.Stream_Element_Offset;
      Length  : out Ada.Streams.Stream_Element_Count)
      return Resource_Record_Type'Class
     with Pre => Index in Message'Range;

   Invalid_Resource_Record : exception;

   --------------------
   -- Generic raw RR --
   --------------------

   --  Generic resource record.
   type Raw_Resource_Record_Type is new Resource_Record_Type with private;

   overriding
   function Create
     (Params : not null access Create_Params)
      return Raw_Resource_Record_Type;

   --  Validate the data. Nothing to be done for this raw type.
   overriding
   procedure Validate (RR : Raw_Resource_Record_Type) is null;

   --  Returns a hex string of the resource record data.
   overriding
   function Get_Data_Str (RR : Raw_Resource_Record_Type) return String;

   ----------
   -- A RR --
   ----------

   --  A RR containing an IPv4 address.
   type A_Resource_Record_Type is new Raw_Resource_Record_Type with private;

   --  Check for a valid IPv4 address as data.
   overriding
   procedure Validate (RR : A_Resource_Record_Type);

   --  Returns a string representation of an IPv4 address.
   overriding
   function Get_Data_Str (RR : A_Resource_Record_Type) return String;

   -------------
   -- AAAA RR --
   -------------

   --  AAAA RR containing an IPv6 address.
   type AAAA_Resource_Record_Type is new Raw_Resource_Record_Type with private;

   --  Check for a valid IPv6 address as data.
   overriding
   procedure Validate (RR : AAAA_Resource_Record_Type);

   --  Returns a string representation of an IPv6 address.
   overriding
   function Get_Data_Str (RR : AAAA_Resource_Record_Type) return String;

   --------------
   -- CNAME RR --
   --------------

   --  CNAME RR containing a domain name.
   type CNAME_Resource_Record_Type is new Raw_Resource_Record_Type with
     private;

   overriding
   function Create
     (Params : not null access Create_Params)
      return CNAME_Resource_Record_Type;

   --  Returns a string representation of the canonical domain name.
   overriding
   function Get_Data_Str (RR : CNAME_Resource_Record_Type) return String;

   --  Returns the canonical domain name.
   function Get_Value
     (RR : CNAME_Resource_Record_Type)
      return Domain.Domain_Type;

private

   type Resource_Record_Type (Len : RDLength_Type) is abstract tagged record
      RType  : RR_Type_Type;
      RClass : RR_Class_Type;
      Domain : DNS.Domain.Domain_Type;
      Data   : Ada.Streams.Stream_Element_Array (1 .. Len) := (others => 0);
   end record;

   type Raw_Resource_Record_Type is new Resource_Record_Type with null record;

   type A_Resource_Record_Type is new Raw_Resource_Record_Type with
     null record;

   type AAAA_Resource_Record_Type is new Raw_Resource_Record_Type with
     null record;

   type CNAME_Resource_Record_Type is new Raw_Resource_Record_Type with record
      CName : DNS.Domain.Domain_Type;
   end record;

   type Create_Params (Len : Ada.Streams.Stream_Element_Count) is record
      Message  : Ada.Streams.Stream_Element_Array (1 .. Len);
      Index    : Ada.Streams.Stream_Element_Offset;
      RType    : RR_Type_Type;
      RClass   : RR_Class_Type;
      RDLength : RDLength_Type;
      Domain   : DNS.Domain.Domain_Type;
   end record;

end DNS.Resource_Record;
