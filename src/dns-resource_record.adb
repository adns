--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Exceptions;
with Ada.Tags.Generic_Dispatching_Constructor;

with Anet;

with DNS.Resource_Record.Raw;

package body DNS.Resource_Record
is

   --  Return the tag to create an object for the given resource record type.
   function To_Tag
     (RType  : RR_Type_Type;
      RClass : RR_Class_Type)
      return Ada.Tags.Tag;

   --  Create an object for the given resource record.
   function Create_RR_Object
     (Message  : Ada.Streams.Stream_Element_Array;
      Index    : Ada.Streams.Stream_Element_Offset;
      RType    : RR_Type_Type;
      RClass   : RR_Class_Type;
      RDLength : RDLength_Type;
      Domain   : DNS.Domain.Domain_Type)
      return Resource_Record_Type'Class;

   -------------------------------------------------------------------------

   function To_Tag
     (RType  : RR_Type_Type;
      RClass : RR_Class_Type)
      return Ada.Tags.Tag
   is
      pragma Unreferenced (RClass);

      Tag_Map : constant array (RR_Type_Type) of Ada.Tags.Tag
        := (A      => A_Resource_Record_Type'Tag,
            AAAA   => AAAA_Resource_Record_Type'Tag,
            CNAME  => CNAME_Resource_Record_Type'Tag,
            others => Raw_Resource_Record_Type'Tag);
   begin
      return Tag_Map (RType);
   end To_Tag;

   -------------------------------------------------------------------------

   function Get_Domain
     (RR : Resource_Record_Type'Class)
      return Domain.Domain_Type
   is (RR.Domain);

   -------------------------------------------------------------------------

   function Get_Type (RR : Resource_Record_Type'Class) return RR_Type_Type
   is (RR.RType);

   -------------------------------------------------------------------------

   function Get_Class (RR : Resource_Record_Type'Class) return RR_Class_Type
   is (RR.RClass);

   -------------------------------------------------------------------------

   function Get_Data
     (RR : Resource_Record_Type'Class)
      return Ada.Streams.Stream_Element_Array
   is (RR.Data);

   -------------------------------------------------------------------------

   function Create_RR_Object
     (Message  : Ada.Streams.Stream_Element_Array;
      Index    : Ada.Streams.Stream_Element_Offset;
      RType    : RR_Type_Type;
      RClass   : RR_Class_Type;
      RDLength : RDLength_Type;
      Domain   : DNS.Domain.Domain_Type)
      return Resource_Record_Type'Class
   is
      function Create_RR is new Ada.Tags.Generic_Dispatching_Constructor
        (T           => Resource_Record_Type,
         Parameters  => Create_Params,
         Constructor => Create);

      Tag    : constant Ada.Tags.Tag
        := To_Tag (RType => RType, RClass => RClass);
      Params : aliased Create_Params
        := (Len      => Message'Length,
            Message  => Message,
            --  Enforce start index of 1
            Index    => Index - Message'First + 1,
            RType    => RType,
            RClass   => RClass,
            RDLength => RDLength,
            Domain   => Domain);
      RR     : constant Resource_Record_Type'Class
        := Create_RR (The_Tag => Tag,
                      Params  => Params'Access);
   begin
      RR.Validate;
      return RR;
   end Create_RR_Object;

   -------------------------------------------------------------------------

   function Deserialize
     (Message :     Ada.Streams.Stream_Element_Array;
      Index   :     Ada.Streams.Stream_Element_Offset;
      Length  : out Ada.Streams.Stream_Element_Count)
      return Resource_Record_Type'Class
   is
      --  Resource record as per RFC 1035, section 4.1.3:
      --
      --                                  1  1  1  1  1  1
      --    0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                                               |
      --  /                                               /
      --  /                      NAME                     /
      --  |                                               |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                      TYPE                     |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                     CLASS                     |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                      TTL                      |
      --  |                                               |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                   RDLENGTH                    |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
      --  /                     RDATA                     /
      --  /                                               /
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

      Cur        : Ada.Streams.Stream_Element_Offset := Index;
      Domain     : DNS.Domain.Domain_Type;
      Domain_Len : Ada.Streams.Stream_Element_Count;

      Buf        : Resource_Record.Raw.Raw_RR_Header_Buffer_Type;
      RR         : Resource_Record.Raw.Raw_RR_Header_Type;

      ----------------------------------------------------------------------

      --  Raise an exception if there isn't enough remaining data.
      procedure Check_Room (Len : Ada.Streams.Stream_Element_Count);

      procedure Check_Room (Len : Ada.Streams.Stream_Element_Count)
      is
      begin
         if Cur + Len - 1 > Message'Last then
            raise Invalid_Resource_Record with "Resource record incomplete";
         end if;
      end Check_Room;
   begin
      begin
         Domain := DNS.Domain.Deserialize (Message => Message,
                                           Index   => Cur,
                                           Length  => Domain_Len);
      exception
         when E : DNS.Domain.Invalid_Domain =>
            raise Invalid_Resource_Record with "Domain in RR invalid - "
              & Ada.Exceptions.Exception_Message (E);
      end;

      Cur := Cur + Domain_Len;

      Check_Room (Buf'Length);

      Buf (Buf'Range) := Message (Cur .. Cur + Buf'Length - 1);
      RR := Resource_Record.Raw.To_Raw_RR_Header_Type (Buf);
      Raw.Validate (RR);

      Cur := Cur + Buf'Length;

      Check_Room (RR.RDLength);
      Length := Domain_Len + Buf'Length + RR.RDLength;

      return Create_RR_Object
        (Message  => Message,
         Index    => Cur,
         RType    => RR.RType,
         RClass   => RR.RClass,
         RDLength => RR.RDLength,
         Domain   => Domain);
   end Deserialize;

   -------------------------------------------------------------------------

   function Create
     (Params : not null access Create_Params)
      return Raw_Resource_Record_Type
   is
      Data : constant Ada.Streams.Stream_Element_Array (1 .. Params.RDLength)
        := Params.Message (Params.Index .. Params.Index + Params.RDLength - 1);
   begin
      return Raw_Resource_Record_Type'
        (Len    => Params.RDLength,
         RType  => Params.RType,
         RClass => Params.RClass,
         Domain => Params.Domain,
         Data   => Data);
   end Create;

   -------------------------------------------------------------------------

   function Get_Data_Str (RR : Raw_Resource_Record_Type) return String
   is
   begin
      return Anet.To_Hex (Data => RR.Data);
   end Get_Data_Str;

   -------------------------------------------------------------------------

   procedure Validate (RR : A_Resource_Record_Type)
   is
      Length : constant Natural := RR.Data'Length;
   begin
      if Length /= Anet.IPv4_Addr_Type'Length then
         raise Invalid_Resource_Record with "A RR data size" & Length'Image
           & ", expected" & Anet.IPv4_Addr_Type'Length'Img;
      end if;
   end Validate;

   -------------------------------------------------------------------------

   function Get_Data_Str (RR : A_Resource_Record_Type) return String
   is
      Addr : Anet.IPv4_Addr_Type;
      Idx  : Ada.Streams.Stream_Element_Offset := RR.Data'First;
   begin
      for B of Addr loop
         B := Anet.Byte (RR.Data (Idx));
         Idx := Idx + 1;
      end loop;

      return Anet.To_String (Address => Addr);
   end Get_Data_Str;

   -------------------------------------------------------------------------

   procedure Validate (RR : AAAA_Resource_Record_Type)
   is
      Length : constant Natural := RR.Data'Length;
   begin
      if Length /= Anet.IPv6_Addr_Type'Length then
         raise Invalid_Resource_Record with "AAAA RR data size" & Length'Image
           & ", expected" & Anet.IPv6_Addr_Type'Length'Img;
      end if;
   end Validate;

   -------------------------------------------------------------------------

   function Get_Data_Str (RR : AAAA_Resource_Record_Type) return String
   is
      Addr : Anet.IPv6_Addr_Type;
      Idx  : Ada.Streams.Stream_Element_Offset := RR.Data'First;
   begin
      for B of Addr loop
         B := Anet.Byte (RR.Data (Idx));
         Idx := Idx + 1;
      end loop;

      return Anet.To_String (Address => Addr);
   end Get_Data_Str;

   -------------------------------------------------------------------------

   function Create
     (Params : not null access Create_Params)
      return CNAME_Resource_Record_Type
   is
      CName  : Domain.Domain_Type;
      Length : Ada.Streams.Stream_Element_Count;
      Data   : constant Ada.Streams.Stream_Element_Array (1 .. Params.RDLength)
        := Params.Message (Params.Index .. Params.Index + Params.RDLength - 1);
   begin
      begin
         CName := Domain.Deserialize (Message => Params.Message,
                                      Index   => Params.Index,
                                      Length  => Length);
      exception
         when E : Domain.Invalid_Domain =>
            raise Invalid_Resource_Record with "CNAME invalid - "
              & Ada.Exceptions.Exception_Message (E);
      end;

      if Length /= Params.RDLength then
         raise Invalid_Resource_Record with "CNAME exceeds RR bounds";
      end if;

      return CNAME_Resource_Record_Type'
        (Len    => Params.RDLength,
         RType  => Params.RType,
         RClass => Params.RClass,
         Domain => Params.Domain,
         Data   => Data,
         CName  => CName);
   end Create;

   -------------------------------------------------------------------------

   function Get_Data_Str (RR : CNAME_Resource_Record_Type) return String
   is (Domain.Get_Domain (RR.CName));

   -------------------------------------------------------------------------

   function Get_Value
     (RR : CNAME_Resource_Record_Type)
      return Domain.Domain_Type
   is (RR.CName);

end DNS.Resource_Record;
