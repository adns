--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;
with Ada.Unchecked_Conversion;

with System;

with Anet;

private package DNS.Resource_Record.Raw
is

   --  Static data of a resource record between domain name and resource data.
   type Raw_RR_Header_Type is record
      RType    : RR_Type_Type;
      RClass   : RR_Class_Type;
      TTL      : Anet.Word32;
      RDLength : RDLength_Type;
   end record
     with
       Size                 => 80,
       Alignment            => 1,
       Bit_Order            => System.High_Order_First,
       Scalar_Storage_Order => System.High_Order_First;

   for Raw_RR_Header_Type use record
      RType    at 0 range 0 .. 15;
      RClass   at 2 range 0 .. 15;
      TTL      at 4 range 0 .. 31;
      RDLength at 8 range 0 .. 15;
   end record;

   Upper_Index : constant Ada.Streams.Stream_Element_Offset
     := Raw_RR_Header_Type'Object_Size /
       Ada.Streams.Stream_Element'Object_Size;

   subtype Raw_RR_Header_Buffer_Type is
     Ada.Streams.Stream_Element_Array (1 .. Upper_Index);

   --  Convert from a buffer.
   function To_Raw_RR_Header_Type is
     new Ada.Unchecked_Conversion
       (Raw_RR_Header_Buffer_Type, Raw_RR_Header_Type);

   --  Validate the static resource record data.
   procedure Validate (RR : Raw_RR_Header_Type);

end DNS.Resource_Record.Raw;
