--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body DNS.Resource_Record.Raw
is

   -------------------------------------------------------------------------

   procedure Validate (RR : Raw_RR_Header_Type)
   is
   begin
      if not RR.RType'Valid then
         raise Invalid_Resource_Record with "Unknown resource record type";
      end if;

      if not RR.RClass'Valid then
         raise Invalid_Resource_Record with "Unknown resource record class";
      end if;
   end Validate;

end DNS.Resource_Record.Raw;
