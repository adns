--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package DNS.Constants
is

   --  The maximum number of pointers followed when parsing compressed domain
   --  names in messages.
   Max_Pointers_Followed   : constant Natural  := 10;

   --  The maximum number of canonical redirects (CNAMEs) followed when
   --  parsing DNS responses.
   Max_Canonical_Redirects : constant Natural  := 10;

   --  The buffer size used when receiving DNS responses.
   Transmit_Buffer_Size    : constant Positive := 1024;

end DNS.Constants;
