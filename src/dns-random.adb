--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Sequential_IO;

package body DNS.Random
is

   Random_Source : constant String := "/dev/urandom";

   package S_IO is new Ada.Sequential_IO (Element_Type => Anet.Double_Byte);

   -------------------------------------------------------------------------

   function Get return Anet.Double_Byte
   is
      use S_IO;

      Input_File : File_Type;
      Value      : Anet.Double_Byte := 0;
   begin
      begin
         Open (File => Input_File,
               Mode => In_File,
               Name => Random_Source,
               Form => "shared=yes");

      exception
         when others =>
            raise Random_Source_Error with "Could not open random source "
              & Random_Source;
      end;

      Read (File => Input_File,
            Item => Value);

      Close (File => Input_File);

      return Value;
   end Get;

end DNS.Random;
