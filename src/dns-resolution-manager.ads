--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private with Ada.Containers.Doubly_Linked_Lists;
private with Ada.Containers.Ordered_Maps;
private with Ada.Real_Time;

private with Adns_Util.Timing_Events;

with DNS.Resolution.List;
with DNS.Server.List;

private with DNS.Resolution.Transmit;
private with DNS.Server.Manager;
private with DNS.Socket.Instances;

package DNS.Resolution.Manager
is

   --  Type to be implemented to receive the result of submitted resolutions.
   type Resolutions_Handler is abstract tagged private;

   --  Called when the submitted resolutions have finished.
   procedure Result
     (Handler     : in out Resolutions_Handler;
      Resolutions :        List.Resolution_List) is abstract;

   --  Initiate the resolution of a domain name. The given handler is
   --  called once the resolution finished.
   procedure Resolve
     (Resolution : Resolution_Type;
      Handler    : Resolutions_Handler'Class);

   --  Initiate multiple resolutions. The given handler is called once all
   --  resolutions have finished.
   procedure Resolve
     (Resolutions : List.Resolution_List;
      Handler     : Resolutions_Handler'Class);

   --  Set the servers to be used for (future) resolutions.
   procedure Set_Servers (Servers : Server.List.Server_List);

   --  Start handling resolutions.
   procedure Start;

   --  Stop handling resolutions.
   procedure Stop;

private

   type Resolutions_Handler is abstract tagged null record;

   package RHH is new Ada.Containers.Indefinite_Holders
     (Element_Type => Resolutions_Handler'Class);

   use type ID_Type;

   --  Type to identify multiple submitted resolutions.
   subtype Submitted_Resolutions_Id_Type is ID_Type;

   --  Type used to manage multiple submitted resolutions.
   type Submitted_Resolutions_Type is record
      Resolutions : List.Resolution_List;
      Handler     : RHH.Holder;
   end record;

   package SROS is new Ada.Containers.Ordered_Maps
     (Element_Type => Submitted_Resolutions_Type,
      Key_Type     => Submitted_Resolutions_Id_Type);

   type Submitted_Resolutions_Map is new SROS.Map with null record;

   package SRIL is new Ada.Containers.Doubly_Linked_Lists
     (Element_Type => Submitted_Resolutions_Id_Type);

   type Submitted_Resolutions_Id_List is new SRIL.List with null record;

   --  Type used to track individual registered resolutions.
   type Managed_Resolution_Type is record
      Resolution : Resolution_Type;
      Submitters : Submitted_Resolutions_Id_List;
   end record;

   package MRL is new Ada.Containers.Doubly_Linked_Lists
     (Element_Type => Managed_Resolution_Type);

   type Managed_Resolution_List is new MRL.List with null record;

   --  Type for active resolutions.  Protected because three different tasks
   --  operate on it, initally the main task, then the timer task and
   --  eventually the notifier task.
   protected type Active_Resolution_Type
   is

      --  Initialize the active resolution with the given socket, which
      --  is initialized.
      procedure Init
        (Socket  : DNS.Socket.Socket_Access;
         Managed : Managed_Resolution_Type;
         Servers : Server.List.Server_List);

      --  Check if this object is currently used.
      function In_Use return Boolean;

      --  Check if this object is currently used to resolve the given query.
      function In_Use (Query : Message.Query.Query_Type) return Boolean;

      --  Add a submitter to this resolution (make sure to prevent that nobody
      --  calls Clear between checking In_Use and adding a submitter).
      procedure Add_Submitter (Id : Submitted_Resolutions_Id_Type);

      --  Check/transmit on the socket, returns the state of the resolution
      --  and the time for the next check, if necessary.
      procedure Transmit
        (State     : out Resolution_State_Type;
         Next_Call : out Ada.Real_Time.Time);

      --  Clear this active resolution (closes the socket) and return the
      --  managed resolution.
      procedure Clear (Managed : out Managed_Resolution_Type);

   private

      Used    : Boolean := False;
      Trans   : Resolution.Transmit.Resolution_Transmit_Type;
      Managed : Managed_Resolution_Type;

   end Active_Resolution_Type;

   --  Type for indices into the array of active resolutions.
   subtype Active_Resolution_Index is Socket.Instances.Socket_Index;

   --  Type for a list of active resolutions.
   type Active_Resolution_Array is array (Active_Resolution_Index) of
     Active_Resolution_Type;

   --  List of active resolutions.
   Active_Resolutions : Active_Resolution_Array := (others => <>);

   --  Manager for active DNS resolutions.
   protected type Resolution_Manager_Type
   is

      --  Start the resolution of a list of domain names.
      procedure Resolve (Submission : Submitted_Resolutions_Type);

      --  Set the servers currently in use.
      procedure Set_Servers (Servers : Server.List.Server_List);

      --  Complete a single resolution.
      procedure Complete
        (Index   :     Active_Resolution_Index;
         Managed : out Managed_Resolution_Type);

      --  Update a resolution in the given list of submitted resolutions.
      procedure Update
        (Id          :     Submitted_Resolutions_Id_Type;
         Resolution  :     Resolution_Type;
         Any_Pending : out Boolean);

      --  Remove a list of submitted resolutions after they are complete.
      procedure Remove
        (Id        :     Submitted_Resolutions_Id_Type;
         To_Notify : out Submitted_Resolutions_Type);

   private

      Server_Manager : Server.Manager.Server_Manager_Type;
      Idle_Count     : Natural := Active_Resolution_Array'Length;
      Waiting        : Managed_Resolution_List;
      Submitted      : Submitted_Resolutions_Map;

   end Resolution_Manager_Type;

   --  Global instance of the resolution manager.
   Resolution_Manager : Resolution_Manager_Type;

   --  Timer event used to check for data on sockets.
   type Transmit_Event_Type
     (Index : Active_Resolution_Index)
   is new Adns_Util.Timing_Events.Event_Type with null record;

   --  Check for data on sockets.
   overriding
   procedure Trigger (Event : in out Transmit_Event_Type);

end DNS.Resolution.Manager;
