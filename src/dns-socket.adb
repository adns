--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with GNAT.OS_Lib;

with Anet.Constants;

with DNS.Random;

package body DNS.Socket
is

   --  Bind the given socket to a random port.  Try the given port first if
   --  it is usable.
   generic
      type Inet_Socket_Type is limited new
        Anet.Sockets.Inet.Inet_Socket_Type with private;
      type Address_Type is private;

      with procedure Bind
        (Socket     : in out Inet_Socket_Type;
         Address    :        Address_Type;
         Port       :        Anet.Port_Type;
         Reuse_Addr :        Boolean);

      Any_Addr : Address_Type;
   procedure Bind
     (Socket : in out Inet_Socket_Type;
      Port   : in out Anet.Port_Type);

   -------------------------------------------------------------------------

   procedure Bind
     (Socket : in out Inet_Socket_Type;
      Port   : in out Anet.Port_Type)
   is
      use type Anet.Port_Type;

      Max_Privileged : constant Anet.Port_Type := 1024;
   begin
      loop
         if Port > Max_Privileged then
            begin
               Bind (Socket     => Socket,
                     Address    => Any_Addr,
                     Port       => Port,
                     Reuse_Addr => False);
               exit;
            exception
               when Anet.Socket_Error =>
                  if GNAT.OS_Lib.Errno /= Anet.Constants.Sys.EADDRINUSE then
                     raise;
                  end if;
            end;
         end if;
         Port := Random.Get;
      end loop;
   end Bind;

   -------------------------------------------------------------------------

   procedure Init (Socket : in out Socket_Type)
   is
      procedure Bind_V4 is new Bind
        (Inet_Socket_Type => Anet.Sockets.Inet.UDPv4_Socket_Type,
         Address_Type     => Anet.IPv4_Addr_Type,
         Bind             => Anet.Sockets.Inet.Bind,
         Any_Addr         => Anet.Any_Addr);
      procedure Bind_V6 is new Bind
        (Inet_Socket_Type => Anet.Sockets.Inet.UDPv6_Socket_Type,
         Address_Type     => Anet.IPv6_Addr_Type,
         Bind             => Anet.Sockets.Inet.Bind,
         Any_Addr         => Anet.Any_Addr_V6);

      Port : Anet.Port_Type := 0;
   begin
      Socket.IPv4_Socket.Init;
      Socket.IPv6_Socket.Init;
      Socket.IPv6_Socket.Set_Socket_Option
        (Level  => Anet.Sockets.IPv6_Level,
         Option => Anet.Sockets.IPv6_V6_Only,
         Value  => True);

      Bind_V4 (Socket => Socket.IPv4_Socket,
               Port   => Port);
      Bind_V6 (Socket => Socket.IPv6_Socket,
               Port   => Port);

      Socket.IPv4_Socket.Set_Nonblocking_Mode;
      Socket.IPv6_Socket.Set_Nonblocking_Mode;

   exception
      when others =>
         Socket.IPv4_Socket.Close;
         Socket.IPv6_Socket.Close;
         raise;
   end Init;

   -------------------------------------------------------------------------

   procedure Close (Socket : in out Socket_Type)
   is
   begin
      Socket.IPv4_Socket.Close;
      Socket.IPv6_Socket.Close;
   end Close;

   -------------------------------------------------------------------------

   procedure Send
     (Socket  : Socket_Type;
      Message : Ada.Streams.Stream_Element_Array;
      Server  : DNS.Server.Server_Type'Class)
   is
      IPv4_Server : DNS.Server.IPv4_Server_Type;
      IPv6_Server : DNS.Server.IPv6_Server_Type;
   begin
      if Server in DNS.Server.IPv4_Server_Type then
         IPv4_Server := DNS.Server.IPv4_Server_Type (Server);
         Socket.IPv4_Socket.Send (Item     => Message,
                                  Dst_Addr => IPv4_Server.Get_Address,
                                  Dst_Port => Server.Get_Port);
      else
         IPv6_Server := DNS.Server.IPv6_Server_Type (Server);
         Socket.IPv6_Socket.Send (Item     => Message,
                                  Dst_Addr => IPv6_Server.Get_Address,
                                  Dst_Port => Server.Get_Port);
      end if;
   end Send;

   -------------------------------------------------------------------------

   function Receive
     (Socket :     Socket_Type;
      Buffer : out Ada.Streams.Stream_Element_Array;
      Last   : out Ada.Streams.Stream_Element_Offset)
      return DNS.Server.Server_Type'Class
   is
      IPv4_Src : Anet.Sockets.Inet.IPv4_Sockaddr_Type;
      IPv6_Src : Anet.Sockets.Inet.IPv6_Sockaddr_Type;
   begin
      --  Try the IPv4 socket first.
      begin
         Socket.IPv4_Socket.Receive (Src  => IPv4_Src,
                                     Item => Buffer,
                                     Last => Last);
         return DNS.Server.Create (IPv4_Src.Addr, IPv4_Src.Port);
      exception
         when Anet.Socket_Error =>
            if GNAT.OS_Lib.Errno = Anet.Constants.Sys.EAGAIN then
               --  Then try the IPv6 socket.
               begin
                  Socket.IPv6_Socket.Receive (Src  => IPv6_Src,
                                              Item => Buffer,
                                              Last => Last);
                  return DNS.Server.Create (IPv6_Src.Addr, IPv6_Src.Port);
               exception
                  when Anet.Socket_Error =>
                     if GNAT.OS_Lib.Errno = Anet.Constants.Sys.EAGAIN then
                        --  No data, caller may try again later.
                        raise No_Data;
                     else
                        raise;
                     end if;
               end;
            else
               raise;
            end if;
      end;
   end Receive;

end DNS.Socket;
