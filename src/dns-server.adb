--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body DNS.Server
is

   -------------------------------------------------------------------------

   function Create
     (Address : String;
      Port    : Port_Type := Server_Port_Default)
      return Server_Type'Class
   is
      IPv4 : Anet.IPv4_Addr_Type;
      IPv6 : Anet.IPv6_Addr_Type;
   begin
      begin
         IPv4 := Anet.To_IPv4_Addr (Address);
         return IPv4_Server_Type'(IPv4 => IPv4,
                                  Port => Port);
      exception
         when Constraint_Error =>
            IPv6 := Anet.To_IPv6_Addr (Address);
            return IPv6_Server_Type'(IPv6 => IPv6,
                                     Port => Port);
      end;
   end Create;

   -------------------------------------------------------------------------

   function Get_Port (Server : Server_Type'Class) return Port_Type
   is (Server.Port);

   -------------------------------------------------------------------------

   function Create
     (Address : Anet.IPv4_Addr_Type;
      Port    : Port_Type)
      return Server_Type'Class
   is
   begin
      return IPv4_Server_Type'(IPv4 => Address,
                               Port => Port);
   end Create;

   -------------------------------------------------------------------------

   function Get_Address
     (Server : IPv4_Server_Type)
      return Anet.IPv4_Addr_Type
   is (Server.IPv4);

   -------------------------------------------------------------------------

   function Create
     (Address : Anet.IPv6_Addr_Type;
      Port    : Port_Type)
      return Server_Type'Class
   is
   begin
      return IPv6_Server_Type'(IPv6 => Address,
                               Port => Port);
   end Create;

   -------------------------------------------------------------------------

   function Get_Address
     (Server : IPv6_Server_Type)
      return Anet.IPv6_Addr_Type
   is (Server.IPv6);

end DNS.Server;
