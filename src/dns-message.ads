--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;

with DNS.Domain;
with DNS.Resource_Record.List;

package DNS.Message
is

   --  DNS message type.
   type Opcode_Type is
     (Op_Query,
      Op_Iquery,
      Op_Status,
      Op_Notify,
      Op_Update);

   --  Result code.
   type Rcode_Type is
     (Rc_No_Error,
      Rc_Format_Error,
      Rc_Server_Failure,
      Rc_Name_Error,
      Rc_Not_Implemented,
      Rc_Refused);

   --  DNS response type.
   type Response_Type is private;

   --  Parse a DNS response.  Raises an expection if the message is invalid,
   --  which includes not being a response.
   function Deserialize
     (Message : Ada.Streams.Stream_Element_Array)
      return Response_Type;

   --  Get the DNS response code.
   function Get_RCode (Response : Response_Type) return Rcode_Type;

   --  Get the queried domain name.
   function Get_QName (Response : Response_Type) return Domain.Domain_Type;

   --  Get the queried RR type.
   function Get_QType (Response : Response_Type) return RR_Type_Type;

   --  Get the list of resource records in the answer section of this response.
   --  By default, only resource records matching the query are returned.
   function Get_Answer
     (Response : Response_Type;
      Filtered : Boolean := True)
      return Resource_Record.List.Resource_Record_List;

   --  Check if the query has actually been answered.
   function Has_Answer (Response : Response_Type) return Boolean;

   --  Returns the final canonical domain name.
   function Get_Canonical
     (Response : Response_Type)
      return Domain.Domain_Type;

   Invalid_Message : exception;

private

   for Opcode_Type use
     (Op_Query  => 0,
      Op_Iquery => 1,
      Op_Status => 2,
      Op_Notify => 4,
      Op_Update => 5);
   for Opcode_Type'Size use 4;

   for Rcode_Type use
     (Rc_No_Error        => 0,
      Rc_Format_Error    => 1,
      Rc_Server_Failure  => 2,
      Rc_Name_Error      => 3,
      Rc_Not_Implemented => 4,
      Rc_Refused         => 5);
   for Rcode_Type'Size use 4;

   type Response_Type is record
      Id        : ID_Type;
      RCode     : Rcode_Type;
      QType     : RR_Type_Type;
      Query     : Domain.Domain_Type;
      Canonical : Domain.Domain_Type;
      Answer    : Resource_Record.List.Resource_Record_List;
      Filtered  : Resource_Record.List.Resource_Record_List;
   end record;

end DNS.Message;
