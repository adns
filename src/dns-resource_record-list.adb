--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body DNS.Resource_Record.List
is

   -------------------------------------------------------------------------

   function Deserialize
     (Message :     Ada.Streams.Stream_Element_Array;
      Index   :     Ada.Streams.Stream_Element_Offset;
      Count   :     Record_Count_Type;
      Length  : out Ada.Streams.Stream_Element_Count)
      return Resource_Record_List
   is
      Idx    : Ada.Streams.Stream_Element_Offset := Index;
      RR_Len : Ada.Streams.Stream_Element_Count;
      List   : Resource_Record_List;
   begin
      Length := 0;

      for X in 1 .. Count loop

         if Idx > Message'Last then
            raise Invalid_Resource_Record with "Resource records in section"
              & " incomplete";
         end if;

         List.Append (New_Item => Deserialize (Message => Message,
                                               Index   => Idx,
                                               Length  => RR_Len));

         Idx := Idx + RR_Len;
         Length := Length + RR_Len;
      end loop;

      return List;
   end Deserialize;

end DNS.Resource_Record.List;
