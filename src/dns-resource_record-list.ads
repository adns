--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Containers.Indefinite_Doubly_Linked_Lists;

package DNS.Resource_Record.List
is

   package Resource_Record_List_Package is new
     Ada.Containers.Indefinite_Doubly_Linked_Lists
       (Element_Type => Resource_Record_Type'Class);

   --  List of resource records.
   type Resource_Record_List is new Resource_Record_List_Package.List with
     null record;

   use type Anet.Double_Byte;

   --  Parses the requested number of resource records starting at the given
   --  index in the message.  Returns the length of all the parsed resource
   --  records.  Raises an Invalid_Resource_Record exception if the parsing
   --  failed for some reason.
   --  Unknown resource records are currently ignored.
   function Deserialize
     (Message : Ada.Streams.Stream_Element_Array;
      Index   : Ada.Streams.Stream_Element_Offset;
      Count   : Record_Count_Type;
      Length  : out Ada.Streams.Stream_Element_Count)
      return Resource_Record_List
     with Pre => Count = 0 or Index in Message'Range;

end DNS.Resource_Record.List;
