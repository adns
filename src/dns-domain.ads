--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;
with Ada.Strings.Unbounded;

package DNS.Domain
is

   --  Validated domain name type.
   type Domain_Type is private;

   --  Validate the given domain name. Raises an Invalid_Domain exception if
   --  the domain name is invalid.
   function Validate
     (Value : Ada.Strings.Unbounded.Unbounded_String)
      return Domain_Type;

   --  Same as above but takes a simple string.
   function Validate
     (Value : String)
      return Domain_Type;

   --  Serialize the given domain name to a stream element array.
   function Serialize
     (Value : Domain_Type)
      return Ada.Streams.Stream_Element_Array;

   use type Ada.Streams.Stream_Element_Offset;

   --  Deserialize the domain name at the given index in the given message.
   --  Returns the length of the name at the given index (may be shorter
   --  than the actual domain name if it is compressed). Raises an
   --  Invalid_Domain exception if the domain name is invalid.
   function Deserialize
     (Message :     Ada.Streams.Stream_Element_Array;
      Index   :     Ada.Streams.Stream_Element_Offset;
      Length  : out Ada.Streams.Stream_Element_Count)
      return Domain_Type
   with Pre => Index in Message'Range;

   --  The validated domain name.
   function Get_Domain (Value : Domain_Type) return String;

   --  Compare two domain names, ignoring character case.
   function "=" (Left, Right : Domain_Type) return Boolean;

   Invalid_Domain : exception;

private

   type Domain_Type is record
      Name : Ada.Strings.Unbounded.Unbounded_String;
   end record;

end DNS.Domain;
