--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;

private with Anet.Sockets.Inet;

with DNS.Server;

package DNS.Socket
is

   --  Type for a DNS socket, which can be used for both IPv4 and IPv6 servers.
   type Socket_Type is tagged limited private;

   --  Access type to pass sockets around.
   type Socket_Access is access all Socket_Type'Class;

   --  Initialize and bind the socket to a randomly allocated local port.
   procedure Init (Socket : in out Socket_Type);

   --  Close the given socket.
   procedure Close (Socket : in out Socket_Type);

   --  Send a message to a DNS server.
   procedure Send
     (Socket  : Socket_Type;
      Message : Ada.Streams.Stream_Element_Array;
      Server  : DNS.Server.Server_Type'Class);

   --  Receive a message from the socket non-blocking.  An exception is raised
   --  if no data was read.  Otherwise, the server address is returned.
   --  Last is the last byte written in the buffer.
   function Receive
     (Socket :     Socket_Type;
      Buffer : out Ada.Streams.Stream_Element_Array;
      Last   : out Ada.Streams.Stream_Element_Offset)
      return Server.Server_Type'Class;

   No_Data : exception;

private

   type Socket_Type is tagged limited record
      IPv4_Socket : Anet.Sockets.Inet.UDPv4_Socket_Type;
      IPv6_Socket : Anet.Sockets.Inet.UDPv6_Socket_Type;
   end record;

end DNS.Socket;
