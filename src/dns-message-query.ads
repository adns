--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;

with DNS.Domain;

package DNS.Message.Query
is

   --  DNS query type.
   type Query_Type is private;

   --  Create a DNS query.
   function Create
     (Id     : DNS.ID_Type;
      Domain : DNS.Domain.Domain_Type;
      QType  : DNS.RR_Type_Type)
      return Query_Type;

   --  Create a DNS query based on another using the given ID.
   function Create
     (Id    : DNS.ID_Type;
      Query : Query_Type)
      return Query_Type;

   --  Serialize the given DNS query to a stream element array.
   function Serialize
     (Value : Query_Type)
      return Ada.Streams.Stream_Element_Array;

   --  Check if the given response matches the query.
   function Matches
     (Query    : Query_Type;
      Response : Response_Type)
      return Boolean;

   --  Check if two queries match (without comparing the Id).
   function Matches
     (Left  : Query_Type;
      Right : Query_Type)
      return Boolean;

   --  Check if the specified query is for the given domain name and type.
   function Matches
     (Query  : Query_Type;
      Domain : DNS.Domain.Domain_Type;
      QType  : DNS.RR_Type_Type)
      return Boolean;

private

   type Query_Type is record
      Id     : DNS.ID_Type;
      Domain : DNS.Domain.Domain_Type;
      QType  : DNS.RR_Type_Type;
   end record;

end DNS.Message.Query;
