--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body DNS.Resolution
is

   -------------------------------------------------------------------------

   function Prepare
     (Domain : DNS.Domain.Domain_Type;
      QType  : RR_Type_Type)
      return Resolution_Type
   is
   begin
      return (State => Pending,
              Query => Message.Query.Create (Id     => 0,
                                             Domain => Domain,
                                             QType  => QType));
   end Prepare;

   -------------------------------------------------------------------------

   function Get_State
     (Resolution : Resolution_Type)
      return Resolution_State_Type
   is (Resolution.State);

   -------------------------------------------------------------------------

   function Get_Response
     (Resolution : Resolution_Type)
      return Message.Response_Type
   is (Resolution.Response);

   -------------------------------------------------------------------------

   function Get_Server
     (Resolution : Resolution_Type)
      return Server.Server_Type'Class
   is (Resolution.Server.Element);

end DNS.Resolution;
