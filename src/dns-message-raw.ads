--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;
with Ada.Unchecked_Conversion;

with System;

private package DNS.Message.Raw
is

   --  Raw DNS header type.
   type Raw_Header_Type is record
      Id      : ID_Type;
      QR      : Boolean;
      OpCode  : Opcode_Type;
      AA      : Boolean;
      TC      : Boolean;
      RD      : Boolean;
      RA      : Boolean;
      Unused  : Boolean;
      AD      : Boolean;
      CD      : Boolean;
      RCode   : Rcode_Type;
      QDCount : Record_Count_Type;
      ANCount : Record_Count_Type;
      NSCount : Record_Count_Type;
      ARCount : Record_Count_Type;
   end record
     with
       Size                 => 96,
       Alignment            => 1,
       Bit_Order            => System.High_Order_First,
       Scalar_Storage_Order => System.High_Order_First;

   for Raw_Header_Type use record
      Id      at 0  range 0 .. 15;
      QR      at 2  range 0 ..  0;
      OpCode  at 2  range 1 ..  4;
      AA      at 2  range 5 ..  5;
      TC      at 2  range 6 ..  6;
      RD      at 2  range 7 ..  7;
      RA      at 3  range 0 ..  0;
      Unused  at 3  range 1 ..  1;
      AD      at 3  range 2 ..  2;
      CD      at 3  range 3 ..  3;
      RCode   at 3  range 4 ..  7;
      QDCount at 4  range 0 .. 15;
      ANCount at 6  range 0 .. 15;
      NSCount at 8  range 0 .. 15;
      ARCount at 10 range 0 .. 15;
   end record;

   use type Ada.Streams.Stream_Element_Offset;

   Upper_Index : constant Ada.Streams.Stream_Element_Offset
     := Raw_Header_Type'Object_Size / Ada.Streams.Stream_Element'Object_Size;

   subtype Raw_Header_Buffer_Type is
     Ada.Streams.Stream_Element_Array (1 .. Upper_Index);

   --  Convert from a buffer.
   function To_Raw_Header_Type is
     new Ada.Unchecked_Conversion (Raw_Header_Buffer_Type, Raw_Header_Type);

   --  Convert to a buffer.
   function To_Raw_Header_Buffer_Type is
     new Ada.Unchecked_Conversion (Raw_Header_Type, Raw_Header_Buffer_Type);

   --  Validate the static DNS header.
   procedure Validate (Header : Raw_Header_Type);

   Null_Raw_Header : constant Raw_Header_Type;

private

   Null_Raw_Header : constant Raw_Header_Type
     := (Id      => 0,
         QR      => False,
         OpCode  => Op_Query,
         AA      => False,
         TC      => False,
         RD      => False,
         RA      => False,
         Unused  => False,
         AD      => False,
         CD      => False,
         RCode   => Rc_No_Error,
         QDCount => 0,
         ANCount => 0,
         NSCount => 0,
         ARCount => 0);

end DNS.Message.Raw;
