--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private with Ada.Containers.Indefinite_Holders;

with DNS.Domain;
with DNS.Message.Query;
with DNS.Server;

package DNS.Resolution
is

   --  State of a resolution.
   type Resolution_State_Type is (Pending, Resolved, Timeout, Failure);

   --  Data of as resolution of a specific RR type for a domain name.
   type Resolution_Type is private;

   --  Prepare a resolution for the given domain name and RR type.
   --  This does not yet allocate a random ID.
   function Prepare
     (Domain : DNS.Domain.Domain_Type;
      QType  : RR_Type_Type)
      return Resolution_Type
     with
       Post => Get_State (Prepare'Result) = Pending;

   --  Get the state of the resolution.
   function Get_State
     (Resolution : Resolution_Type)
      return Resolution_State_Type;

   --  Get the response, once it has been received successfully.
   function Get_Response
     (Resolution : Resolution_Type)
      return Message.Response_Type
     with
       Pre => Get_State (Resolution) = Resolved;

   --  Get the server from which the response has been received.
   function Get_Server
     (Resolution : Resolution_Type)
      return Server.Server_Type'Class
     with
       Pre => Get_State (Resolution) = Resolved;

private

   use type Server.Server_Type;

   package SH is new Ada.Containers.Indefinite_Holders
     (Element_Type => Server.Server_Type'Class);

   type Resolution_Type (State : Resolution_State_Type := Pending) is record
      Query : Message.Query.Query_Type;
      case State is
         when Resolved =>
            Server   : SH.Holder;
            Response : Message.Response_Type;
         when others =>
            null;
      end case;
   end record;

end DNS.Resolution;
