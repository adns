--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Assertions;
with Ada.Exceptions;

with DNS.Constants;
with DNS.Message.Raw;
with DNS.Message.Query.Raw;

package body DNS.Message
is

   -------------------------------------------------------------------------

   function Deserialize
     (Message : Ada.Streams.Stream_Element_Array)
      return Response_Type
   is
      use Ada.Streams;
      use Anet;

      --  DNS message format as per RFC 1035:
      --
      --  +---------------------+
      --  |        Header       |
      --  +---------------------+
      --  |       Question      | the question for the name server
      --  +---------------------+
      --  |        Answer       | RRs answering the question
      --  +---------------------+
      --  |      Authority      | RRs pointing toward an authority
      --  +---------------------+
      --  |      Additional     | RRs holding additional information
      --  +---------------------+

      Idx     : Ada.Streams.Stream_Element_Offset := Message'First;
      Query   : Domain.Domain_Type;
      RR_List : Resource_Record.List.Resource_Record_List;
      Length  : Ada.Streams.Stream_Element_Count;

      HBuf    : DNS.Message.Raw.Raw_Header_Buffer_Type;
      Header  : DNS.Message.Raw.Raw_Header_Type;

      QBuf     : DNS.Message.Query.Raw.Raw_Question_Buffer_Type;
      Question : DNS.Message.Query.Raw.Raw_Question_Type;

      ----------------------------------------------------------------------

      --  Parse a given number of resource records in a particular section.
      procedure Parse_RRs (Count : Record_Count_Type; Section : String);

      --  Process the RRs in the answer section of a response.
      procedure Process_Response (Response : in out Response_Type);

      ----------------------------------------------------------------------

      procedure Parse_RRs (Count : Record_Count_Type; Section : String)
      is
      begin
         RR_List := Resource_Record.List.Deserialize
           (Message => Message,
            Index   => Idx,
            Count   => Count,
            Length  => Length);

         Idx := Idx + Length;
      exception
         when Ada.Assertions.Assertion_Error =>
            raise Invalid_Message with "Message incomplete, unable to parse"
              & " resource records in " & Section & " section";
         when E : Resource_Record.Invalid_Resource_Record =>
            raise Invalid_Message with "Failed parsing " & Section & " section"
              & " - " & Ada.Exceptions.Exception_Message (E);
      end Parse_RRs;

      ----------------------------------------------------------------------

      procedure Process_Response (Response : in out Response_Type)
      is
         use type Domain.Domain_Type;

         Redirects : Natural :=  0;
      begin
         Response.Canonical := Response.Query;

         --  Only process successful responses.
         if Response.RCode /= DNS.Message.Rc_No_Error then
            return;
         end if;

         --  Go through the RRs at least twice to avoid processing RRs of the
         --  correct type for non-canonical names.
         <<Retry_Canonical>>
         for RR of Response.Answer loop
            if RR.Get_Type = CNAME
              and then RR.Get_Domain = Response.Canonical
            then
               Redirects := Redirects + 1;
               if Redirects > Constants.Max_Canonical_Redirects then
                  raise Invalid_Message with "Too many canonical references";
               end if;

               declare
                  C : constant Resource_Record.CNAME_Resource_Record_Type
                    := Resource_Record.CNAME_Resource_Record_Type (RR);
               begin
                  Response.Canonical := C.Get_Value;
               end;

               --  Check for RRs with the canonical name from the beginning,
               --  don't rely on the correct ordering of the RRs.
               goto Retry_Canonical;
            end if;
         end loop;

         for RR of Response.Answer loop
            if RR.Get_Domain = Response.Canonical
              and then RR.Get_Type = Response.QType
            then
               Response.Filtered.Append (New_Item => RR);
            end if;
         end loop;
      end Process_Response;
   begin
      if Message'Length < HBuf'Length then
         raise Invalid_Message with "DNS message too short";
      end if;

      --  Header as per RFC 1035, section 4.1.1:
      --
      --                                  1  1  1  1  1  1
      --    0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                      ID                       |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                    QDCOUNT                    |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                    ANCOUNT                    |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                    NSCOUNT                    |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                    ARCOUNT                    |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

      HBuf (HBuf'Range) := Message (Idx .. Idx + HBuf'Length - 1);
      Idx := Idx + HBuf'Length;
      Header := DNS.Message.Raw.To_Raw_Header_Type (HBuf);
      Raw.Validate (Header);

      if not Header.QR then
         raise Invalid_Message with "Message is not a DNS response";
      elsif Header.OpCode /= Op_Query then
         raise Invalid_Message with "Message is not a query";
      elsif Header.QDCount /= 1 then
         raise Invalid_Message with "Number of questions" &
           Header.QDCount'Image & ", expected 1";
      end if;

      --  Question as per RFC 1035, section 4.1.2:
      --
      --                                  1  1  1  1  1  1
      --    0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                                               |
      --  /                     QNAME                     /
      --  /                                               /
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                     QTYPE                     |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  |                     QCLASS                    |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

      begin
         Query := Domain.Deserialize (Message => Message,
                                      Index   => Idx,
                                      Length  => Length);
      exception
         when Ada.Assertions.Assertion_Error | Constraint_Error =>
            raise Invalid_Message with "Message incomplete, unable to parse"
              & " queried domain name";
         when E : Domain.Invalid_Domain =>
            raise Invalid_Message with "Domain in question invalid - "
              & Ada.Exceptions.Exception_Message (E);
      end;

      Idx := Idx + Length;

      if Idx + QBuf'Length - 1 > Message'Last then
         raise Invalid_Message with "Message incomplete, unable to parse"
           & " question";
      end if;

      QBuf (QBuf'Range) := Message (Idx .. Idx + QBuf'Length - 1);
      Idx := Idx + QBuf'Length;
      Question := DNS.Message.Query.Raw.To_Raw_Question_Type (QBuf);
      DNS.Message.Query.Raw.Validate (Question);

      return R : Response_Type do
         R.Id    := Header.Id;
         R.RCode := Header.RCode;
         R.QType := Question.QType;
         R.Query := Query;

         --  We parse all sections but ignore the latter two
         Parse_RRs (Header.ANCount, "answer");
         R.Answer := RR_List;
         Parse_RRs (Header.NSCount, "authority");
         Parse_RRs (Header.ARCount, "additional");

         if Idx <= Message'Last then
            raise Invalid_Message with "Unparsed data after the DNS response";
         end if;

         Process_Response (R);
      end return;
   end Deserialize;

   -------------------------------------------------------------------------

   function Get_RCode (Response : Response_Type) return Rcode_Type
   is (Response.RCode);

   -------------------------------------------------------------------------

   function Get_QName (Response : Response_Type) return Domain.Domain_Type
   is (Response.Query);

   -------------------------------------------------------------------------

   function Get_QType (Response : Response_Type) return RR_Type_Type
   is (Response.QType);

   -------------------------------------------------------------------------

   function Get_Answer
     (Response : Response_Type;
      Filtered : Boolean := True)
      return Resource_Record.List.Resource_Record_List
   is (if Filtered then Response.Filtered else Response.Answer);

   -------------------------------------------------------------------------

   function Has_Answer (Response : Response_Type) return Boolean
   is (Response.RCode = Rc_No_Error and then not Response.Filtered.Is_Empty);

   -------------------------------------------------------------------------

   function Get_Canonical
     (Response : Response_Type)
      return Domain.Domain_Type
   is (Response.Canonical);

end DNS.Message;
