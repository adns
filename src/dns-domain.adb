--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Characters.Handling;
with Ada.Strings.Unbounded.Equal_Case_Insensitive;
with Ada.Unchecked_Conversion;

with System;

with DNS.Constants;

package body DNS.Domain
is

   --  Validate the given domain name.  The flag indicates whether it was
   --  absolute or not, that is, if it ended with a dot (i.e. at the root,
   --  which is a single dot), see RFC 1034, section 3.1.  An alternative term
   --  for this is fully qualified domain name (FQDN), but people often use
   --  that also for domain names that don't end with a dot.
   procedure Validate_Domain
     (Value    :     Ada.Strings.Unbounded.Unbounded_String;
      Absolute : out Boolean);

   -------------------------------------------------------------------------

   procedure Validate_Domain
     (Value    :     Ada.Strings.Unbounded.Unbounded_String;
      Absolute : out Boolean)
   is
      use Ada.Strings.Unbounded;
      use Ada.Characters.Handling;

      Total_Length, Current_Label_Length    : Natural := 0;
      Current_Character, Previous_Character : Character;

      --  Make sure the current label has a valid length and ends with a letter
      --  or digit and check that the maximum total length is not exceeded.
      procedure Validate_Label;

      ----------------------------------------------------------------------

      procedure Validate_Label
      is
      begin
         if Current_Label_Length = 0 then
            raise Invalid_Domain with "Label empty";
         elsif Current_Label_Length > Max_Label_Length then
            raise Invalid_Domain with "Label too long";
         elsif not Is_Alphanumeric (Previous_Character) then
            raise Invalid_Domain with "Label doesn't end with number or digit";
         end if;
         --  Account for the length byte for each label
         Total_Length := Total_Length + Current_Label_Length + 1;
         if Total_Length >= Max_Domain_Length then
            raise Invalid_Domain with "Domain name too long";
         end if;
      end Validate_Label;
   begin
      Absolute := False;

      --  Treat empty strings as valid.
      if Length (Value) = 0 then
         return;
      end if;

      --  Just the root.
      if Length (Value) = 1 and then Element (Value, 1) = '.' then
         Absolute := True;
         return;
      end if;

      for I in 1 .. Length (Value) loop

         Current_Character := Element (Value, I);

         if Current_Character = '.' then
            Validate_Label;
            Current_Label_Length := 0;
         else
            if Current_Label_Length = 0 then
               if not Is_Alphanumeric (Current_Character) then
                  raise Invalid_Domain with
                    "Label must start with a letter or digit";
               end if;
            else
               if not Is_Alphanumeric (Current_Character)
                 and then Current_Character /= '-'
               then
                  raise Invalid_Domain with
                    "Only letters, digits and hyphens are allowed in labels";
               end if;
            end if;

            Current_Label_Length := Current_Label_Length + 1;
         end if;

         Previous_Character := Current_Character;
      end loop;

      --  If the name didn't end with a dot, check if the last label is valid.
      if Current_Label_Length > 0 then
         Validate_Label;
      else
         Absolute := True;
      end if;
   end Validate_Domain;

   -------------------------------------------------------------------------

   function Validate
     (Value : Ada.Strings.Unbounded.Unbounded_String)
      return Domain_Type
   is
      Name     : Ada.Strings.Unbounded.Unbounded_String := Value;
      Absolute : Boolean;
   begin
      Validate_Domain (Value    => Name,
                       Absolute => Absolute);

      if not Absolute then
         Ada.Strings.Unbounded.Append (Name, ".");
      end if;

      return (Name => Name);
   end Validate;

   -------------------------------------------------------------------------

   function Validate (Value : String) return Domain_Type
   is
   begin
      return Validate (Ada.Strings.Unbounded.To_Unbounded_String (Value));
   end Validate;

   -------------------------------------------------------------------------

   function Serialize
     (Value : Domain_Type)
      return Ada.Streams.Stream_Element_Array
   is
      use Ada.Streams;
      use Ada.Strings.Unbounded;

      Len     : constant Natural      := Length (Value.Name);
      Out_Len : Stream_Element_Offset := Stream_Element_Offset (Len);
      From    : Natural               := 1;
      To      : Natural;
      Label   : Unbounded_String;
   begin
      --  Account for the terminating root.
      if Len > 1 or else (Len = 1 and then Value.Name /= ".") then
         Out_Len := Out_Len + 1;
      end if;

      return Encoding : Stream_Element_Array (1 .. Out_Len) do
         while From <= Len loop
            To := Index (Value.Name, ".", From);

            Label := Unbounded_Slice (Value.Name, From, To - 1);
            Encoding (Stream_Element_Offset (From))
              := Stream_Element (Length (Label));
            for I in 1 .. Length (Label) loop
               Encoding (Stream_Element_Offset (From + I))
                 := Stream_Element (Character'Pos (Element (Label, I)));
            end loop;

            From := To + 1;
         end loop;

         --  The root is signified by a zero-byte.
         Encoding (Out_Len) := Stream_Element (0);
      end return;
   end Serialize;

   -------------------------------------------------------------------------

   function Deserialize
     (Message :     Ada.Streams.Stream_Element_Array;
      Index   :     Ada.Streams.Stream_Element_Offset;
      Length  : out Ada.Streams.Stream_Element_Count)
      return Domain_Type
   is
      use Ada.Streams;
      use Ada.Strings.Unbounded;

      --  Regular label (max. length 2**6-1 = 63):
      --
      --                                  1  1  1  1  1  1
      --    0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  | 0  0|     Length      |      Name/Bytes       |
      --  +--+--+--+--+--+--+--+--+                       +
      --  |                                               |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --
      --  Ends with a zero byte, or a compression label/pointer (offset
      --  relative to the start of the DNS header, i.e. 0 = ID field):
      --
      --                                  1  1  1  1  1  1
      --    0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      --  | 1  1|                Offset                   |
      --  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

      Name         : Unbounded_String;
      Current      : Stream_Element_Offset := Index;
      Pointers     : Natural               := 0;
      Total_Length : Stream_Element_Count  := 0;
      Label_Length : Stream_Element_Count;

      subtype Compression_Offset_Type is
        Stream_Element_Count range 0 .. 2 ** 14 - 1;

      type Compressed_Label is record
         Indicator : Boolean;
         Offset    : Compression_Offset_Type;
      end record
        with
          Size                 => 16,
          Alignment            => 1,
          Bit_Order            => System.High_Order_First,
          Scalar_Storage_Order => System.High_Order_First;

      for Compressed_Label use record
         Indicator at 0 range 0 .. 1;
         Offset    at 0 range 2 .. 15;
      end record;

      subtype Target_Buffer is Stream_Element_Array (1 .. 2);

      TBuf   : Target_Buffer;
      Target : Compressed_Label;

      function To_Compressed_Label is
        new Ada.Unchecked_Conversion (Target_Buffer, Compressed_Label);
   begin
      Length := 0;
      Parse_Labels :
      loop
         Follow_Pointers :
         loop
            Label_Length := Stream_Element_Count (Message (Current));

            --  Check if the length indicates an offset (see diagrams above).
            exit Follow_Pointers when Label_Length < 2#11000000#;

            --  Compressed name, i.e. this is an offset.  The two bytes for
            --  the first pointer count towards the length of the initial
            --  name (e.g. to skip it during parsing) but not towards the total
            --  length of the domain name.
            if Pointers = 0 then
               Length := Length + 2;
            end if;

            if Current + 1 > Message'Last then
               raise Invalid_Domain with "Pointer incomplete";
            end if;

            TBuf (1 .. 2) := Message (Current .. Current + 1);
            Target := To_Compressed_Label (TBuf);

            Current  := Message'First + Target.Offset;
            Pointers := Pointers + 1;
            if Target.Offset = 0 or else Current > Message'Last then
               raise Invalid_Domain with "Invalid pointer";
            elsif Pointers > Constants.Max_Pointers_Followed then
               raise Invalid_Domain with "Pointer limit reached";
            end if;
         end loop Follow_Pointers;

         if Pointers = 0 then
            Length := Length + Label_Length + 1;
         end if;
         Total_Length := Total_Length + Label_Length + 1;

         --  We're done when we reach the root label.
         if Label_Length = 0 then
            if Total_Length = 1 then
               Append (Name, ".");
            end if;
            exit Parse_Labels;
         end if;

         --  The latter two checks ensure there is space for the root label.
         if Label_Length > Max_Label_Length then
            raise Invalid_Domain with "Label length invalid";
         elsif Current + Label_Length >= Message'Last then
            raise Invalid_Domain with "Label too long";
         elsif Total_Length >= Max_Domain_Length then
            raise Invalid_Domain with "Domain name too long";
         end if;

         declare
            Label : String (1 .. Integer (Label_Length));
            Char  : Stream_Element;
         begin
            for I in Label'Range loop
               Char := Message (Current + Stream_Element_Offset (I));
               Label (I) := Character'Val (Char);
            end loop;
            Append (Name, Label);
            Append (Name, ".");
         end;

         Current := Current + Label_Length + 1;
      end loop Parse_Labels;

      return Validate (Name);
   end Deserialize;

   -------------------------------------------------------------------------

   function Get_Domain (Value : Domain_Type) return String
   is (Ada.Strings.Unbounded.To_String (Value.Name));

   -------------------------------------------------------------------------

   --  According to RFC 4343, case insensitivity only affects ASCII letters,
   --  Equal_Case_Insensitive potentially does more.
   function "=" (Left, Right : Domain_Type) return Boolean
   is (Ada.Strings.Unbounded.Equal_Case_Insensitive
       (Left  => Left.Name,
        Right => Right.Name));

end DNS.Domain;
