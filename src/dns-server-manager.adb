--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body DNS.Server.Manager
is

   -------------------------------------------------------------------------

   procedure Set
     (Manager : in out Server_Manager_Type;
      Servers :        List.Server_List)
   is
   begin
      Manager.Data := Servers;
   end Set;

   -------------------------------------------------------------------------

   function Get (Manager : Server_Manager_Type) return List.Server_List
   is (Manager.Data);

   -------------------------------------------------------------------------

   procedure Success
     (Manager : in out Server_Manager_Type;
      Server  :        Server_Type'Class)
   is
      use List.Server_List_Package;

      I : constant Extended_Index := Manager.Data.Find_Index (Item => Server);
   begin
      if I /= No_Index and then I > Manager.Data.First_Index then
         Manager.Data.Delete (Index => I);
         Manager.Data.Prepend (New_Item => Server);
      end if;
   end Success;

end DNS.Server.Manager;
