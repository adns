--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with DNS.Message.Raw;
with DNS.Message.Query.Raw;

package body DNS.Message.Query
is

   -------------------------------------------------------------------------

   function Create
     (Id     : DNS.ID_Type;
      Domain : DNS.Domain.Domain_Type;
      QType  : DNS.RR_Type_Type)
      return Query_Type
   is
   begin
      return (Id => Id, Domain => Domain, QType => QType);
   end Create;

   -------------------------------------------------------------------------

   function Create
     (Id     : DNS.ID_Type;
      Query  : Query_Type)
      return Query_Type
   is
   begin
      return (Id => Id, Domain => Query.Domain, QType => Query.QType);
   end Create;

   -------------------------------------------------------------------------

   function Serialize
     (Value : Query_Type)
      return Ada.Streams.Stream_Element_Array
   is
      use Ada.Streams;

      HBuffer  : DNS.Message.Raw.Raw_Header_Buffer_Type;
      Header   : DNS.Message.Raw.Raw_Header_Type
        := DNS.Message.Raw.Null_Raw_Header;

      QBuffer  : DNS.Message.Query.Raw.Raw_Question_Buffer_Type;
      Question : DNS.Message.Query.Raw.Raw_Question_Type;
   begin
      Header.Id      := Value.Id;
      Header.QR      := False;
      Header.OpCode  := Op_Query;
      --  Request recursive querying.
      Header.RD      := True;
      Header.QDCount := 1;

      HBuffer := DNS.Message.Raw.To_Raw_Header_Buffer_Type (Header);

      Question.QType  := Value.QType;
      Question.QClass := IN_Class;

      QBuffer := DNS.Message.Query.Raw.To_Raw_Question_Buffer_Type (Question);

      declare
         Labels : constant Stream_Element_Array
           := DNS.Domain.Serialize (Value.Domain);
         Len    : constant Stream_Element_Offset
           := HBuffer'Length + Labels'Length + QBuffer'Length;
         Offset : Stream_Element_Offset := HBuffer'Length + 1;
      begin
         return Stream : Stream_Element_Array (1 .. Len)
         do
            Stream (1 .. HBuffer'Length) := HBuffer;
            Stream (Offset .. Offset + Labels'Length - 1) := Labels;
            Offset := Offset + Labels'Length;
            Stream (Offset .. Offset + QBuffer'Length - 1) := QBuffer;
         end return;
      end;
   end Serialize;

   -------------------------------------------------------------------------

   function Matches
     (Query    : Query_Type;
      Response : Response_Type)
      return Boolean
   is
      use type Anet.Double_Byte;
      use type DNS.Domain.Domain_Type;
   begin
      return Query.Id = Response.Id and then
        Query.QType = Response.QType and then
        Query.Domain = Response.Query;
   end Matches;

   -------------------------------------------------------------------------

   function Matches
     (Left  : Query_Type;
      Right : Query_Type)
      return Boolean
   is
      use type DNS.Domain.Domain_Type;
   begin
      return Left.QType = Right.QType and then
        Left.Domain = Right.Domain;
   end Matches;

   -------------------------------------------------------------------------

   function Matches
     (Query  : Query_Type;
      Domain : DNS.Domain.Domain_Type;
      QType  : DNS.RR_Type_Type)
      return Boolean
   is
      use type DNS.Domain.Domain_Type;
   begin
      return Query.QType = QType and then
        Query.Domain = Domain;
   end Matches;

end DNS.Message.Query;
