--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Real_Time;

with DNS.Server.List;
with DNS.Socket;

package DNS.Resolution.Transmit
is

   --  Handles (re-)transmitting a DNS query to a set of servers.
   type Resolution_Transmit_Type is private;

   --  Initialize the transmission of a DNS query.
   procedure Init
     (Transmit : in out Resolution_Transmit_Type;
      Query    :        Message.Query.Query_Type;
      Servers  :        Server.List.Server_List;
      Socket   :        DNS.Socket.Socket_Access);

   --  (Re-)transmit the query and check for a response.  While the state of
   --  the resolution is pending, the time for the next call is returned.
   procedure Transmit
     (Transmit   : in out Resolution_Transmit_Type;
      Resolution : in out Resolution_Type;
      Next_Call  :    out Ada.Real_Time.Time);

   --  Close the given transmission and the associated socket.
   procedure Close
     (Transmit : in out Resolution_Transmit_Type);

private

   type Resolution_Transmit_Type is record
      Query         : Message.Query.Query_Type;
      Servers       : Server.List.Server_List;
      Index         : Natural := 0;
      Retransmits   : Natural := 0;
      Next_Transmit : Ada.Real_Time.Time := Ada.Real_Time.Time_First;
      Socket        : DNS.Socket.Socket_Access;
      Socket_Init   : Boolean := False;
   end record;

end DNS.Resolution.Transmit;
