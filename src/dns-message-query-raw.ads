--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;
with Ada.Unchecked_Conversion;

with System;

package DNS.Message.Query.Raw
is

   --  Raw DNS question suffix (following the name).
   type Raw_Question_Type is record
      QType  : RR_Type_Type;
      QClass : RR_Class_Type;
   end record
     with
       Size                 => 32,
       Alignment            => 1,
       Bit_Order            => System.High_Order_First,
       Scalar_Storage_Order => System.High_Order_First;

   for Raw_Question_Type use record
      QType  at 0  range 0 .. 15;
      QClass at 2  range 0 .. 15;
   end record;

   use type Ada.Streams.Stream_Element_Offset;

   Upper_Index : constant Ada.Streams.Stream_Element_Offset
     := Raw_Question_Type'Object_Size / Ada.Streams.Stream_Element'Object_Size;

   subtype Raw_Question_Buffer_Type is
     Ada.Streams.Stream_Element_Array (1 .. Upper_Index);

   --  Convert from a buffer.
   function To_Raw_Question_Type is
     new Ada.Unchecked_Conversion
       (Raw_Question_Buffer_Type, Raw_Question_Type);

   --  Convert to a buffer.
   function To_Raw_Question_Buffer_Type is
     new Ada.Unchecked_Conversion
       (Raw_Question_Type, Raw_Question_Buffer_Type);

   --  Validate the question suffix.
   procedure Validate (Question : Raw_Question_Type);

end DNS.Message.Query.Raw;
