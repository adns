--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with DNS.Server.List;

package DNS.Server.Manager
is

   --  Manager for DNS server addresses.
   type Server_Manager_Type is private;

   --  Replace the list of DNS server addresses.
   procedure Set
     (Manager : in out Server_Manager_Type;
      Servers :        List.Server_List);

   --  Get the current list of DNS server addresses.
   function Get (Manager : Server_Manager_Type) return List.Server_List;

   --  Report a successful response from the given server in order to
   --  promote it.
   procedure Success
     (Manager : in out Server_Manager_Type;
      Server  :        Server_Type'Class);

private

   type Server_Manager_Type is record
      Data : List.Server_List;
   end record;

end DNS.Server.Manager;
