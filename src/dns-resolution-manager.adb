--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Exceptions;

with Adns_Util.Logger;
with Adns_Util.Timer;

with DNS.Random;

package body DNS.Resolution.Manager
is
   package Logger renames Adns_Util.Logger;

   package ARIL is new Ada.Containers.Doubly_Linked_Lists
     (Element_Type => Active_Resolution_Index);

   --  Queue of complete resolutions.
   protected type Notify_Queue_Type
   is

      --  Enqueue a complete resolution.
      procedure Enqueue (Index : Active_Resolution_Index);

      --  Dequeue a complete resolution, if any is queued.
      entry Dequeue (Index : out Active_Resolution_Index);

   private

      Queue : ARIL.List;

   end Notify_Queue_Type;

   --  Resolutions waiting to get handled.
   Notify_Queue : Notify_Queue_Type;

   --  Task to notify handlers.
   task Notifier is
      entry Start;
      entry Stop;
   end Notifier;

   --  Schedule a transmit at the given time.
   procedure Schedule_Transmit
     (Index : Active_Resolution_Index;
      Time  : Ada.Real_Time.Time);

   -------------------------------------------------------------------------

   procedure Resolve
     (Resolution : Resolution_Type;
      Handler    : Resolutions_Handler'Class)
   is
      Resolutions : List.Resolution_List;
   begin
      Resolutions.Append (New_Item => Resolution);
      Resolve (Resolutions => Resolutions,
               Handler     => Handler);
   end Resolve;

   -------------------------------------------------------------------------

   procedure Resolve
     (Resolutions : List.Resolution_List;
      Handler     : Resolutions_Handler'Class)
   is
      Submission : constant Submitted_Resolutions_Type
        := (Resolutions => Resolutions,
            Handler     => RHH.To_Holder (Handler));
   begin
      if Resolutions.Is_Empty then
         raise Constraint_Error with "No resolutions supplied";
      end if;
      Resolution_Manager.Resolve (Submission);
   end Resolve;

   -------------------------------------------------------------------------

   procedure Set_Servers (Servers : Server.List.Server_List)
   is
   begin
      Resolution_Manager.Set_Servers (Servers => Servers);
   end Set_Servers;

   -------------------------------------------------------------------------

   procedure Start
   is
   begin
      Notifier.Start;
   end Start;

   -------------------------------------------------------------------------

   procedure Stop
   is
   begin
      if not Notifier'Terminated then
         Notifier.Stop;
      end if;
   end Stop;

   -------------------------------------------------------------------------

   procedure Schedule_Transmit
     (Index : Active_Resolution_Index;
      Time  : Ada.Real_Time.Time)
   is
      Ev : Transmit_Event_Type (Index => Index);
   begin
      Ev.Set_Time (At_Time => Time);

      Adns_Util.Timer.Schedule (Event => Ev);
   end Schedule_Transmit;

   -------------------------------------------------------------------------

   protected body Active_Resolution_Type
   is

      ----------------------------------------------------------------------

      procedure Init
        (Socket  : DNS.Socket.Socket_Access;
         Managed : Managed_Resolution_Type;
         Servers : Server.List.Server_List)
      is
      begin
         Active_Resolution_Type.Managed := Managed;
         Resolution.Transmit.Init
           (Transmit => Trans,
            Query    => Managed.Resolution.Query,
            Servers  => Servers,
            Socket   => Socket);
         Used := True;
      end Init;

      ----------------------------------------------------------------------

      function In_Use return Boolean
      is (Used);

      ----------------------------------------------------------------------

      function In_Use (Query : Message.Query.Query_Type) return Boolean
      is (Used and then
          Message.Query.Matches (Query, Managed.Resolution.Query));

      ----------------------------------------------------------------------

      procedure Add_Submitter (Id : Submitted_Resolutions_Id_Type)
      is
      begin
         Managed.Submitters.Append (New_Item => Id);
      end Add_Submitter;

      ----------------------------------------------------------------------

      procedure Transmit
        (State     : out Resolution_State_Type;
         Next_Call : out Ada.Real_Time.Time)
      is
      begin
         Resolution.Transmit.Transmit (Trans, Managed.Resolution, Next_Call);
         State := Managed.Resolution.State;
      end Transmit;

      ----------------------------------------------------------------------

      procedure Clear (Managed : out Managed_Resolution_Type)
      is
      begin
         Managed := Active_Resolution_Type.Managed;
         Active_Resolution_Type.Managed.Submitters.Clear;
         Resolution.Transmit.Close (Transmit => Trans);
         Used := False;
      end Clear;

   end Active_Resolution_Type;

   -------------------------------------------------------------------------

   protected body Resolution_Manager_Type
   is

      ----------------------------------------------------------------------

      --  Check if there already is an active resolution for the given query.
      --  Returns its index if found.
      function Find_Active
        (Query :     Message.Query.Query_Type;
         Index : out Active_Resolution_Index)
         return Boolean
      is
      begin
         for I in Active_Resolutions'Range loop
            if Active_Resolutions (I).In_Use (Query => Query) then
               Index := I;
               return True;
            end if;
         end loop;

         return False;
      end Find_Active;

      ----------------------------------------------------------------------

      --  Check if there already is a resolution for the given query waiting
      --  for an idle socket.  Return a cursor pointing to it if found.
      function Find_Waiting
        (Query : Message.Query.Query_Type)
         return MRL.Cursor
      is
         Pos : MRL.Cursor := Waiting.First;
         R   : Managed_Resolution_Type;
      begin
         while MRL.Has_Element (Position => Pos) loop
            R := MRL.Element (Position => Pos);
            exit when Message.Query.Matches (Query, R.Resolution.Query);
            Pos := MRL.Next (Position => Pos);
         end loop;

         return Pos;
      end Find_Waiting;

      ----------------------------------------------------------------------

      --  "Activate" any waiting resolutions if any sockets are idle.
      procedure Check_Waiting
      is
         Managed : Managed_Resolution_Type;
         Index   : Active_Resolution_Index;
      begin
         while not Waiting.Is_Empty and then Idle_Count > 0 loop
            Managed := Waiting.First_Element;
            Waiting.Delete_First;

            for I in Active_Resolutions'Range loop
               if not Active_Resolutions (I).In_Use then
                  Index := I;
                  exit;
               end if;
            end loop;

            Idle_Count := Idle_Count - 1;

            Active_Resolutions (Index).Init
              (Socket  => Socket.Instances.Sockets (Index)'Access,
               Managed => Managed,
               Servers => Server.Manager.Get (Manager => Server_Manager));

            --  Schedule an event immediately to start the resolution.
            Schedule_Transmit (Index, Ada.Real_Time.Time_First);
         end loop;
      end Check_Waiting;

      ----------------------------------------------------------------------

      procedure Resolve
        (Resolution : Resolution_Type;
         Submitter  : Submitted_Resolutions_Id_Type)
      is
         Query   : Message.Query.Query_Type := Resolution.Query;
         Managed : Managed_Resolution_Type;
         Pos     : MRL.Cursor;
         Index   : Active_Resolution_Index;
      begin
         --  First check if we already have a matching active resolution.
         if Find_Active (Query, Index) then
            Active_Resolutions (Index).Add_Submitter (Id => Submitter);
         else
            --  Or if one is waiting for an idle socket.
            Pos := Find_Waiting (Query);
            if MRL.Has_Element (Position => Pos) then
               Waiting.Reference (Position => Pos).Submitters.Append
                 (New_Item => Submitter);
            else
               Query := Message.Query.Create (Id    => Random.Get,
                                              Query => Query);
               Managed := (Resolution => (State => Pending,
                                          Query => Query),
                           others     => <>);
               Managed.Submitters.Append (New_Item => Submitter);
               Waiting.Append (New_Item => Managed);
            end if;

            Check_Waiting;
         end if;
      end Resolve;

      ----------------------------------------------------------------------

      procedure Resolve (Submission : Submitted_Resolutions_Type)
      is
         Id : Submitted_Resolutions_Id_Type;
      begin
         loop
            Id := Random.Get;
            exit when not Submitted.Contains (Key => Id);
         end loop;

         Submitted.Insert (Key      => Id,
                           New_Item => Submission);

         for Resolution of Submission.Resolutions loop
            Resolve (Resolution => Resolution,
                     Submitter  => Id);
         end loop;
      end Resolve;

      ----------------------------------------------------------------------

      procedure Set_Servers (Servers : Server.List.Server_List)
      is
      begin
         Server.Manager.Set (Manager => Server_Manager,
                             Servers => Servers);
      end Set_Servers;

      ----------------------------------------------------------------------

      procedure Complete
        (Index   :     Active_Resolution_Index;
         Managed : out Managed_Resolution_Type)
      is
      begin
         Active_Resolutions (Index).Clear (Managed => Managed);
         Idle_Count := Idle_Count + 1;

         if Managed.Resolution.State = Resolved then
            Server.Manager.Success
              (Manager => Server_Manager,
               Server  => Managed.Resolution.Server.Element);
         end if;

         Check_Waiting;
      end Complete;

      ----------------------------------------------------------------------

      procedure Update
        (Id          :     Submitted_Resolutions_Id_Type;
         Resolution  :     Resolution_Type;
         Any_Pending : out Boolean)
      is
         Ref : constant SROS.Reference_Type := Submitted.Reference (Key => Id);
         R   : Resolution_Type;
      begin
         Any_Pending := False;

         for Pos in Ref.Resolutions.Iterate loop
            R := Ref.Resolutions.Constant_Reference (Position => Pos);
            if R.State = Pending then
               if Message.Query.Matches (Left  => R.Query,
                                         Right => Resolution.Query)
               then
                  Ref.Resolutions.Replace_Element (Position => Pos,
                                                   New_Item => Resolution);
               else
                  Any_Pending := True;
               end if;
            end if;
         end loop;
      end Update;

      ----------------------------------------------------------------------

      procedure Remove
        (Id        :     Submitted_Resolutions_Id_Type;
         To_Notify : out Submitted_Resolutions_Type)
      is
      begin
         To_Notify := Submitted (Key => Id);
         Submitted.Delete (Key => Id);
      end Remove;

   end Resolution_Manager_Type;

   -------------------------------------------------------------------------

   protected body Notify_Queue_Type
   is

      ----------------------------------------------------------------------

      procedure Enqueue (Index : Active_Resolution_Index)
      is
      begin
         Queue.Append (New_Item => Index);
      end Enqueue;

      ----------------------------------------------------------------------

      entry Dequeue (Index : out Active_Resolution_Index)
        when not Queue.Is_Empty
      is
      begin
         Index := Queue.First_Element;
         Queue.Delete_First;
      end Dequeue;

   end Notify_Queue_Type;

   -------------------------------------------------------------------------

   task body Notifier
   is
      Running     : Boolean;
      Index       : Active_Resolution_Index;
      Managed     : Managed_Resolution_Type;
      Submitted   : Submitted_Resolutions_Type;
      Any_Pending : Boolean;
   begin
      loop
         Running := True;

         select
            accept Start;
         or
            terminate;
         end select;

         while Running loop

            select
               accept Stop do
                  Running := False;
               end Stop;
            else
               select
                  Notify_Queue.Dequeue (Index => Index);

                  Resolution_Manager.Complete (Index   => Index,
                                               Managed => Managed);
                  for Id of Managed.Submitters loop
                     Resolution_Manager.Update
                       (Id          => Id,
                        Resolution  => Managed.Resolution,
                        Any_Pending => Any_Pending);
                     if not Any_Pending then
                        Resolution_Manager.Remove
                          (Id        => Id,
                           To_Notify => Submitted);
                        declare
                           Ext_Handler : Resolutions_Handler'Class
                             := Submitted.Handler.Element;
                        begin
                           Ext_Handler.Result (Submitted.Resolutions);
                        exception
                           when E : others =>
                              Logger.Log
                                (Level   => Logger.Error,
                                 Message => "Ignoring exception in resolution "
                                 & "handler");
                              Logger.Log
                                (Level   => Logger.Error,
                                 Message =>
                                    Ada.Exceptions.Exception_Information
                                   (X => E));
                        end;
                     end if;
                  end loop;

               or
                  delay 0.5;
               end select;
            end select;
         end loop;
      end loop;

   exception
      when E : others =>
         Logger.Log
           (Level   => Logger.Error,
            Message => "Notifier terminated due to error");
         Logger.Log
           (Level   => Logger.Error,
            Message => Ada.Exceptions.Exception_Information (X => E));
   end Notifier;

   -------------------------------------------------------------------------

   procedure Trigger (Event : in out Transmit_Event_Type)
   is
      State     : Resolution.Resolution_State_Type;
      Next_Call : Ada.Real_Time.Time;
   begin
      Active_Resolutions (Event.Index).Transmit
        (State     => State,
         Next_Call => Next_Call);

      if State = Pending then
         Schedule_Transmit (Event.Index, Next_Call);
      else
         Notify_Queue.Enqueue (Index => Event.Index);
      end if;
   end Trigger;

end DNS.Resolution.Manager;
