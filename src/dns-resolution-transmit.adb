--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Exceptions;
with Ada.Streams;

with GNAT.OS_Lib;

with Anet.Constants;

with Adns_Util.Logger;

with DNS.Config;
with DNS.Constants;

package body DNS.Resolution.Transmit
is

   package Logger renames Adns_Util.Logger;

   -------------------------------------------------------------------------

   procedure Init
     (Transmit : in out Resolution_Transmit_Type;
      Query    :        Message.Query.Query_Type;
      Servers  :        Server.List.Server_List;
      Socket   :        DNS.Socket.Socket_Access)
   is
   begin
      Transmit := (Query   => Query,
                   Servers => Servers,
                   Socket  => Socket,
                   others  => <>);
   end Init;

   -------------------------------------------------------------------------

   procedure Transmit
     (Transmit   : in out Resolution_Transmit_Type;
      Resolution : in out Resolution_Type;
      Next_Call  :    out Ada.Real_Time.Time)
   is
      Size   : constant Ada.Streams.Stream_Element_Offset
        := Ada.Streams.Stream_Element_Offset (Constants.Transmit_Buffer_Size);
      Buffer : Ada.Streams.Stream_Element_Array (1 .. Size);
      Last   : Ada.Streams.Stream_Element_Offset;

      Response : Message.Response_Type;

      ----------------------------------------------------------------------

      --  Handle the situation that no data was available on the socket.
      procedure Handle_No_Data;

      --  Check if the given IP address and port is from a known server.
      function Is_Known_Server
        (From : Server.Server_Type'Class)
         return Boolean;

      ----------------------------------------------------------------------

      procedure Handle_No_Data
      is
         use Ada.Real_Time;

         Now  : constant Time := Clock;
         Next : Float;
      begin
         --  Just queue another check unless we've reached the timeout.
         --  This is also the case initially.
         if Now >= Transmit.Next_Transmit then
            if Transmit.Servers.Is_Empty then
               Logger.Log (Level   => Logger.Error,
                           Message => "No DNS servers available");
               Resolution := (State => Failure,
                              Query => Resolution.Query);
               return;
            end if;

            --  Check if we reached the last server.
            --  This is also the case initially.
            if Transmit.Index < Natural (Transmit.Servers.Length) then
               --  If not, use the next server, with the same timeout.
               Transmit.Index := Transmit.Index + 1;
            else
               --  Start with the first server again and increase the timeout.
               Transmit.Index := 1;
               Transmit.Retransmits := Transmit.Retransmits + 1;

               if Transmit.Retransmits > Config.Max_Retransmits then
                  Logger.Log (Level   => Logger.Info,
                              Message => "Giving up after" &
                                Config.Max_Retransmits'Img & " retransmits");
                  Resolution := (State => Timeout,
                                 Query => Resolution.Query);
                  return;
               end if;
            end if;

            Next := Config.Retransmission_Timeout *
              Config.Retransmission_Backoff ** Transmit.Retransmits;
            Transmit.Next_Transmit := Now + To_Time_Span (Duration (Next));

            begin
               Socket.Send
                 (Socket  => Transmit.Socket.all,
                  Message => DNS.Message.Query.Serialize (Transmit.Query),
                  Server  => Transmit.Servers (Transmit.Index));
            exception
               when E : Anet.Socket_Error =>
                  if GNAT.OS_Lib.Errno = Anet.Constants.Sys.ENETUNREACH then
                     --  The server's IP is not reachable.  We could remove it
                     --  from the list to not have to wait for a response that
                     --  can never come.  Or we could immediately switch to the
                     --  next server, but that might result in a weird loop if
                     --  there is no connectivity and we never wait at all.
                     --  Anyway, the network connectivity might change, so
                     --  perhaps just ignoring it like this is a better
                     --  approach.
                     null;
                  else
                     Logger.Log (Level   => Logger.Info,
                                 Message => "Socket error while sending - "
                                   & Ada.Exceptions.Exception_Message (E));
                     Resolution := (State => Failure,
                                    Query => Resolution.Query);
                     return;
                  end if;
            end;
         end if;

         Next_Call := Now + To_Time_Span (Config.Socket_Check_Interval);
      end Handle_No_Data;

      ----------------------------------------------------------------------

      function Is_Known_Server
        (From : Server.Server_Type'Class)
         return Boolean
      is
         Index  : Positive := 1;
      begin
         for Server of Transmit.Servers loop
            if Server = From then
               if Index /= Transmit.Index then
                  --  We received this as response to a previous (re)transmit.
                  Transmit.Index := Index;
               end if;
               return True;
            end if;
            Index := Index + 1;
         end loop;

         return False;
      end Is_Known_Server;
   begin
      if not Transmit.Socket_Init then
         begin
            Transmit.Socket_Init := True;
            Socket.Init (Socket => Transmit.Socket.all);
         exception
            when E : Anet.Socket_Error =>
               Logger.Log (Level   => Logger.Error,
                           Message => "Socket error while initializing - "
                           & Ada.Exceptions.Exception_Message (E));
               Resolution := (State => Failure,
                              Query => Resolution.Query);
               return;
         end;
      end if;

      begin
         declare
            From : constant Server.Server_Type'Class
              := Socket.Receive (Socket => Transmit.Socket.all,
                                 Buffer => Buffer,
                                 Last   => Last);
         begin
            Response := Message.Deserialize (Message => Buffer (1 .. Last));

            --  This might indicate a possible attack (e.g. the ID doesn't
            --  match), perhaps the actual reply gets through, so just ignore.
            if not Message.Query.Matches (Query    => Transmit.Query,
                                          Response => Response)
            then
               raise Message.Invalid_Message with "Response doesn't" &
                 " match query";
            end if;

            --  Similarly to the above, receiving a message from an unknown IP
            --  could indicate an attack, ignore it and wait.
            if not Is_Known_Server (From) then
               raise Message.Invalid_Message with "Response not received" &
                 " from a known server";
            end if;

            Resolution :=
              (State    => Resolved,
               Query    => Resolution.Query,
               Response => Response,
               Server   => SH.To_Holder (Transmit.Servers (Transmit.Index)));
         end;
      exception
         when Socket.No_Data =>
            Handle_No_Data;
         when E : Message.Invalid_Message =>
            --  Treat invalid message as if we received no data.
            Logger.Log (Level   => Logger.Debug,
                        Message => "Received invalid message - "
                        & Ada.Exceptions.Exception_Message (E));
            Handle_No_Data;
         when E : Anet.Socket_Error =>
            Logger.Log (Level   => Logger.Error,
                        Message => "Socket error while receiving - "
                        & Ada.Exceptions.Exception_Message (E));
            Resolution := (State => Failure,
                           Query => Resolution.Query);
      end;
   end Transmit;

   -------------------------------------------------------------------------

   procedure Close (Transmit : in out Resolution_Transmit_Type)
   is
   begin
      if Transmit.Socket_Init then
         Transmit.Socket_Init := False;
         Transmit.Socket.Close;
      end if;
   end Close;

end DNS.Resolution.Transmit;
