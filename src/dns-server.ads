--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package DNS.Server
is

   --  Type for DNS server ports.
   subtype Port_Type is Anet.Double_Byte;

   --  Type for DNS server addresses.
   type Server_Type is tagged private;

   --  Create a DNS server, raises an exception if the address is invalid.
   function Create
     (Address : String;
      Port    : Port_Type := Server_Port_Default)
      return Server_Type'Class;

   --  Get the destination port for the server.
   function Get_Port (Server : Server_Type'Class) return Port_Type;

   --  Type for IPv4 server addresses.
   type IPv4_Server_Type is new Server_Type with private;

   --  Create an IPv4 server.
   function Create
     (Address : Anet.IPv4_Addr_Type;
      Port    : Port_Type)
      return Server_Type'Class;

   --  Get the IPv4 address.
   function Get_Address (Server : IPv4_Server_Type) return Anet.IPv4_Addr_Type;

   --  Type for IPv6 server addresses.
   type IPv6_Server_Type is new Server_Type with private;

   --  Create an IPv6 server.
   function Create
     (Address : Anet.IPv6_Addr_Type;
      Port    : Port_Type)
      return Server_Type'Class;

   --  Get the IPv6 address.
   function Get_Address (Server : IPv6_Server_Type) return Anet.IPv6_Addr_Type;

private

   type Server_Type is tagged record
      Port : Port_Type;
   end record;

   type IPv4_Server_Type is new Server_Type with record
      IPv4 : Anet.IPv4_Addr_Type;
   end record;

   type IPv6_Server_Type is new Server_Type with record
      IPv6 : Anet.IPv6_Addr_Type;
   end record;

end DNS.Server;
