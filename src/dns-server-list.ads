--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Containers.Indefinite_Vectors;

package DNS.Server.List
is

   package Server_List_Package is new
     Ada.Containers.Indefinite_Vectors
       (Index_Type   => Positive,
        Element_Type => Server_Type'Class);

   --  List of DNS servers.  Actually implemented via a vector so individual
   --  servers may be accessed directly.
   type Server_List is new Server_List_Package.Vector with null record;

end DNS.Server.List;
