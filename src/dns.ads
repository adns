--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Anet;

package DNS
is

   --  Maximum length of a domain name, i.e. labels + their length in bytes,
   --  terminated with a zero byte for the root.
   Max_Domain_Length : constant := 255;

   --  Maximum length of a label in a domain name.
   Max_Label_Length : constant := 63;

   --  Maximum number of labels in a domain name (except for the terminating
   --  root, each label is at least one byte plus its length).
   Max_Label_Count : constant := 127;

   --  Default UDP server port number for the DNS protocol.
   Server_Port_Default : constant := 53;

   --  DNS request identifier.
   subtype ID_Type is Anet.Double_Byte;

   --  Number of records in question, answer, authority, additional sections.
   subtype Record_Count_Type is Anet.Double_Byte;

   --  DNS RR class.
   type RR_Class_Type is (IN_Class)
     with Size => 16;

   --  DNS RR type codes.
   type RR_Type_Type is
     (A,
      NS,
      CNAME,
      SOA,
      AAAA)
     with Size => 16;

private

   for RR_Class_Type use
     (IN_Class => 1);

   for RR_Type_Type use
     (A     => 1,
      NS    => 2,
      CNAME => 5,
      SOA   => 6,
      AAAA  => 28);

end DNS;
