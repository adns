#!/usr/bin/python
#
# Script is based on listing by Brandon Falk, see
# https://gamozolabs.github.io/fuzzing/2018/09/16/scaling_afl.html

import argparse
import multiprocessing
import os
import shutil
import subprocess
import sys
import threading
import time

CPU_COUNT = multiprocessing.cpu_count()

lock = threading.Lock()
sps = []

do_execute = True


def do_work(thread_nr):
    cur_cpu = thread_nr % CPU_COUNT
    master_arg = "-M"
    if thread_nr != 0:
        master_arg = "-S"

    # Restart if it dies, which happens on startup a bit
    while do_execute:
        try:
            sp = subprocess.Popen([
                "taskset", "-c", "%d" % cur_cpu,
                "afl-fuzz", "-i", INPUT_DIR, "-o", OUTPUT_DIR,
                master_arg, "fuzzer%d" % thread_nr, "--",
                "%s" % BINARY, "@@"],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            lock.acquire()
            if do_execute:
                sps.insert(thread_nr, sp)
                lock.release()
                sp.wait()
            else:
                lock.release()
                sp.terminate()

        except:
            pass

        lock.acquire()
        try:
            sps.pop(thread_nr)
        except:
            pass
        lock.release()

        print("Thread %d afl-fuzz instance terminated" % thread_nr)

        # Some backoff if we fail to run
        time.sleep(1.0)


def parse_args():
    """
    Returned parsed command line arguments
    """
    arg_parser = argparse.ArgumentParser(description='Parallel AFL execution')
    arg_parser.add_argument('in_dir', type=str, help='Input directory')
    arg_parser.add_argument('out_dir', type=str, help='Output directory')
    arg_parser.add_argument('binary', type=str, help='Binary to fuzz')
    arg_parser.add_argument('--threads', type=int, default=CPU_COUNT,
                            help=('Number of parallel ALF instances to run '
                                  ' (default: ' + str(CPU_COUNT)))
    return arg_parser.parse_args()


args = parse_args()
INPUT_DIR = args.in_dir
OUTPUT_DIR = args.out_dir
BINARY = args.binary
THREAD_COUNT = args.threads

assert os.path.exists(INPUT_DIR), "Invalid input directory"
assert os.path.exists(BINARY), "Invalid binary"

if THREAD_COUNT > CPU_COUNT:
    print("Warning: Thread count (%d) is larger than CPU count (%d)"
          % (THREAD_COUNT, CPU_COUNT))

if not os.path.exists(OUTPUT_DIR):
    print("Creating output directory")
    os.mkdir(OUTPUT_DIR)

# Disable AFL affinity as we do it better
os.environ["AFL_NO_AFFINITY"] = "1"


for thread in range(0, THREAD_COUNT):
    threading.Timer(0.0, do_work, args=[thread]).start()

    # Let master stabilize first
    if thread == 0:
        time.sleep(1.0)

while do_execute and threading.active_count() > 1:
    try:
        subprocess.check_call(["watch", "-n3", "afl-whatsup", OUTPUT_DIR])
    except KeyboardInterrupt:
        print("Stopping")
        lock.acquire()
        do_execute = False
        for proc in sps:
            try:
                proc.terminate()
            except:
                pass
        lock.release()
    except:
        pass
