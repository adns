--
--  Copyright (C) 2019 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Command_Line;
with Ada.Exceptions;
with Ada.Text_IO;

with GNAT.Exception_Actions;

with Anet.OS;

with Adns_Util.Logger;
with Adns_Util.Timer;

with DNS.Domain;
with DNS.Message;
with DNS.Resource_Record.List;

procedure DNS_Msg_Deserialize
is
   Msg            : DNS.Message.Response_Type;
   Rcode_Unused   : DNS.Message.Rcode_Type;
   Rtype_Unused   : DNS.RR_Type_Type;
   Domain, Canon  : DNS.Domain.Domain_Type;
   Records_Unused : DNS.Resource_Record.List.Resource_Record_List;
   Has_Ans_Unused : Boolean;
begin
   begin
      Msg := DNS.Message.Deserialize
        (Message => Anet.OS.Read_File
           (Filename => Ada.Command_Line.Argument (1)));

      --  Accessing values should not raise exceptions.

      Rcode_Unused := DNS.Message.Get_RCode (Response => Msg);
      Rtype_Unused := DNS.Message.Get_QType (Response => Msg);
      Domain       := DNS.Message.Get_QName (Response => Msg);
      declare
         Dom_Str_Unused : constant String := DNS.Domain.Get_Domain
           (Value => Domain);
      begin
         null;
      end;

      Canon := DNS.Message.Get_Canonical (Response => Msg);

      Records_Unused := DNS.Message.Get_Answer (Response => Msg);
      Has_Ans_Unused := DNS.Message.Has_Answer (Response => Msg);

   exception
      when DNS.Message.Invalid_Message => null;
   end;

   Adns_Util.Timer.Stop;
   Adns_Util.Logger.Stop;

exception
   when E : others =>
      Adns_Util.Timer.Stop;
      Adns_Util.Logger.Stop;
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (X => E));
      GNAT.Exception_Actions.Core_Dump (Occurrence => E);
end DNS_Msg_Deserialize;
