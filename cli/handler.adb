--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Text_IO;

with Anet;

with Adns_Util.Termination;

with DNS.Domain;
with DNS.Message;
with DNS.Server;

package body Handler
is

   procedure Result
     (Handler     : in out Adns_Resolutions_Handler;
      Resolutions :        DNS.Resolution.List.Resolution_List)
   is
      pragma Unreferenced (Handler);

      use type DNS.Message.Rcode_Type;

      Canonical  : DNS.Domain.Domain_Type;
      Response   : DNS.Message.Response_Type;

      Resolution : constant DNS.Resolution.Resolution_Type
        := Resolutions.First_Element;
   begin
      case DNS.Resolution.Get_State (Resolution => Resolution) is
         when DNS.Resolution.Resolved =>
            null;
         when DNS.Resolution.Timeout =>
            Ada.Text_IO.Put_Line ("Timeout!");
            Adns_Util.Termination.Signal
              (Exit_Status => Adns_Util.Termination.Failure);
            return;
         when DNS.Resolution.Failure =>
            Ada.Text_IO.Put_Line ("Failure!");
            Adns_Util.Termination.Signal
              (Exit_Status => Adns_Util.Termination.Failure);
            return;
         when others =>
            Ada.Text_IO.Put_Line ("Unknown issue!");
            Adns_Util.Termination.Signal
              (Exit_Status => Adns_Util.Termination.Failure);
            return;
      end case;

      declare
         From     : constant DNS.Server.Server_Type'Class
           := DNS.Resolution.Get_Server (Resolution);
         Serverv4 : DNS.Server.IPv4_Server_Type;
         Serverv6 : DNS.Server.IPv6_Server_Type;
      begin
         Ada.Text_IO.Put ("Received response from: ");
         if From in DNS.Server.IPv4_Server_Type then
            Serverv4 := DNS.Server.IPv4_Server_Type (From);
            Ada.Text_IO.Put (Anet.To_String (Serverv4.Get_Address));
         else
            Serverv6 := DNS.Server.IPv6_Server_Type (From);
            Ada.Text_IO.Put (Anet.To_String (Serverv6.Get_Address));
         end if;
         Ada.Text_IO.Put_Line (" port" & From.Get_Port'Img);
      end;

      Response := DNS.Resolution.Get_Response (Resolution => Resolution);

      if DNS.Message.Get_RCode (Response => Response) = DNS.Message.Rc_No_Error
      then
         Canonical := DNS.Message.Get_Canonical (Response => Response);
         Ada.Text_IO.Put_Line ("Canonical: "
                               & DNS.Domain.Get_Domain (Canonical));
         if DNS.Message.Has_Answer (Response => Response) then
            for RR of DNS.Message.Get_Answer (Response => Response) loop
               Ada.Text_IO.Put_Line (RR.Get_Data_Str);
            end loop;
         end if;
         Adns_Util.Termination.Signal
           (Exit_Status => Adns_Util.Termination.Success);
      else
         Ada.Text_IO.Put_Line ("Negative result: "
                               & DNS.Message.Get_RCode (Response)'Image);
         Adns_Util.Termination.Signal
              (Exit_Status => Adns_Util.Termination.Failure);
      end if;
   end Result;

end Handler;
