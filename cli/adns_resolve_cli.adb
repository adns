--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Command_Line;
with Ada.Exceptions;
with Ada.IO_Exceptions;
with Ada.Text_IO;
with Ada.Strings.Fixed;

with GNAT.Command_Line;

with Adns_Util.Logger;
with Adns_Util.Termination;

with DNS.Client;
with DNS.Domain;
with DNS.Resolution.Manager;
with DNS.Server.List;
with DNS.Version;

with Handler;

procedure Adns_Resolve_Cli
is
   package Logger renames Adns_Util.Logger;

   package PT_IO is new Ada.Text_IO.Integer_IO (Num => DNS.Server.Port_Type);

   Config     : GNAT.Command_Line.Command_Line_Configuration;
   Servers    : DNS.Server.List.Server_List;
   Resolution : DNS.Resolution.Resolution_Type;
   RR_Type    : DNS.RR_Type_Type := DNS.A;
   Domain     : DNS.Domain.Domain_Type;

   Handle_Result : Handler.Adns_Resolutions_Handler;

   Invalid_Cmd_Line : exception;

   -------------------------------------------------------------------------

   --  Try to parse the given string as server#port combination.
   function Parse_Server (Value : String) return DNS.Server.Server_Type'Class;

   --  Parse command line arguments
   procedure Parse_Argument (Switch, Value : String);

   -------------------------------------------------------------------------

   function Parse_Server (Value : String) return DNS.Server.Server_Type'Class
   is
      Sep  : constant Natural
        := Ada.Strings.Fixed.Index (Source => Value, Pattern => "#");
      Port : DNS.Server.Port_Type;
      Last : Positive;
   begin
      if Sep = 0 then
         return DNS.Server.Create (Address => Value);
      end if;

      PT_IO.Get (From => Value (Sep + 1 .. Value'Last),
                 Item => Port,
                 Last => Last);
      return DNS.Server.Create (Address => Value (Value'First .. Sep - 1),
                                Port    => Port);
   end Parse_Server;

   -------------------------------------------------------------------------

   procedure Parse_Argument (Switch, Value : String)
   is
   begin
      if Switch = "-s" or else Switch = "--server" then
         Servers.Append (New_Item => Parse_Server (Value));
      elsif Switch = "-t" or else Switch = "--type" then
         RR_Type := DNS.RR_Type_Type'Value (Value);
      end if;
   end Parse_Argument;
begin
   Logger.Use_Stdout;
   DNS.Client.Start;

   GNAT.Command_Line.Set_Usage
     (Config   => Config,
      Usage    => "[-s server[#port]]+ [-t qtype] name",
      Help     => "DNS client (" & DNS.Version.Version_String & ")");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Switch      => "-h",
      Long_Switch => "--help",
      Help        => "Display usage and exit");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Callback    => Parse_Argument'Unrestricted_Access,
      Switch      => "-s:",
      Long_Switch => "--server=",
      Help        => "Server address (may be repeated), defaults to Google's" &
        " public DNS servers");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Callback    => Parse_Argument'Unrestricted_Access,
      Switch      => "-t:",
      Long_Switch => "--type=",
      Help        => "Resource record type to query, defaults to A");

   begin
      GNAT.Command_Line.Getopt (Config => Config);

   exception
      when GNAT.Command_Line.Invalid_Switch =>
         --  Automatically prints output.
         raise GNAT.Command_Line.Exit_From_Command_Line;
      when GNAT.Command_Line.Invalid_Parameter =>
         raise Invalid_Cmd_Line with "Missing argument for option '-"
           & GNAT.Command_Line.Full_Switch & "'";
      when Ada.IO_Exceptions.Data_Error | Constraint_Error =>
         raise Invalid_Cmd_Line with "Invalid value for option '-"
           & GNAT.Command_Line.Full_Switch & "'";
   end;

   GNAT.Command_Line.Free (Config => Config);

   if Natural (Servers.Length) = 0 then
      Servers.Append (New_Item => DNS.Server.Create ("8.8.8.8"));
      Servers.Append (New_Item => DNS.Server.Create ("2001:4860:4860::8888"));
      Servers.Append (New_Item => DNS.Server.Create ("8.8.4.4"));
      Servers.Append (New_Item => DNS.Server.Create ("2001:4860:4860::8844"));
   end if;

   declare
      Name : constant String := GNAT.Command_Line.Get_Argument;
   begin
      if Name'Length = 0 then
         raise Invalid_Cmd_Line with "Domain name missing";
      end if;

      DNS.Resolution.Manager.Set_Servers (Servers);

      Domain := DNS.Domain.Validate (Name);

      Resolution := DNS.Resolution.Prepare (Domain => Domain,
                                            QType  => RR_Type);

      DNS.Resolution.Manager.Resolve (Resolution,
                                      Handler => Handle_Result);

      Ada.Command_Line.Set_Exit_Status (Code => Adns_Util.Termination.Wait);
   end;

   DNS.Client.Stop;

exception
   when GNAT.Command_Line.Exit_From_Command_Line =>
      DNS.Client.Stop;
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when E : Invalid_Cmd_Line =>
      Adns_Util.Logger.Log
        (Message => Ada.Exceptions.Exception_Message (X => E));
      Adns_Util.Logger.Log
        (Message => "try """ & Ada.Command_Line.Command_Name &
           " --help"" for more information.");
      DNS.Client.Stop;
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when E : others =>
      Adns_Util.Logger.Log
        (Message => Ada.Exceptions.Exception_Information (X => E));
      DNS.Client.Stop;
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);

end Adns_Resolve_Cli;
