--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package Adns_Util.Logger is

   type Log_Level is
     (Debug,
      Info,
      Notice,
      Warning,
      Error,
      Critical,
      Alert,
      Emergency);

   procedure Log
     (Level   : Log_Level := Info;
      Message : String);
   --  Log the specified message with given loglevel.

   procedure Use_Stdout;
   --  Switch to console based logging.

   procedure Set_Log_Level (Level : Log_Level);
   --  Set the minimum log level for logged messages.

   procedure Stop;
   --  Stop logger.

end Adns_Util.Logger;
