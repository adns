--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Containers.Indefinite_Holders;
with Ada.Streams;

with DNS.Server;

package DNS.Socket.Mock
is
   --  Type for a DNS socket, which can be used for both IPv4 and IPv6 servers.
   type Socket_Type is new DNS.Socket.Socket_Type with null record;

   --  Initialize and bind the socket to a randomly allocated local port.
   overriding
   procedure Init (Socket : in out Socket_Type);

   --  Close the given socket.
   overriding
   procedure Close (Socket : in out Socket_Type);

   --  Send a message to a DNS server.
   overriding
   procedure Send
     (Socket  : Socket_Type;
      Message : Ada.Streams.Stream_Element_Array;
      Server  : DNS.Server.Server_Type'Class);

   --  Receive a message from the socket non-blocking.  An exception is raised
   --  if no data was read.  Otherwise, the server address is returned.
   --  Last is the last byte written in the buffer.
   overriding
   function Receive
     (Socket :     Socket_Type;
      Buffer : out Ada.Streams.Stream_Element_Array;
      Last   : out Ada.Streams.Stream_Element_Offset)
      return DNS.Server.Server_Type'Class;

   use type DNS.Server.Server_Type;

   package SH is new Ada.Containers.Indefinite_Holders
     (Element_Type => DNS.Server.Server_Type'Class);

   --  Type for stats on sockets.
   type Socket_Data_Type is
      record
         Init_Count     : Natural := 0;
         Init_Error     : Natural := 0;
         Close_Count    : Natural := 0;
         Send_Count     : Natural := 0;
         --  The message that was passed to the socket to a specific server.
         Send_Msg       : Ada.Streams.Stream_Element_Array (1 .. 2048)
           := (others => 0);
         Send_Len       : Ada.Streams.Stream_Element_Offset := 0;
         Send_Server    : SH.Holder;
         Send_Error     : Natural := 0;
         --  Configure after how many calls to Receive() the configured message
         --  is returned from the given server.
         Receive_After  : Natural := 0;
         Receive_Count  : Natural := 0;
         Receive_Msg    : Ada.Streams.Stream_Element_Array (1 .. 2048)
           := (others => 0);
         Receive_Len    : Ada.Streams.Stream_Element_Offset := 0;
         Receive_Server : SH.Holder;
         Receive_Error  : Natural := 0;
      end record;

   --  Assume there is only a single socket.
   Socket_Data : Socket_Data_Type;

   --  Clear data collected during a test run.
   procedure Clear;

end DNS.Socket.Mock;
