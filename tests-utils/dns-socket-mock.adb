--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body DNS.Socket.Mock
is

   -------------------------------------------------------------------------

   procedure Init (Socket : in out Socket_Type)
   is
      pragma Unreferenced (Socket);
   begin
      Socket_Data.Init_Count := Socket_Data.Init_Count + 1;
      if Socket_Data.Init_Count = Socket_Data.Init_Error then
         raise Anet.Socket_Error;
      end if;
   end Init;

   -------------------------------------------------------------------------

   procedure Close (Socket : in out Socket_Type)
   is
      pragma Unreferenced (Socket);
   begin
      Socket_Data.Close_Count := Socket_Data.Close_Count + 1;
   end Close;

   -------------------------------------------------------------------------

   procedure Send
     (Socket  : Socket_Type;
      Message : Ada.Streams.Stream_Element_Array;
      Server  : DNS.Server.Server_Type'Class)
   is
      pragma Unreferenced (Socket);
   begin
      Socket_Data.Send_Count := Socket_Data.Send_Count + 1;
      if Socket_Data.Send_Count = Socket_Data.Send_Error then
         raise Anet.Socket_Error;
      end if;

      Socket_Data.Send_Msg (1 .. Message'Length) := Message;
      Socket_Data.Send_Len := Message'Length;
      Socket_Data.Send_Server := SH.To_Holder (Server);
   end Send;

   -------------------------------------------------------------------------

   function Receive
     (Socket :     Socket_Type;
      Buffer : out Ada.Streams.Stream_Element_Array;
      Last   : out Ada.Streams.Stream_Element_Offset)
      return DNS.Server.Server_Type'Class
   is
      use type Ada.Streams.Stream_Element_Offset;

      pragma Unreferenced (Socket);
   begin
      Socket_Data.Receive_Count := Socket_Data.Receive_Count + 1;
      if Socket_Data.Receive_Count = Socket_Data.Receive_Error then
         raise Anet.Socket_Error;
      end if;
      if Socket_Data.Receive_Count /= Socket_Data.Receive_After
        or else Socket_Data.Receive_Server.Is_Empty
      then
         raise DNS.Socket.No_Data;
      end if;

      Buffer (Buffer'First .. Buffer'First + Socket_Data.Receive_Len - 1)
        := Socket_Data.Receive_Msg (1 .. Socket_Data.Receive_Len);
      Last := Buffer'First + Socket_Data.Receive_Len - 1;
      return Socket_Data.Receive_Server.Element;
   end Receive;

   -------------------------------------------------------------------------

   procedure Clear
   is
   begin
      Socket_Data.Send_Server.Clear;
      Socket_Data.Receive_Server.Clear;
      Socket_Data := (others => <>);
   end Clear;

end DNS.Socket.Mock;
