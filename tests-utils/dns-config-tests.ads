--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package DNS.Config is

   --  The number of sockets that can concurrently be in use.
   --  Only use one socket for tests (the value is not actually used).
   Max_Sockets : constant := 1;

   --  With the numbers below one retransmit is sent after 0.2s, and after
   --  a total of 6 checks on the socket the timeout occurs at 0.5s.

   --  Maximum number of retransmits per server.
   Max_Retransmits : constant := 1;

   --  Base retransmission timeout in seconds.
   Retransmission_Timeout : constant := 0.2;

   --  Factor by which the retransmission interval is increased.
   Retransmission_Backoff : constant Float := 1.5;

   --  Interval in seconds used to check for data on sockets.
   Socket_Check_Interval : constant := 0.1;

end DNS.Config;
