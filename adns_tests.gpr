--
--  Copyright (C) 2019  Tobias Brunner <tbrunner@hsr.ch>
--  Copyright (C) 2019  Reto Buerki <reet@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with "adns_util";
with "adns_common";

project Adns_Tests is

   for Source_Dirs use ("src", "tests-utils");
   for Object_Dir use "obj/tests";

   Compiler_Switches := Adns_Common.Compiler'Switches ("Ada")
     & ("-gnatwn",
        "-ftest-coverage",
        "-fprofile-arcs");

   package Compiler is
      for Switches ("Ada") use Compiler_Switches;
   end Compiler;

   package Gnattest is
      for Tests_Dir use "../../tests";
      for Harness_Dir use "gnattest/harness";
      for Skeletons_Default use "fail";
      for Gnattest_Switches use ("--ignore=tests-ignore.txt");
   end Gnattest;

   package Make is
      --  Path is relative to gnattest harness directory of the actual
      --  project.
      for Makefile use "../../../../Makefile";
   end Make;

   package Naming is
      for Spec ("DNS.Config") use
        "dns-config-tests.ads";
      for Spec ("DNS.Socket.Instances") use
        "dns-socket-instances-mock.ads";
   end Naming;

end Adns_Tests;
